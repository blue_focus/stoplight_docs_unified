# Messenger types

Unified API tries to unify messenger APIs and to hide differences between messaging platforms.
This documentation discusses messengers that Unified API support and some of their characterstics. 

> **_NOTE:_**  All phone numbers, especially in messenger IDs and `to` fields should start with international code and without starting 0 or +, e.g. `491234567890`

## WhatsApp through 360Dialog

360Dialog is also called MessagePipe. To create MessagePipe WhatsApp gateway you will need to provide
phone number or WA ID as `messenger_id` and MessagePipe auth token as `auth_key`.

```json
{
    "provider": "MessagePipe",
    "messenger": "WhatsApp",
    "webhook_url": "https://webhook.example.com/",
    "webhook_secret": "LFqC1DWEWkxJ2aUUvWjM",
    "messenger_id": "4901234567890",
    "auth_key": "bAhfb5TxcMosjKLcxVAyo3PEW9"
}
```

## Webchat by Inbox Solutions 

To create Webchat gateway you will need to provide webchat ID as `messenger_id` and auth key as `auth_key`.

```json
{
    "provider": "Webchat",
    "messenger": "Webchat",
    "webhook_url": "https://webhook.example.com/",
    "webhook_secret": "Gsis0YfhEqUOemZopKWV",
    "messenger_id": "nThSNHRcQc",
    "auth_key": "qMeWWxOM2gVkZsTo3o2Vh0TRbeK8724XNJh1iileXqNbwAbul5op32dhd6UyKqaL"
}
```

## Facebook Messenger

To create Facebook Messenger you will need to provide [Facebook Page ID](https://www.facebook.com/help/1503421039731588/?helpref=uf_share) as `messenger_id` 
and [user access token](https://developers.facebook.com/docs/facebook-login/access-tokens/) as `auth_key`.  

Facebook Messenger allows to message users who first contacted Facebook pages you own.  

```json
{
    "provider": "Facebook",
    "messenger": "FacebookMessenger",
    "webhook_url": "https://webhook.example.com/",
    "webhook_secret": "SejVbVBv3HX2LYCP2fy0",
    "messenger_id": "100848125297189",
    "auth_key": "EAAyfhwpP4UVgukfAw2EIziLRH5zjbU7WdaUUGYxqFfjINxPdWm6Osh7C8hS9Cw9hNg9w3GoclcIFpX3Dzl3hY"
}
```

> **_NOTE:_**  Currently it is not possible to create Facebook Messenger gateway outside of 
>our ChatWerk by Inbox Solutions GmbH (App ID: 178756520631573) Facebook application. To achieve that,
>access tokens also must be correctly scoped. Contact us to discuss details and get support.

## Facebook Instagram

To create Instagram gateway you will need to provide Instagram business account ID as `messenger_id`, [Facebook Page ID](https://www.facebook.com/help/1503421039731588/?helpref=uf_share) as `facebook_page_id` 
and [user access token](https://developers.facebook.com/docs/facebook-login/access-tokens/) as `auth_key`.  
Instagram business account and Facebook page must be connected (see: https://www.facebook.com/business/help/898752960195806).

Facebook Instagram allows to message other Instagram users after they contact your account.  

```json
{
    "provider": "Facebook",
    "messenger": "Instagram",
    "webhook_url": "https://webhook.example.com/",
    "webhook_secret": "SejVbVBv3HX2LYCP2fy0",
    "messenger_id": "17841448926352460",
    "facebook_page_id": "100848125297189",
    "auth_key": "EAAyfhwpP4UVgukfAw2EIziLRH5zjbU7WdaUUGYxqFfjINxPdWm6Osh7C8hS9Cw9hNg9w3GoclcIFpX3Dzl3hY"
}
```

> **_NOTE:_**  Currently it is not possible to create Facebook Messenger gateway outside of 
>our ChatWerk by Inbox Solutions GmbH (App ID: 178756520631573) Facebook application. To achieve that,
>access tokens also must be correctly scoped. Contact us to discuss details and get support.

## Telegram

To create Telegram gateway you will need to provide bot name (Telegram bot names end with `_bot`) as `messenger_id`
and bot token as `auth_key`. 
You create Telegram bots by messaging [BotFather](https://t.me/botfather). You will set bot name and get bot token there.

```json
{
    "messenger": "TelegramBot",
    "provider": "Telegram",
    "webhook_url": "https://webhook.example.com/",
    "webhook_secret": "zSGQf5zbwE1AGjO6xkk9",
    "messenger_id": "example_bot",
    "auth_key": "1234567890:zqMTyZXPldThwUoBJ4L43nZJz4flTNX44LH"
}
```

## Google Business Messages

To create Google Business Messages gateway, you need to provide details about your business.
You don't need `messenger_id` or auth data in this case, but you will need to provide domains, contact information or office hours.
Google support will contact you or brand contact you provide to verify the data and grant Unified API right to process messages from Google BM chat. 
Process of creating Google BM gateway may take up to 10 work days to complete.

```json
{
    "provider": "Google",
    "messenger": "GoogleBM",
    "webhook_url": "https://webhook.example.com/",
    "webhook_secret": "jgNYLBCc1LfCk0aIRnhD",
    "messenger_settings": {
        "profile_photo": {
            "url": "https://storage.example.com/my-logo"
        },
        "name": "My business",
        "locale": "de",
        "website_url": "https://example.com",
        "privacy_policy_url": "https://example.com/privacy-policy",
        "contact_page_url": "https://example.com/contact",
        "brand_contact_name": "John Doe",
        "brand_contact_email": "johndoe@example.com",
        "welcome_message": "Hello, how can I help you?",
        "offline_message": "Sorry, we're having a break. We will be back soon.",
        "domains": [
            "example.com",
            "example.de"
        ],
        "region_codes": [
            "de",
            "at",
            "ch"
        ],
        "phone_numbers": [
            {
                "number": "4901234567890"
            }
        ],
        "office_hours": [
            {
                "start_time": {
                    "hours": 8,
                    "minutes": 0
                },
                "end_time": {
                    "hours": 15,
                    "minutes": 59
                },
                "time_zone": "Europe/Berlin",
                "start_day": "Monday",
                "end_day": "Friday"
            }
        ]
    }
}
```

## ChatwerkConnect by Inbox Solutions 
                   
This gateway allows sending and receiving messages between Unified gateways. 
Messages sent from one gateway are passed to another gateways webhook directly.
To create ChatwerkConnect gateway you only need to set gateway name. 

```json
{
    "provider": "ChatWerk",
    "messenger": "ChatWerkConnect",
    "webhook_url": "https://webhook.example.com/",
    "webhook_secret": "Gsis0YfhEqUOemZopKWV",
    "messenger_settings": {
        "name": "Example GTW name"
    }
}
```

As a response you will receive `messenger_id` with UUID string, eg.: `284c18e2-727f-423a-84f5-6ce776b0b63d`.
This value must be as a recipient of future messages. Gateway name is sent as chat/from name of messages. 
