# Error handling

Unified API communicates error using HTTP status codes and payloads
which can also contain more error codes unique to Unified API.

## HTTP status codes

| HTTP status code 	          | Description 	                                                                                                                                                          |
|-----------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 200 OK 	                    | 	                                                                                                                                                                      |
| 400 Bad Request 	           | Check payload body if available. 	                                                                                                                                     |
| 401 Unauthorized 	          | Credentials you provided are incorrect. <br />Check payload body if available. 	                                                                                       |
| 404 Not found 	             | Resource you are trying to access doesn't exist. <br />Check payload body if available.                                                                                |
| 422 Unprocessable Entity 	  | Invalid or unprocessable data. <br />Check payload body.                                                                                                               |
| 500 Internal Server Error 	 | Either Unified API experiences server issues or there is problem with messaging providers. Try again later or contact support. <br />Check payload body if available.	 |

## Structure of error payloads

Here is an error payload you could receive when creating gateways `POST /v1/gateway`: 
```json
{
    "code": 4002,
    "error_id": "c2f3a08e-1c88-4af0-861b-3e0b4d642434",
    "message": "Invalid parameter",
    "errors": [
        {
            "code": "unique_messenger_id",
            "field": "messenger_id"
        }
    ]
}
```

* `code` - Unified API error code, listed [below](./unified_error_handling.md#error-codes).
* `error_id` - Error identifier, which can be shared to support, can let us find more specific reason of the error.
* `message` - Additional description of the error.
* `errors` - An array of more specific errors found when processing requests, especially for problems related with request payload.
    * `code` - Error code, as string, describing actual problem related with provided data.
    * `field` - Related field.
    * `param` - Value provided in request.
    
All fields are optional.

## Error codes

| Error code 	 | Name 	                    | Description 	                                                                              |
|--------------|---------------------------|--------------------------------------------------------------------------------------------|
| `4000` 	     | ErrorGenericError 	       | 	                                                                                          |
| `4002` 	     | ErrorInvalidParameter 	   | Provided data is incorrect. Check `message` and `errors` to get more specific explanation. |
| `4003` 	     | ErrorInvalidCredentials 	 | Credentials to provider are incorrect.                                                     |
| `4010` 	     | ErrorContactNotFound 	    | Contact is not found, e.g. when cannot sent message to unknown recipient.                  |
| `4011` 	     | ErrorFileIsTooBig 	       | File is too big to be sent or processed.                                                   |
