# Introduction

Unified API allows sending and receiving messages through different messaging platforms using single, unified API.

We currently support:

* [WhatsApp](https://www.whatsapp.com/business/api/) through [360Dialog](https://www.360dialog.com/en/)
* [Facebook Messenger](https://www.facebook.com/business/marketing/messenger)
* [Instagram](https://developers.facebook.com/products/messenger/messenger-api-instagram/)
* [Telegram bots](https://core.telegram.org/bots)
* [Google Business Messages](https://developers.google.com/business-communications/business-messages)
* [ChatwerkConnect](https://chatwerk.de/) - inbox to inbox messages

Before you start to [receive](./unified_receiving_events.html) and [send](./unified_send_messages.html) messages 
you will need to [create and configure your gateway](./unified_gateway_setup.html).