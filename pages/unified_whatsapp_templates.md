# WhatsApp templates

A unique feature of WhatsApp is ability to create and send _template messages_.
When template is created you must provide general contents of message. 
Template is then submitted for an approval by WhatsApp.
When template is approved, you can send template message by providing parameters into the messages.
Templates are meant to be used as notifications and allow to message users 
outside of 24h window, when normal messages can be sent.  

[See WhatsApp documentation](https://developers.facebook.com/docs/whatsapp/api/messages/message-templates/) for details.

We provide API to create, list, delete and send _template messages_.