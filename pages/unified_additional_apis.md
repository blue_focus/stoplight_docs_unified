# Additional APIs

Apart from messaging and gateway related APIs we provide some additional APIs.

## Messenger limits

Messenger limits endpoint will allow you to dynamically access information about which messengers Unified API supports, 
what message types are allowed and what our the limits related to content.

Request:

```curl
curl --location 
--request GET 'http://api.unified.chatwerk.de/v1/messengers-limits' 
--header 'x-uniapi-key: MTIzNDU2Nzg6YWJjZGVmZ2hpamtsMTIzNDU2Nzg5MA' 
```
