# Create gateway

In order to set up communication between a messenger app and Unified API you must create *a gateway*.
Gateway bundles all configuration we need to process messages and events.

Example request to create a new gateway:

```curl
curl 
--request POST 'https://api.unified.chatwerk.de/v1/gateways' 
--header 'Content-Type: application/json' 
--header 'x-uniapi-key: MTIzNDU2Nzg6YWJjZGVmZ2hpamtsMTIzNDU2Nzg5MA' 
--data-raw '{
    "messenger_id": "490123456789",
    "provider": "Facebook",
    "messenger": "FacebookMessenger",
    "auth_key": "YYBFPeGeRZnsxkgzu3lVXdef",
    "webhook_url": "https://webhook.example.com/",
    "webhook_secret": "DS5yf0ZxKuDS5yf0ZxKu"
    "reference": "my-fb-page-490123456789"
}'
```

When creating gateway you may need to set following fields:

* `messenger_id` - Unique number or name that messenger app assigns to your business, bot or contact
* `provider` - Integrates directly with messaging platform or operates messaging app. Provider can support many messengers.
* `messenger` - Messaging product you use when communicating with users.
* `auth_key`, `auth_secret` - Credentials to access provider API.
* `webhook_url` - URL where Unified API will send messages and events.
* `webhook_secret` - Secret will be added to webhook events to authenticate them.
* `messenger_settings` - Other messenger related settings, like profile description, availability or photos. 
* `reference` - It's any value you want to assign to your gateway. This value will be reported and provided with gateway related data. 

We present examples of real full payloads required to created gateways on [messengers page](./unified_messenger_examples.html).

## FAQ

**What operations can be done on gateways?**

You can create gateways, update some of their attributes, e.g. webhook_url or webhook_secret and delete gateways.
You cannot change gateway provider, messenger or messenger_id. 

**Why `messenger_id` isn't required for all gateways?**

Some providers and messengers may not require any ID, because entrypoint for user communication is added when you setup gateway.
For example Google Business Messages doesn't need it, because Google BM creates its chat when you create Unified API gateway.
Google BM gateway has messenger ID, but it's returned as a response from Unified API.