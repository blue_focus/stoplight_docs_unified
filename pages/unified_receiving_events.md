# Receive events

To receive events you need to configure webhook url first. 
This can be done when updating or [creating gateway](./unified_gateway_setup.html).

Requests you receive on your webhook have form:
 
```http
POST / HTTP/1.1
Host: webhook.example.com
Content-Type: application/json
X-Uniapi-Secret: DS5yf0ZxKuDS5yf0ZxKu

{
    "gateway": {
        "provider": "Telegram",
        "messenger": "TelegramBot",
        "id": "KXjghYKPjC",
        "gateway": "example_bot"
    },
    "messages": [
        {
            "id": "1",
            "from": {
                "contact_id": "1234567890",
                "profile_name": "John Smith",
                "language": "en"
            },
            "text": "Hello",
            "timestamp": 1627201815,
            "type": "text",
            "chat": {
                "contact_id": "1234567890",
                "name": "John Smith",
                "type": "person"
            }
        }
    ]
}
```

> **_NOTE:_**  Data structure of incoming events is documented under `WebhookData` model.

We consider an event to be delivered if you respond with 200 OK status.
If Unified API cannot deliver an event, it will retry sending it later. 
Events that cannot be delivered for a longer period will eventually get removed.

Webhook events contain messages, statuses and system events.

## Receiving messages

Message types are the same as for messages sent, and we describe them [here](./unified_send_messages.md#message-types).

```json
{
    "gateway": {
        "provider": "Telegram",
        "messenger": "TelegramBot",
        "id": "MCDzSbOtpr",
        "gateway": "example_bot"
    },
    "messages": [
        {
            "id": "1",
            "from": {
                "contact_id": "1234567890",
                "profile_name": "John Smith",
                "language": "en"
            },
            "text": "Hello",
            "timestamp": 1627201815,
            "type": "text",
            "chat": {
                "contact_id": "1111111111",
                "name": "John Smith",
                "type": "person"
            }
        }
    ]
}
```

> **_NOTE:_**  You need messages `contact_id` value from `from` field (`1234567890` in above example) to address messages you send.

## Receiving statuses

If you send a message, you may receive status events for that message 
when message is delivered, read, etc. depending on what messenger and provider support.

Statuses always have `message_id` fields, specifying the message they are related to.
Some statuses may be related to messages you receive, like `DELETED` status. 
Support statuses are described in API specification. 


Example:

```json
{
    "gateway": {
        "provider": "MessagePipe",
        "messenger": "WhatsApp",
        "id": "fVxrwxgNpM",
        "gateway": "491234567890"
    },
    "statuses": [
        {
            "message_id": "gBGGSGBYNWMPAgnDh-vU1z4CPLo",
            "timestamp": 1627201815,
            "status": "read"
        }
    ]
}
```

## Receiving system events

System messages are related to gateway and chat rather than any specific message.

For example, you receive `CALLBACK` system event when user clicks on one of buttons you sent them. 
Callback doesn't have message ids. You can lookup action related to button by the event's payload.

In general, types of system messages we support are specific to messenger/provider type of gateway.

Example request:

```json
{
    "gateway": {
        "provider": "Telegram",
        "messenger": "TelegramBot",
        "id": "mBtfEKBFAc", 
        "gateway": "example_bot"
    },
    "system": [
        {
            "timestamp": 1627201815,
            "callbacks": [
                {
                    "payload": "OPT_CONFIRM"
                }
            ],
            "type": "callback",
            "chat": {
                "contact_id": "1234567890",
                "name": "John Smith",
                "type": "person"
            }
        }
    ]
}
```
