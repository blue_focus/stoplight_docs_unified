# Send messages

To send a message you need:

* to [create gateway](./unified_gateway_setup.html).
* to [receive any message](./unified_receiving_events.html) from user first to get their contact id.

If you want to message user you need to make an HTTP POST request. 

```curl
curl --location --request POST 'https://api.unified.chatwerk.de/v1/gateways/AHdjDAAYeI/message' \
--header 'Content-Type: application/json' \
--header 'x-uniapi-key: MTIzNDU2Nzg6YWJjZGVmZ2hpamtsMTIzNDU2Nzg5MA' 
--data-raw '{
    "to": "4901234567890",
    "type": "text",
    "text": "Hello from Unified"
}'
```

As a response you should receive message id. Storing this ID will allow you to receive and process 
[statuses related to message](./unified_receiving_events.md#Receiving-statuses), like `delivered` or `read`.

```json
{
    "id": "gBEGkYiEB1VXAglK1ZEqA1YKPrU"
}
```

## Message types

### Text

To send a text message:

```json
{
    "to": "4901234567890",
    "type": "text",
    "text": "Hello from Unified"
}
```

### Image

To send an image message:

```json
{
    "to": "4901234567890",
    "type": "image",
    "media_url": "https://storage.example.com/images/photo1.jpg",
    "text": "Look at this picture!"
}
```

### Video

To send a video message:

```json
{
    "to": "4901234567890",
    "type": "video",
    "media_url": "https://storage.example.com/video/video1.mp4"
}
```

### Audio

To send an audio message:

```json
{
    "to": "4901234567890",
    "type": "audio",
    "media_url": "https://storage.example.com/audio/audio1.mp3"
}
```

### Document

To send a document message:

```json
{
    "to": "4901234567890",
    "type": "document",
    "media_url": "https://storage.example.com/files/document.pdf",
    "media_filename": "document.pdf"
}
```

### File

To send a file message:

```json
{
    "to": "4901234567890",
    "type": "file",
    "media_url": "https://storage.example.com/files/file.txt",
    "media_filename": "file.txt"
}
```

### Buttons

Buttons message contains buttons and user can click on buttons. 
Depending on messenger buttons may disapear or become disable after clicking or will continue to be visible.

There are different types of buttons:  

* `callback` - when clicked you will receive [callback events](./unified_receiving_events.md#receiving-system-events) on your webhook, event will contain buttons payload
* `url` - when clicked user is redirected to web browser and visits page you sent as payload
* `phone_number` - when clicked user is redirected to phone call app with preset phone number which you provided in payload

Messengers may not support all of them and mixing them may not be allowed. 
Every messenger has its own allowed number of buttons allowed in a single message - you can get this limit 
from our [messengers-limits](./unified_additional_apis.md#messenger-limits) endpoint. 

To send a buttons message:

```json
{
    "to": "4901234567890",
    "type": "buttons",
    "text": "What is your favorite color?",
    "buttons": [
        {
            "type": "callback",
            "text": "Blue",
            "payload": "OPT_0000FF"
        },
        {
            "type": "callback",
            "text": "Red",
            "payload": "OPT_FF0000"
        }
    ]
}
```

### List

> **_NOTE:_**  List message is currently only supported by MessagePipe WhatsApp.

List message contains list of items from which user can select and submit one of them.
When user submits their answer, you will [receive a callback event](./unified_receiving_events.md#Receiving-system-events).

To send a list message:

```json
{
    "to": "4901234567890",
    "type": "list",
    "text": "Choose a dish",
    "button_name": "Our menu",
    "header": {
        "type": "image",
        "text": "Carte",
        "media_url": "https://storage.example.com/photo/carte.jpg"
    },
    "items": [
        {
            "text": "Chicken",
            "payload": "OPT_CHICKEN"
        },
        {
            "text": "Fish",
            "payload": "OPT_FISH"
        }
    ],
    "footer": {
        "text": "All dishes are served with potatoes and salad."
    }
}
```

### Multiselect

Multiselect message contains buttons and user can choose and submit many of them at the same time.

> **_NOTE:_**  Multiselect message is currently only supported by Webchat.

To send a multiselect message:

```json
{
    "to": "4901234567890",
    "type": "mutliselect",
    "text": "What gear you need?",
    "buttons": [
        {
            "type": "callback",
            "text": "Skis",
            "payload": "OPT_SKIS"
        },
        {
            "type": "callback",
            "text": "Shoes",
            "payload": "OPT_SHOES"
        },
        {
            "type": "callback",
            "text": "Helmet",
            "payload": "OPT_HELMET"
        }   
    ]
}
```

### Location

If you send location, user will receive a map pin with location you marked.

To send a location message:

```json
{
    "to": "4901234567890",
    "type": "location",
    "location": {
        "name": "Reichstag",
        "address": "Platz der Republik 1, 11011 Berlin",
        "latitude": 52.518622,
        "longitude": 13.375951
    }
}
```

### Contact

To send a contact message:

```json
{
    "to": "4901234567890",
    "type": "contact",
    "contacts": [
        {
            "name": {
                "formatted_name": "Mr. John P. Smith M.D.",
                "first_name": "John",
                "last_name": "Smith",
                "additional_name": "Paul",
                "prefix": "Mr.",
                "suffix": "M.D."
            },
            "org": {
                "company": "John Corp",
                "department": "Development",
                "title": "CEO"
            },
            "birthday": "1987-06-05",
            "photo": {
                "url": "https://storage.example.com/images/photo.jpg"
            },
            "phones": [
                {
                    "phone": "491234567890",
                    "type": "HOME"
                }
            ],
            "addresses": [
                {
                    "street": "Platz Der Republik 123",
                    "city": "Berlin",
                    "state": "Berlin",
                    "zip": "11011",
                    "country": "Germany",
                    "country_code": "de",
                    "type": "WORK"
                }
            ],
            "emails": [
                {
                    "email": "john@smith.de",
                    "type": "WORK"
                }
            ]
        }
    ]
}
```

Contact message field support

|           | WhatsApp (MessagePipe)            | Telegram       | Facebook | Webchat | GoogleBM |
|-----------|-----------------------------------|----------------|----------|---------|----------|
| vcard     | no                                | only receiving | no       | no      | no       |
| name      | yes                               | yes            | no       | no      | no       |
| org       | yes                               | yes            | no       | no      | no       |
| birthday  | yes                               | yes            | no       | no      | no       |
| photo     | only receiving (data, mime_type)  | yes            | no       | no      | no       |
| phones    | yes                               | yes            | no       | no      | no       |
| addresses | yes                               | yes            | no       | no      | no       |
| emails    | yes                               | yes            | no       | no      | no       |
| urls      | yes                               | yes            | no       | no      | no       |
| ims       | only receiving (service, user_id) | yes            | no       | no      | no       |
