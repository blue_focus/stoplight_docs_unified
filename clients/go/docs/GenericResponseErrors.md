# GenericResponseErrors

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Field** | **string** |  | 
**Message** | Pointer to **string** |  | [optional] 
**Code** | Pointer to **string** |  | [optional] 
**Param** | Pointer to **string** |  | [optional] 

## Methods

### NewGenericResponseErrors

`func NewGenericResponseErrors(field string, ) *GenericResponseErrors`

NewGenericResponseErrors instantiates a new GenericResponseErrors object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGenericResponseErrorsWithDefaults

`func NewGenericResponseErrorsWithDefaults() *GenericResponseErrors`

NewGenericResponseErrorsWithDefaults instantiates a new GenericResponseErrors object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetField

`func (o *GenericResponseErrors) GetField() string`

GetField returns the Field field if non-nil, zero value otherwise.

### GetFieldOk

`func (o *GenericResponseErrors) GetFieldOk() (*string, bool)`

GetFieldOk returns a tuple with the Field field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetField

`func (o *GenericResponseErrors) SetField(v string)`

SetField sets Field field to given value.


### GetMessage

`func (o *GenericResponseErrors) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *GenericResponseErrors) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *GenericResponseErrors) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *GenericResponseErrors) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetCode

`func (o *GenericResponseErrors) GetCode() string`

GetCode returns the Code field if non-nil, zero value otherwise.

### GetCodeOk

`func (o *GenericResponseErrors) GetCodeOk() (*string, bool)`

GetCodeOk returns a tuple with the Code field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCode

`func (o *GenericResponseErrors) SetCode(v string)`

SetCode sets Code field to given value.

### HasCode

`func (o *GenericResponseErrors) HasCode() bool`

HasCode returns a boolean if a field has been set.

### GetParam

`func (o *GenericResponseErrors) GetParam() string`

GetParam returns the Param field if non-nil, zero value otherwise.

### GetParamOk

`func (o *GenericResponseErrors) GetParamOk() (*string, bool)`

GetParamOk returns a tuple with the Param field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParam

`func (o *GenericResponseErrors) SetParam(v string)`

SetParam sets Param field to given value.

### HasParam

`func (o *GenericResponseErrors) HasParam() bool`

HasParam returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


