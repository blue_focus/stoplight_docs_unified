# WebhookMessageAudio

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Url** | **string** |  | 
**Mime** | **string** |  | 
**Voice** | **bool** |  | 
**Filename** | Pointer to **string** |  | [optional] 

## Methods

### NewWebhookMessageAudio

`func NewWebhookMessageAudio(url string, mime string, voice bool, ) *WebhookMessageAudio`

NewWebhookMessageAudio instantiates a new WebhookMessageAudio object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookMessageAudioWithDefaults

`func NewWebhookMessageAudioWithDefaults() *WebhookMessageAudio`

NewWebhookMessageAudioWithDefaults instantiates a new WebhookMessageAudio object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WebhookMessageAudio) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WebhookMessageAudio) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WebhookMessageAudio) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *WebhookMessageAudio) HasId() bool`

HasId returns a boolean if a field has been set.

### GetUrl

`func (o *WebhookMessageAudio) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *WebhookMessageAudio) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *WebhookMessageAudio) SetUrl(v string)`

SetUrl sets Url field to given value.


### GetMime

`func (o *WebhookMessageAudio) GetMime() string`

GetMime returns the Mime field if non-nil, zero value otherwise.

### GetMimeOk

`func (o *WebhookMessageAudio) GetMimeOk() (*string, bool)`

GetMimeOk returns a tuple with the Mime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMime

`func (o *WebhookMessageAudio) SetMime(v string)`

SetMime sets Mime field to given value.


### GetVoice

`func (o *WebhookMessageAudio) GetVoice() bool`

GetVoice returns the Voice field if non-nil, zero value otherwise.

### GetVoiceOk

`func (o *WebhookMessageAudio) GetVoiceOk() (*bool, bool)`

GetVoiceOk returns a tuple with the Voice field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVoice

`func (o *WebhookMessageAudio) SetVoice(v bool)`

SetVoice sets Voice field to given value.


### GetFilename

`func (o *WebhookMessageAudio) GetFilename() string`

GetFilename returns the Filename field if non-nil, zero value otherwise.

### GetFilenameOk

`func (o *WebhookMessageAudio) GetFilenameOk() (*string, bool)`

GetFilenameOk returns a tuple with the Filename field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFilename

`func (o *WebhookMessageAudio) SetFilename(v string)`

SetFilename sets Filename field to given value.

### HasFilename

`func (o *WebhookMessageAudio) HasFilename() bool`

HasFilename returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


