# ChatType

## Enum


* `CHAT_UNKNOWN` (value: `"unknown"`)

* `CHAT_PERSON` (value: `"person"`)

* `CHAT_GROUP` (value: `"group"`)

* `CHAT_CHANNEL` (value: `"channel"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


