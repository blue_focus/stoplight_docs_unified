# WhatsAppTemplateMessageComponents

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**SubType** | Pointer to **string** | For buttons only | [optional] 
**Index** | Pointer to **string** | For buttons only | [optional] 
**Parameters** | Pointer to [**[]WhatsAppTemplateMessageParameters**](WhatsAppTemplateMessageParameters.md) |  | [optional] 

## Methods

### NewWhatsAppTemplateMessageComponents

`func NewWhatsAppTemplateMessageComponents(type_ string, ) *WhatsAppTemplateMessageComponents`

NewWhatsAppTemplateMessageComponents instantiates a new WhatsAppTemplateMessageComponents object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMessageComponentsWithDefaults

`func NewWhatsAppTemplateMessageComponentsWithDefaults() *WhatsAppTemplateMessageComponents`

NewWhatsAppTemplateMessageComponentsWithDefaults instantiates a new WhatsAppTemplateMessageComponents object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *WhatsAppTemplateMessageComponents) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WhatsAppTemplateMessageComponents) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WhatsAppTemplateMessageComponents) SetType(v string)`

SetType sets Type field to given value.


### GetSubType

`func (o *WhatsAppTemplateMessageComponents) GetSubType() string`

GetSubType returns the SubType field if non-nil, zero value otherwise.

### GetSubTypeOk

`func (o *WhatsAppTemplateMessageComponents) GetSubTypeOk() (*string, bool)`

GetSubTypeOk returns a tuple with the SubType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSubType

`func (o *WhatsAppTemplateMessageComponents) SetSubType(v string)`

SetSubType sets SubType field to given value.

### HasSubType

`func (o *WhatsAppTemplateMessageComponents) HasSubType() bool`

HasSubType returns a boolean if a field has been set.

### GetIndex

`func (o *WhatsAppTemplateMessageComponents) GetIndex() string`

GetIndex returns the Index field if non-nil, zero value otherwise.

### GetIndexOk

`func (o *WhatsAppTemplateMessageComponents) GetIndexOk() (*string, bool)`

GetIndexOk returns a tuple with the Index field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIndex

`func (o *WhatsAppTemplateMessageComponents) SetIndex(v string)`

SetIndex sets Index field to given value.

### HasIndex

`func (o *WhatsAppTemplateMessageComponents) HasIndex() bool`

HasIndex returns a boolean if a field has been set.

### GetParameters

`func (o *WhatsAppTemplateMessageComponents) GetParameters() []WhatsAppTemplateMessageParameters`

GetParameters returns the Parameters field if non-nil, zero value otherwise.

### GetParametersOk

`func (o *WhatsAppTemplateMessageComponents) GetParametersOk() (*[]WhatsAppTemplateMessageParameters, bool)`

GetParametersOk returns a tuple with the Parameters field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParameters

`func (o *WhatsAppTemplateMessageComponents) SetParameters(v []WhatsAppTemplateMessageParameters)`

SetParameters sets Parameters field to given value.

### HasParameters

`func (o *WhatsAppTemplateMessageComponents) HasParameters() bool`

HasParameters returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


