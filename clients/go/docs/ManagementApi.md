# \ManagementApi

All URIs are relative to *https://api.unified.chatwerk.de/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ApikeyCreate**](ManagementApi.md#ApikeyCreate) | **Post** /apikeys | Create api key
[**ApikeyDelete**](ManagementApi.md#ApikeyDelete) | **Delete** /apikeys/{apiKeyId} | Delete API key
[**GatewayCreate**](ManagementApi.md#GatewayCreate) | **Post** /gateways | Create gateway
[**GatewayDelete**](ManagementApi.md#GatewayDelete) | **Delete** /gateways/{gatewayId} | Delete gateway
[**GatewayGet**](ManagementApi.md#GatewayGet) | **Get** /gateways/{gatewayId} | Get gateway
[**GatewayUpdate**](ManagementApi.md#GatewayUpdate) | **Patch** /gateways/{gatewayId} | Update gateway
[**GatewaysList**](ManagementApi.md#GatewaysList) | **Get** /gateways | List gateways



## ApikeyCreate

> ApiKey ApikeyCreate(ctx).Execute()

Create api key



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ManagementApi.ApikeyCreate(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ManagementApi.ApikeyCreate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ApikeyCreate`: ApiKey
    fmt.Fprintf(os.Stdout, "Response from `ManagementApi.ApikeyCreate`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiApikeyCreateRequest struct via the builder pattern


### Return type

[**ApiKey**](ApiKey.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ApikeyDelete

> GenericResponse ApikeyDelete(ctx, apiKeyId).Execute()

Delete API key



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    apiKeyId := "apiKeyId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ManagementApi.ApikeyDelete(context.Background(), apiKeyId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ManagementApi.ApikeyDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ApikeyDelete`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `ManagementApi.ApikeyDelete`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**apiKeyId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiApikeyDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GatewayCreate

> Gateway GatewayCreate(ctx).GatewayCreateRequest(gatewayCreateRequest).Execute()

Create gateway



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayCreateRequest := *openapiclient.NewGatewayCreateRequest(openapiclient.Provider("MessagePipe"), openapiclient.Messenger("WhatsApp")) // GatewayCreateRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ManagementApi.GatewayCreate(context.Background()).GatewayCreateRequest(gatewayCreateRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ManagementApi.GatewayCreate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GatewayCreate`: Gateway
    fmt.Fprintf(os.Stdout, "Response from `ManagementApi.GatewayCreate`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGatewayCreateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gatewayCreateRequest** | [**GatewayCreateRequest**](GatewayCreateRequest.md) |  | 

### Return type

[**Gateway**](Gateway.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GatewayDelete

> GenericResponse GatewayDelete(ctx, gatewayId).Execute()

Delete gateway



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ManagementApi.GatewayDelete(context.Background(), gatewayId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ManagementApi.GatewayDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GatewayDelete`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `ManagementApi.GatewayDelete`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGatewayDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GatewayGet

> Gateway GatewayGet(ctx, gatewayId).Execute()

Get gateway



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ManagementApi.GatewayGet(context.Background(), gatewayId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ManagementApi.GatewayGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GatewayGet`: Gateway
    fmt.Fprintf(os.Stdout, "Response from `ManagementApi.GatewayGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGatewayGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**Gateway**](Gateway.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GatewayUpdate

> Gateway GatewayUpdate(ctx, gatewayId).GatewayPatchRequest(gatewayPatchRequest).Execute()

Update gateway



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    gatewayPatchRequest := *openapiclient.NewGatewayPatchRequest() // GatewayPatchRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ManagementApi.GatewayUpdate(context.Background(), gatewayId).GatewayPatchRequest(gatewayPatchRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ManagementApi.GatewayUpdate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GatewayUpdate`: Gateway
    fmt.Fprintf(os.Stdout, "Response from `ManagementApi.GatewayUpdate`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGatewayUpdateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **gatewayPatchRequest** | [**GatewayPatchRequest**](GatewayPatchRequest.md) |  | 

### Return type

[**Gateway**](Gateway.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GatewaysList

> GatewaysList GatewaysList(ctx).Offset(offset).Limit(limit).Execute()

List gateways



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    offset := int32(56) // int32 |  (optional)
    limit := int32(56) // int32 |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ManagementApi.GatewaysList(context.Background()).Offset(offset).Limit(limit).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ManagementApi.GatewaysList``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GatewaysList`: GatewaysList
    fmt.Fprintf(os.Stdout, "Response from `ManagementApi.GatewaysList`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiGatewaysListRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int32** |  | 
 **limit** | **int32** |  | 

### Return type

[**GatewaysList**](GatewaysList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

