# ButtonType

## Enum


* `BUTTON_CALLBACK` (value: `"callback"`)

* `BUTTON_URL` (value: `"url"`)

* `BUTTON_PHONE_NUMBER` (value: `"phone_number"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


