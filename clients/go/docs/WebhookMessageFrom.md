# WebhookMessageFrom

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContactId** | **string** |  | 
**ProfileName** | **string** |  | 
**Language** | Pointer to **string** |  | [optional] 

## Methods

### NewWebhookMessageFrom

`func NewWebhookMessageFrom(contactId string, profileName string, ) *WebhookMessageFrom`

NewWebhookMessageFrom instantiates a new WebhookMessageFrom object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookMessageFromWithDefaults

`func NewWebhookMessageFromWithDefaults() *WebhookMessageFrom`

NewWebhookMessageFromWithDefaults instantiates a new WebhookMessageFrom object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContactId

`func (o *WebhookMessageFrom) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *WebhookMessageFrom) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *WebhookMessageFrom) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetProfileName

`func (o *WebhookMessageFrom) GetProfileName() string`

GetProfileName returns the ProfileName field if non-nil, zero value otherwise.

### GetProfileNameOk

`func (o *WebhookMessageFrom) GetProfileNameOk() (*string, bool)`

GetProfileNameOk returns a tuple with the ProfileName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProfileName

`func (o *WebhookMessageFrom) SetProfileName(v string)`

SetProfileName sets ProfileName field to given value.


### GetLanguage

`func (o *WebhookMessageFrom) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *WebhookMessageFrom) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *WebhookMessageFrom) SetLanguage(v string)`

SetLanguage sets Language field to given value.

### HasLanguage

`func (o *WebhookMessageFrom) HasLanguage() bool`

HasLanguage returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


