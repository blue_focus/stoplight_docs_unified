# \MiscApi

All URIs are relative to *https://api.unified.chatwerk.de/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetMessengerSettings**](MiscApi.md#GetMessengerSettings) | **Get** /gateways/{gatewayId}/messenger-settings | Get messenger settings
[**GetMessengersLimits**](MiscApi.md#GetMessengersLimits) | **Get** /messengers-limits | Get messengers limits
[**SetMessengerSettings**](MiscApi.md#SetMessengerSettings) | **Patch** /gateways/{gatewayId}/messenger-settings | Set messenger settings



## GetMessengerSettings

> MessengerSettings GetMessengerSettings(ctx, gatewayId).Execute()

Get messenger settings



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MiscApi.GetMessengerSettings(context.Background(), gatewayId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MiscApi.GetMessengerSettings``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMessengerSettings`: MessengerSettings
    fmt.Fprintf(os.Stdout, "Response from `MiscApi.GetMessengerSettings`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetMessengerSettingsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**MessengerSettings**](MessengerSettings.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMessengersLimits

> MessengerLimits GetMessengersLimits(ctx).Execute()

Get messengers limits



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MiscApi.GetMessengersLimits(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MiscApi.GetMessengersLimits``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMessengersLimits`: MessengerLimits
    fmt.Fprintf(os.Stdout, "Response from `MiscApi.GetMessengersLimits`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetMessengersLimitsRequest struct via the builder pattern


### Return type

[**MessengerLimits**](MessengerLimits.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetMessengerSettings

> SetMessengerSettings(ctx, gatewayId).MessengerSettings(messengerSettings).Execute()

Set messenger settings



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    messengerSettings := *openapiclient.NewMessengerSettings() // MessengerSettings |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MiscApi.SetMessengerSettings(context.Background(), gatewayId).MessengerSettings(messengerSettings).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MiscApi.SetMessengerSettings``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiSetMessengerSettingsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **messengerSettings** | [**MessengerSettings**](MessengerSettings.md) |  | 

### Return type

 (empty response body)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

