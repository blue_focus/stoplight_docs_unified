# MessengerLimitsMessengerLimits

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MaxTextLength** | **int32** |  | 
**SessionTimeoutSeconds** | **int32** | 0 means no session timeout | 
**Type** | [**Messenger**](Messenger.md) |  | 
**Provider** | Pointer to [**Provider**](Provider.md) |  | [optional] 
**MessageTypes** | [**[]MessengerLimitsMessageTypes**](MessengerLimitsMessageTypes.md) |  | 

## Methods

### NewMessengerLimitsMessengerLimits

`func NewMessengerLimitsMessengerLimits(maxTextLength int32, sessionTimeoutSeconds int32, type_ Messenger, messageTypes []MessengerLimitsMessageTypes, ) *MessengerLimitsMessengerLimits`

NewMessengerLimitsMessengerLimits instantiates a new MessengerLimitsMessengerLimits object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessengerLimitsMessengerLimitsWithDefaults

`func NewMessengerLimitsMessengerLimitsWithDefaults() *MessengerLimitsMessengerLimits`

NewMessengerLimitsMessengerLimitsWithDefaults instantiates a new MessengerLimitsMessengerLimits object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMaxTextLength

`func (o *MessengerLimitsMessengerLimits) GetMaxTextLength() int32`

GetMaxTextLength returns the MaxTextLength field if non-nil, zero value otherwise.

### GetMaxTextLengthOk

`func (o *MessengerLimitsMessengerLimits) GetMaxTextLengthOk() (*int32, bool)`

GetMaxTextLengthOk returns a tuple with the MaxTextLength field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxTextLength

`func (o *MessengerLimitsMessengerLimits) SetMaxTextLength(v int32)`

SetMaxTextLength sets MaxTextLength field to given value.


### GetSessionTimeoutSeconds

`func (o *MessengerLimitsMessengerLimits) GetSessionTimeoutSeconds() int32`

GetSessionTimeoutSeconds returns the SessionTimeoutSeconds field if non-nil, zero value otherwise.

### GetSessionTimeoutSecondsOk

`func (o *MessengerLimitsMessengerLimits) GetSessionTimeoutSecondsOk() (*int32, bool)`

GetSessionTimeoutSecondsOk returns a tuple with the SessionTimeoutSeconds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSessionTimeoutSeconds

`func (o *MessengerLimitsMessengerLimits) SetSessionTimeoutSeconds(v int32)`

SetSessionTimeoutSeconds sets SessionTimeoutSeconds field to given value.


### GetType

`func (o *MessengerLimitsMessengerLimits) GetType() Messenger`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MessengerLimitsMessengerLimits) GetTypeOk() (*Messenger, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MessengerLimitsMessengerLimits) SetType(v Messenger)`

SetType sets Type field to given value.


### GetProvider

`func (o *MessengerLimitsMessengerLimits) GetProvider() Provider`

GetProvider returns the Provider field if non-nil, zero value otherwise.

### GetProviderOk

`func (o *MessengerLimitsMessengerLimits) GetProviderOk() (*Provider, bool)`

GetProviderOk returns a tuple with the Provider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProvider

`func (o *MessengerLimitsMessengerLimits) SetProvider(v Provider)`

SetProvider sets Provider field to given value.

### HasProvider

`func (o *MessengerLimitsMessengerLimits) HasProvider() bool`

HasProvider returns a boolean if a field has been set.

### GetMessageTypes

`func (o *MessengerLimitsMessengerLimits) GetMessageTypes() []MessengerLimitsMessageTypes`

GetMessageTypes returns the MessageTypes field if non-nil, zero value otherwise.

### GetMessageTypesOk

`func (o *MessengerLimitsMessengerLimits) GetMessageTypesOk() (*[]MessengerLimitsMessageTypes, bool)`

GetMessageTypesOk returns a tuple with the MessageTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageTypes

`func (o *MessengerLimitsMessengerLimits) SetMessageTypes(v []MessengerLimitsMessageTypes)`

SetMessageTypes sets MessageTypes field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


