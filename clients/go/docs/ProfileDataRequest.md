# ProfileDataRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContactId** | **string** |  | 

## Methods

### NewProfileDataRequest

`func NewProfileDataRequest(contactId string, ) *ProfileDataRequest`

NewProfileDataRequest instantiates a new ProfileDataRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProfileDataRequestWithDefaults

`func NewProfileDataRequestWithDefaults() *ProfileDataRequest`

NewProfileDataRequestWithDefaults instantiates a new ProfileDataRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContactId

`func (o *ProfileDataRequest) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *ProfileDataRequest) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *ProfileDataRequest) SetContactId(v string)`

SetContactId sets ContactId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


