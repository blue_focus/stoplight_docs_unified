# MessengerLimits

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MessengerLimits** | [**[]MessengerLimitsMessengerLimits**](MessengerLimitsMessengerLimits.md) |  | 

## Methods

### NewMessengerLimits

`func NewMessengerLimits(messengerLimits []MessengerLimitsMessengerLimits, ) *MessengerLimits`

NewMessengerLimits instantiates a new MessengerLimits object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessengerLimitsWithDefaults

`func NewMessengerLimitsWithDefaults() *MessengerLimits`

NewMessengerLimitsWithDefaults instantiates a new MessengerLimits object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessengerLimits

`func (o *MessengerLimits) GetMessengerLimits() []MessengerLimitsMessengerLimits`

GetMessengerLimits returns the MessengerLimits field if non-nil, zero value otherwise.

### GetMessengerLimitsOk

`func (o *MessengerLimits) GetMessengerLimitsOk() (*[]MessengerLimitsMessengerLimits, bool)`

GetMessengerLimitsOk returns a tuple with the MessengerLimits field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerLimits

`func (o *MessengerLimits) SetMessengerLimits(v []MessengerLimitsMessengerLimits)`

SetMessengerLimits sets MessengerLimits field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


