# WebhookMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Type** | [**MessageType**](MessageType.md) |  | 
**IsEdit** | Pointer to **bool** |  | [optional] 
**Direction** | Pointer to [**MessageDirection**](MessageDirection.md) |  | [optional] 
**From** | [**WebhookMessageFrom**](WebhookMessageFrom.md) |  | 
**Chat** | [**WebhookMessageChat**](WebhookMessageChat.md) |  | 
**Text** | **string** |  | 
**Images** | Pointer to [**[]WebhookMessageImage**](WebhookMessageImage.md) |  | [optional] 
**Videos** | Pointer to [**[]WebhookMessageVideo**](WebhookMessageVideo.md) |  | [optional] 
**Documents** | Pointer to [**[]WebhookMessageDocument**](WebhookMessageDocument.md) |  | [optional] 
**Audios** | Pointer to [**[]WebhookMessageAudio**](WebhookMessageAudio.md) |  | [optional] 
**Contacts** | Pointer to [**[]Contact**](Contact.md) |  | [optional] 
**Location** | Pointer to [**Location**](Location.md) |  | [optional] 
**Errors** | Pointer to [**[]WebhookError**](WebhookError.md) |  | [optional] 
**Timestamp** | **int32** |  | 
**Context** | Pointer to [**MessageContext**](MessageContext.md) |  | [optional] 
**Reaction** | Pointer to [**Reaction**](Reaction.md) |  | [optional] 
**Historical** | Pointer to **bool** |  | [optional] 
**Status** | Pointer to [**MessageStatus**](MessageStatus.md) |  | [optional] 

## Methods

### NewWebhookMessage

`func NewWebhookMessage(id string, type_ MessageType, from WebhookMessageFrom, chat WebhookMessageChat, text string, timestamp int32, ) *WebhookMessage`

NewWebhookMessage instantiates a new WebhookMessage object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookMessageWithDefaults

`func NewWebhookMessageWithDefaults() *WebhookMessage`

NewWebhookMessageWithDefaults instantiates a new WebhookMessage object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WebhookMessage) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WebhookMessage) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WebhookMessage) SetId(v string)`

SetId sets Id field to given value.


### GetType

`func (o *WebhookMessage) GetType() MessageType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WebhookMessage) GetTypeOk() (*MessageType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WebhookMessage) SetType(v MessageType)`

SetType sets Type field to given value.


### GetIsEdit

`func (o *WebhookMessage) GetIsEdit() bool`

GetIsEdit returns the IsEdit field if non-nil, zero value otherwise.

### GetIsEditOk

`func (o *WebhookMessage) GetIsEditOk() (*bool, bool)`

GetIsEditOk returns a tuple with the IsEdit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsEdit

`func (o *WebhookMessage) SetIsEdit(v bool)`

SetIsEdit sets IsEdit field to given value.

### HasIsEdit

`func (o *WebhookMessage) HasIsEdit() bool`

HasIsEdit returns a boolean if a field has been set.

### GetDirection

`func (o *WebhookMessage) GetDirection() MessageDirection`

GetDirection returns the Direction field if non-nil, zero value otherwise.

### GetDirectionOk

`func (o *WebhookMessage) GetDirectionOk() (*MessageDirection, bool)`

GetDirectionOk returns a tuple with the Direction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDirection

`func (o *WebhookMessage) SetDirection(v MessageDirection)`

SetDirection sets Direction field to given value.

### HasDirection

`func (o *WebhookMessage) HasDirection() bool`

HasDirection returns a boolean if a field has been set.

### GetFrom

`func (o *WebhookMessage) GetFrom() WebhookMessageFrom`

GetFrom returns the From field if non-nil, zero value otherwise.

### GetFromOk

`func (o *WebhookMessage) GetFromOk() (*WebhookMessageFrom, bool)`

GetFromOk returns a tuple with the From field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFrom

`func (o *WebhookMessage) SetFrom(v WebhookMessageFrom)`

SetFrom sets From field to given value.


### GetChat

`func (o *WebhookMessage) GetChat() WebhookMessageChat`

GetChat returns the Chat field if non-nil, zero value otherwise.

### GetChatOk

`func (o *WebhookMessage) GetChatOk() (*WebhookMessageChat, bool)`

GetChatOk returns a tuple with the Chat field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChat

`func (o *WebhookMessage) SetChat(v WebhookMessageChat)`

SetChat sets Chat field to given value.


### GetText

`func (o *WebhookMessage) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *WebhookMessage) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *WebhookMessage) SetText(v string)`

SetText sets Text field to given value.


### GetImages

`func (o *WebhookMessage) GetImages() []WebhookMessageImage`

GetImages returns the Images field if non-nil, zero value otherwise.

### GetImagesOk

`func (o *WebhookMessage) GetImagesOk() (*[]WebhookMessageImage, bool)`

GetImagesOk returns a tuple with the Images field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetImages

`func (o *WebhookMessage) SetImages(v []WebhookMessageImage)`

SetImages sets Images field to given value.

### HasImages

`func (o *WebhookMessage) HasImages() bool`

HasImages returns a boolean if a field has been set.

### GetVideos

`func (o *WebhookMessage) GetVideos() []WebhookMessageVideo`

GetVideos returns the Videos field if non-nil, zero value otherwise.

### GetVideosOk

`func (o *WebhookMessage) GetVideosOk() (*[]WebhookMessageVideo, bool)`

GetVideosOk returns a tuple with the Videos field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideos

`func (o *WebhookMessage) SetVideos(v []WebhookMessageVideo)`

SetVideos sets Videos field to given value.

### HasVideos

`func (o *WebhookMessage) HasVideos() bool`

HasVideos returns a boolean if a field has been set.

### GetDocuments

`func (o *WebhookMessage) GetDocuments() []WebhookMessageDocument`

GetDocuments returns the Documents field if non-nil, zero value otherwise.

### GetDocumentsOk

`func (o *WebhookMessage) GetDocumentsOk() (*[]WebhookMessageDocument, bool)`

GetDocumentsOk returns a tuple with the Documents field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDocuments

`func (o *WebhookMessage) SetDocuments(v []WebhookMessageDocument)`

SetDocuments sets Documents field to given value.

### HasDocuments

`func (o *WebhookMessage) HasDocuments() bool`

HasDocuments returns a boolean if a field has been set.

### GetAudios

`func (o *WebhookMessage) GetAudios() []WebhookMessageAudio`

GetAudios returns the Audios field if non-nil, zero value otherwise.

### GetAudiosOk

`func (o *WebhookMessage) GetAudiosOk() (*[]WebhookMessageAudio, bool)`

GetAudiosOk returns a tuple with the Audios field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAudios

`func (o *WebhookMessage) SetAudios(v []WebhookMessageAudio)`

SetAudios sets Audios field to given value.

### HasAudios

`func (o *WebhookMessage) HasAudios() bool`

HasAudios returns a boolean if a field has been set.

### GetContacts

`func (o *WebhookMessage) GetContacts() []Contact`

GetContacts returns the Contacts field if non-nil, zero value otherwise.

### GetContactsOk

`func (o *WebhookMessage) GetContactsOk() (*[]Contact, bool)`

GetContactsOk returns a tuple with the Contacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContacts

`func (o *WebhookMessage) SetContacts(v []Contact)`

SetContacts sets Contacts field to given value.

### HasContacts

`func (o *WebhookMessage) HasContacts() bool`

HasContacts returns a boolean if a field has been set.

### GetLocation

`func (o *WebhookMessage) GetLocation() Location`

GetLocation returns the Location field if non-nil, zero value otherwise.

### GetLocationOk

`func (o *WebhookMessage) GetLocationOk() (*Location, bool)`

GetLocationOk returns a tuple with the Location field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocation

`func (o *WebhookMessage) SetLocation(v Location)`

SetLocation sets Location field to given value.

### HasLocation

`func (o *WebhookMessage) HasLocation() bool`

HasLocation returns a boolean if a field has been set.

### GetErrors

`func (o *WebhookMessage) GetErrors() []WebhookError`

GetErrors returns the Errors field if non-nil, zero value otherwise.

### GetErrorsOk

`func (o *WebhookMessage) GetErrorsOk() (*[]WebhookError, bool)`

GetErrorsOk returns a tuple with the Errors field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrors

`func (o *WebhookMessage) SetErrors(v []WebhookError)`

SetErrors sets Errors field to given value.

### HasErrors

`func (o *WebhookMessage) HasErrors() bool`

HasErrors returns a boolean if a field has been set.

### GetTimestamp

`func (o *WebhookMessage) GetTimestamp() int32`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *WebhookMessage) GetTimestampOk() (*int32, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *WebhookMessage) SetTimestamp(v int32)`

SetTimestamp sets Timestamp field to given value.


### GetContext

`func (o *WebhookMessage) GetContext() MessageContext`

GetContext returns the Context field if non-nil, zero value otherwise.

### GetContextOk

`func (o *WebhookMessage) GetContextOk() (*MessageContext, bool)`

GetContextOk returns a tuple with the Context field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContext

`func (o *WebhookMessage) SetContext(v MessageContext)`

SetContext sets Context field to given value.

### HasContext

`func (o *WebhookMessage) HasContext() bool`

HasContext returns a boolean if a field has been set.

### GetReaction

`func (o *WebhookMessage) GetReaction() Reaction`

GetReaction returns the Reaction field if non-nil, zero value otherwise.

### GetReactionOk

`func (o *WebhookMessage) GetReactionOk() (*Reaction, bool)`

GetReactionOk returns a tuple with the Reaction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReaction

`func (o *WebhookMessage) SetReaction(v Reaction)`

SetReaction sets Reaction field to given value.

### HasReaction

`func (o *WebhookMessage) HasReaction() bool`

HasReaction returns a boolean if a field has been set.

### GetHistorical

`func (o *WebhookMessage) GetHistorical() bool`

GetHistorical returns the Historical field if non-nil, zero value otherwise.

### GetHistoricalOk

`func (o *WebhookMessage) GetHistoricalOk() (*bool, bool)`

GetHistoricalOk returns a tuple with the Historical field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHistorical

`func (o *WebhookMessage) SetHistorical(v bool)`

SetHistorical sets Historical field to given value.

### HasHistorical

`func (o *WebhookMessage) HasHistorical() bool`

HasHistorical returns a boolean if a field has been set.

### GetStatus

`func (o *WebhookMessage) GetStatus() MessageStatus`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *WebhookMessage) GetStatusOk() (*MessageStatus, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *WebhookMessage) SetStatus(v MessageStatus)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *WebhookMessage) HasStatus() bool`

HasStatus returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


