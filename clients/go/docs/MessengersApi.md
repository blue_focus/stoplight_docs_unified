# \MessengersApi

All URIs are relative to *https://api.unified.chatwerk.de/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CheckContacts**](MessengersApi.md#CheckContacts) | **Post** /gateways/{gatewayId}/contacts | Check contacts
[**GetMedia**](MessengersApi.md#GetMedia) | **Get** /gateways/{gatewayId}/media/{mediaId} | Get media
[**GetProfileInfo**](MessengersApi.md#GetProfileInfo) | **Post** /gateways/{gatewayId}/profile-info | Get contact profile info
[**GetProfilePhotos**](MessengersApi.md#GetProfilePhotos) | **Post** /gateways/{gatewayId}/profile-photos | Get contact profile photos
[**SendMessage**](MessengersApi.md#SendMessage) | **Post** /gateways/{gatewayId}/message | Send message
[**UploadMedia**](MessengersApi.md#UploadMedia) | **Post** /gateways/{gatewayId}/media | Upload media



## CheckContacts

> CheckContactsResponse CheckContacts(ctx, gatewayId).CheckContactsRequest(checkContactsRequest).Execute()

Check contacts



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    checkContactsRequest := *openapiclient.NewCheckContactsRequest([]string{"Contacts_example"}) // CheckContactsRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MessengersApi.CheckContacts(context.Background(), gatewayId).CheckContactsRequest(checkContactsRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MessengersApi.CheckContacts``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CheckContacts`: CheckContactsResponse
    fmt.Fprintf(os.Stdout, "Response from `MessengersApi.CheckContacts`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiCheckContactsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **checkContactsRequest** | [**CheckContactsRequest**](CheckContactsRequest.md) |  | 

### Return type

[**CheckContactsResponse**](CheckContactsResponse.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMedia

> *os.File GetMedia(ctx, gatewayId, mediaId).Execute()

Get media



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    mediaId := "mediaId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MessengersApi.GetMedia(context.Background(), gatewayId, mediaId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MessengersApi.GetMedia``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetMedia`: *os.File
    fmt.Fprintf(os.Stdout, "Response from `MessengersApi.GetMedia`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 
**mediaId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetMediaRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[***os.File**](*os.File.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetProfileInfo

> ProfileInfo GetProfileInfo(ctx, gatewayId).ProfileDataRequest(profileDataRequest).Execute()

Get contact profile info



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    profileDataRequest := *openapiclient.NewProfileDataRequest("ContactId_example") // ProfileDataRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MessengersApi.GetProfileInfo(context.Background(), gatewayId).ProfileDataRequest(profileDataRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MessengersApi.GetProfileInfo``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetProfileInfo`: ProfileInfo
    fmt.Fprintf(os.Stdout, "Response from `MessengersApi.GetProfileInfo`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetProfileInfoRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **profileDataRequest** | [**ProfileDataRequest**](ProfileDataRequest.md) |  | 

### Return type

[**ProfileInfo**](ProfileInfo.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetProfilePhotos

> ProfilePhotos GetProfilePhotos(ctx, gatewayId).ProfileDataRequest(profileDataRequest).Execute()

Get contact profile photos



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    profileDataRequest := *openapiclient.NewProfileDataRequest("ContactId_example") // ProfileDataRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MessengersApi.GetProfilePhotos(context.Background(), gatewayId).ProfileDataRequest(profileDataRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MessengersApi.GetProfilePhotos``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetProfilePhotos`: ProfilePhotos
    fmt.Fprintf(os.Stdout, "Response from `MessengersApi.GetProfilePhotos`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetProfilePhotosRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **profileDataRequest** | [**ProfileDataRequest**](ProfileDataRequest.md) |  | 

### Return type

[**ProfilePhotos**](ProfilePhotos.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SendMessage

> SendMessageResponse SendMessage(ctx, gatewayId).SendMessageRequest(sendMessageRequest).Execute()

Send message



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    sendMessageRequest := *openapiclient.NewSendMessageRequest("To_example", openapiclient.MessageType("text")) // SendMessageRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MessengersApi.SendMessage(context.Background(), gatewayId).SendMessageRequest(sendMessageRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MessengersApi.SendMessage``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SendMessage`: SendMessageResponse
    fmt.Fprintf(os.Stdout, "Response from `MessengersApi.SendMessage`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiSendMessageRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **sendMessageRequest** | [**SendMessageRequest**](SendMessageRequest.md) |  | 

### Return type

[**SendMessageResponse**](SendMessageResponse.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UploadMedia

> UploadMediaResponse UploadMedia(ctx, gatewayId).ContentType(contentType).Body(body).Execute()

Upload media

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    contentType := "contentType_example" // string | 
    body := os.NewFile(1234, "some_file") // *os.File |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.MessengersApi.UploadMedia(context.Background(), gatewayId).ContentType(contentType).Body(body).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `MessengersApi.UploadMedia``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UploadMedia`: UploadMediaResponse
    fmt.Fprintf(os.Stdout, "Response from `MessengersApi.UploadMedia`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiUploadMediaRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **contentType** | **string** |  | 
 **body** | ***os.File** |  | 

### Return type

[**UploadMediaResponse**](UploadMediaResponse.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

