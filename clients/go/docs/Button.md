# Button

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to [**ButtonType**](ButtonType.md) |  | [optional] 
**Text** | Pointer to **string** |  | [optional] 
**Payload** | Pointer to **string** |  | [optional] 

## Methods

### NewButton

`func NewButton() *Button`

NewButton instantiates a new Button object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewButtonWithDefaults

`func NewButtonWithDefaults() *Button`

NewButtonWithDefaults instantiates a new Button object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *Button) GetType() ButtonType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Button) GetTypeOk() (*ButtonType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Button) SetType(v ButtonType)`

SetType sets Type field to given value.

### HasType

`func (o *Button) HasType() bool`

HasType returns a boolean if a field has been set.

### GetText

`func (o *Button) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *Button) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *Button) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *Button) HasText() bool`

HasText returns a boolean if a field has been set.

### GetPayload

`func (o *Button) GetPayload() string`

GetPayload returns the Payload field if non-nil, zero value otherwise.

### GetPayloadOk

`func (o *Button) GetPayloadOk() (*string, bool)`

GetPayloadOk returns a tuple with the Payload field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayload

`func (o *Button) SetPayload(v string)`

SetPayload sets Payload field to given value.

### HasPayload

`func (o *Button) HasPayload() bool`

HasPayload returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


