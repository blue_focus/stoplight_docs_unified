# WebhookDataGateway

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Provider** | [**Provider**](Provider.md) |  | 
**Messenger** | [**Messenger**](Messenger.md) |  | 
**Gateway** | Pointer to **string** |  | [optional] 

## Methods

### NewWebhookDataGateway

`func NewWebhookDataGateway(id string, provider Provider, messenger Messenger, ) *WebhookDataGateway`

NewWebhookDataGateway instantiates a new WebhookDataGateway object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookDataGatewayWithDefaults

`func NewWebhookDataGatewayWithDefaults() *WebhookDataGateway`

NewWebhookDataGatewayWithDefaults instantiates a new WebhookDataGateway object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WebhookDataGateway) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WebhookDataGateway) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WebhookDataGateway) SetId(v string)`

SetId sets Id field to given value.


### GetProvider

`func (o *WebhookDataGateway) GetProvider() Provider`

GetProvider returns the Provider field if non-nil, zero value otherwise.

### GetProviderOk

`func (o *WebhookDataGateway) GetProviderOk() (*Provider, bool)`

GetProviderOk returns a tuple with the Provider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProvider

`func (o *WebhookDataGateway) SetProvider(v Provider)`

SetProvider sets Provider field to given value.


### GetMessenger

`func (o *WebhookDataGateway) GetMessenger() Messenger`

GetMessenger returns the Messenger field if non-nil, zero value otherwise.

### GetMessengerOk

`func (o *WebhookDataGateway) GetMessengerOk() (*Messenger, bool)`

GetMessengerOk returns a tuple with the Messenger field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessenger

`func (o *WebhookDataGateway) SetMessenger(v Messenger)`

SetMessenger sets Messenger field to given value.


### GetGateway

`func (o *WebhookDataGateway) GetGateway() string`

GetGateway returns the Gateway field if non-nil, zero value otherwise.

### GetGatewayOk

`func (o *WebhookDataGateway) GetGatewayOk() (*string, bool)`

GetGatewayOk returns a tuple with the Gateway field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGateway

`func (o *WebhookDataGateway) SetGateway(v string)`

SetGateway sets Gateway field to given value.

### HasGateway

`func (o *WebhookDataGateway) HasGateway() bool`

HasGateway returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


