# WebhookSystemMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | [**SystemMessageType**](SystemMessageType.md) |  | 
**Chat** | [**WebhookMessageChat**](WebhookMessageChat.md) |  | 
**MigrateToNewChat** | Pointer to **string** |  | [optional] 
**ChatContact** | Pointer to [**ChatContact**](ChatContact.md) |  | [optional] 
**Timestamp** | Pointer to **int32** |  | [optional] 
**Callbacks** | Pointer to [**[]Callback**](Callback.md) |  | [optional] 
**Value** | Pointer to **string** |  | [optional] 
**Text** | Pointer to **string** |  | [optional] 

## Methods

### NewWebhookSystemMessage

`func NewWebhookSystemMessage(type_ SystemMessageType, chat WebhookMessageChat, ) *WebhookSystemMessage`

NewWebhookSystemMessage instantiates a new WebhookSystemMessage object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookSystemMessageWithDefaults

`func NewWebhookSystemMessageWithDefaults() *WebhookSystemMessage`

NewWebhookSystemMessageWithDefaults instantiates a new WebhookSystemMessage object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *WebhookSystemMessage) GetType() SystemMessageType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WebhookSystemMessage) GetTypeOk() (*SystemMessageType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WebhookSystemMessage) SetType(v SystemMessageType)`

SetType sets Type field to given value.


### GetChat

`func (o *WebhookSystemMessage) GetChat() WebhookMessageChat`

GetChat returns the Chat field if non-nil, zero value otherwise.

### GetChatOk

`func (o *WebhookSystemMessage) GetChatOk() (*WebhookMessageChat, bool)`

GetChatOk returns a tuple with the Chat field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChat

`func (o *WebhookSystemMessage) SetChat(v WebhookMessageChat)`

SetChat sets Chat field to given value.


### GetMigrateToNewChat

`func (o *WebhookSystemMessage) GetMigrateToNewChat() string`

GetMigrateToNewChat returns the MigrateToNewChat field if non-nil, zero value otherwise.

### GetMigrateToNewChatOk

`func (o *WebhookSystemMessage) GetMigrateToNewChatOk() (*string, bool)`

GetMigrateToNewChatOk returns a tuple with the MigrateToNewChat field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMigrateToNewChat

`func (o *WebhookSystemMessage) SetMigrateToNewChat(v string)`

SetMigrateToNewChat sets MigrateToNewChat field to given value.

### HasMigrateToNewChat

`func (o *WebhookSystemMessage) HasMigrateToNewChat() bool`

HasMigrateToNewChat returns a boolean if a field has been set.

### GetChatContact

`func (o *WebhookSystemMessage) GetChatContact() ChatContact`

GetChatContact returns the ChatContact field if non-nil, zero value otherwise.

### GetChatContactOk

`func (o *WebhookSystemMessage) GetChatContactOk() (*ChatContact, bool)`

GetChatContactOk returns a tuple with the ChatContact field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChatContact

`func (o *WebhookSystemMessage) SetChatContact(v ChatContact)`

SetChatContact sets ChatContact field to given value.

### HasChatContact

`func (o *WebhookSystemMessage) HasChatContact() bool`

HasChatContact returns a boolean if a field has been set.

### GetTimestamp

`func (o *WebhookSystemMessage) GetTimestamp() int32`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *WebhookSystemMessage) GetTimestampOk() (*int32, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *WebhookSystemMessage) SetTimestamp(v int32)`

SetTimestamp sets Timestamp field to given value.

### HasTimestamp

`func (o *WebhookSystemMessage) HasTimestamp() bool`

HasTimestamp returns a boolean if a field has been set.

### GetCallbacks

`func (o *WebhookSystemMessage) GetCallbacks() []Callback`

GetCallbacks returns the Callbacks field if non-nil, zero value otherwise.

### GetCallbacksOk

`func (o *WebhookSystemMessage) GetCallbacksOk() (*[]Callback, bool)`

GetCallbacksOk returns a tuple with the Callbacks field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCallbacks

`func (o *WebhookSystemMessage) SetCallbacks(v []Callback)`

SetCallbacks sets Callbacks field to given value.

### HasCallbacks

`func (o *WebhookSystemMessage) HasCallbacks() bool`

HasCallbacks returns a boolean if a field has been set.

### GetValue

`func (o *WebhookSystemMessage) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *WebhookSystemMessage) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *WebhookSystemMessage) SetValue(v string)`

SetValue sets Value field to given value.

### HasValue

`func (o *WebhookSystemMessage) HasValue() bool`

HasValue returns a boolean if a field has been set.

### GetText

`func (o *WebhookSystemMessage) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *WebhookSystemMessage) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *WebhookSystemMessage) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *WebhookSystemMessage) HasText() bool`

HasText returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


