# Contact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Vcard** | Pointer to **string** |  | [optional] 
**Name** | Pointer to [**ContactName**](ContactName.md) |  | [optional] 
**Org** | Pointer to [**ContactOrg**](ContactOrg.md) |  | [optional] 
**Birthday** | Pointer to **string** |  | [optional] 
**Photo** | Pointer to [**ContactPhoto**](ContactPhoto.md) |  | [optional] 
**Phones** | Pointer to [**[]ContactPhone**](ContactPhone.md) |  | [optional] 
**Addresses** | Pointer to [**[]ContactAddress**](ContactAddress.md) |  | [optional] 
**Emails** | Pointer to [**[]ContactEmail**](ContactEmail.md) |  | [optional] 
**Urls** | Pointer to [**[]ContactUrl**](ContactUrl.md) |  | [optional] 
**Ims** | Pointer to [**[]ContactIM**](ContactIM.md) |  | [optional] 

## Methods

### NewContact

`func NewContact() *Contact`

NewContact instantiates a new Contact object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactWithDefaults

`func NewContactWithDefaults() *Contact`

NewContactWithDefaults instantiates a new Contact object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVcard

`func (o *Contact) GetVcard() string`

GetVcard returns the Vcard field if non-nil, zero value otherwise.

### GetVcardOk

`func (o *Contact) GetVcardOk() (*string, bool)`

GetVcardOk returns a tuple with the Vcard field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVcard

`func (o *Contact) SetVcard(v string)`

SetVcard sets Vcard field to given value.

### HasVcard

`func (o *Contact) HasVcard() bool`

HasVcard returns a boolean if a field has been set.

### GetName

`func (o *Contact) GetName() ContactName`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Contact) GetNameOk() (*ContactName, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Contact) SetName(v ContactName)`

SetName sets Name field to given value.

### HasName

`func (o *Contact) HasName() bool`

HasName returns a boolean if a field has been set.

### GetOrg

`func (o *Contact) GetOrg() ContactOrg`

GetOrg returns the Org field if non-nil, zero value otherwise.

### GetOrgOk

`func (o *Contact) GetOrgOk() (*ContactOrg, bool)`

GetOrgOk returns a tuple with the Org field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOrg

`func (o *Contact) SetOrg(v ContactOrg)`

SetOrg sets Org field to given value.

### HasOrg

`func (o *Contact) HasOrg() bool`

HasOrg returns a boolean if a field has been set.

### GetBirthday

`func (o *Contact) GetBirthday() string`

GetBirthday returns the Birthday field if non-nil, zero value otherwise.

### GetBirthdayOk

`func (o *Contact) GetBirthdayOk() (*string, bool)`

GetBirthdayOk returns a tuple with the Birthday field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBirthday

`func (o *Contact) SetBirthday(v string)`

SetBirthday sets Birthday field to given value.

### HasBirthday

`func (o *Contact) HasBirthday() bool`

HasBirthday returns a boolean if a field has been set.

### GetPhoto

`func (o *Contact) GetPhoto() ContactPhoto`

GetPhoto returns the Photo field if non-nil, zero value otherwise.

### GetPhotoOk

`func (o *Contact) GetPhotoOk() (*ContactPhoto, bool)`

GetPhotoOk returns a tuple with the Photo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhoto

`func (o *Contact) SetPhoto(v ContactPhoto)`

SetPhoto sets Photo field to given value.

### HasPhoto

`func (o *Contact) HasPhoto() bool`

HasPhoto returns a boolean if a field has been set.

### GetPhones

`func (o *Contact) GetPhones() []ContactPhone`

GetPhones returns the Phones field if non-nil, zero value otherwise.

### GetPhonesOk

`func (o *Contact) GetPhonesOk() (*[]ContactPhone, bool)`

GetPhonesOk returns a tuple with the Phones field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhones

`func (o *Contact) SetPhones(v []ContactPhone)`

SetPhones sets Phones field to given value.

### HasPhones

`func (o *Contact) HasPhones() bool`

HasPhones returns a boolean if a field has been set.

### GetAddresses

`func (o *Contact) GetAddresses() []ContactAddress`

GetAddresses returns the Addresses field if non-nil, zero value otherwise.

### GetAddressesOk

`func (o *Contact) GetAddressesOk() (*[]ContactAddress, bool)`

GetAddressesOk returns a tuple with the Addresses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddresses

`func (o *Contact) SetAddresses(v []ContactAddress)`

SetAddresses sets Addresses field to given value.

### HasAddresses

`func (o *Contact) HasAddresses() bool`

HasAddresses returns a boolean if a field has been set.

### GetEmails

`func (o *Contact) GetEmails() []ContactEmail`

GetEmails returns the Emails field if non-nil, zero value otherwise.

### GetEmailsOk

`func (o *Contact) GetEmailsOk() (*[]ContactEmail, bool)`

GetEmailsOk returns a tuple with the Emails field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmails

`func (o *Contact) SetEmails(v []ContactEmail)`

SetEmails sets Emails field to given value.

### HasEmails

`func (o *Contact) HasEmails() bool`

HasEmails returns a boolean if a field has been set.

### GetUrls

`func (o *Contact) GetUrls() []ContactUrl`

GetUrls returns the Urls field if non-nil, zero value otherwise.

### GetUrlsOk

`func (o *Contact) GetUrlsOk() (*[]ContactUrl, bool)`

GetUrlsOk returns a tuple with the Urls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrls

`func (o *Contact) SetUrls(v []ContactUrl)`

SetUrls sets Urls field to given value.

### HasUrls

`func (o *Contact) HasUrls() bool`

HasUrls returns a boolean if a field has been set.

### GetIms

`func (o *Contact) GetIms() []ContactIM`

GetIms returns the Ims field if non-nil, zero value otherwise.

### GetImsOk

`func (o *Contact) GetImsOk() (*[]ContactIM, bool)`

GetImsOk returns a tuple with the Ims field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIms

`func (o *Contact) SetIms(v []ContactIM)`

SetIms sets Ims field to given value.

### HasIms

`func (o *Contact) HasIms() bool`

HasIms returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


