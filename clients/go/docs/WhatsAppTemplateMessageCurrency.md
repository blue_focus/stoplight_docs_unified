# WhatsAppTemplateMessageCurrency

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FallbackValue** | Pointer to **string** |  | [optional] 
**Code** | Pointer to **string** |  | [optional] 
**Amount1000** | Pointer to **int32** |  | [optional] 

## Methods

### NewWhatsAppTemplateMessageCurrency

`func NewWhatsAppTemplateMessageCurrency() *WhatsAppTemplateMessageCurrency`

NewWhatsAppTemplateMessageCurrency instantiates a new WhatsAppTemplateMessageCurrency object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMessageCurrencyWithDefaults

`func NewWhatsAppTemplateMessageCurrencyWithDefaults() *WhatsAppTemplateMessageCurrency`

NewWhatsAppTemplateMessageCurrencyWithDefaults instantiates a new WhatsAppTemplateMessageCurrency object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFallbackValue

`func (o *WhatsAppTemplateMessageCurrency) GetFallbackValue() string`

GetFallbackValue returns the FallbackValue field if non-nil, zero value otherwise.

### GetFallbackValueOk

`func (o *WhatsAppTemplateMessageCurrency) GetFallbackValueOk() (*string, bool)`

GetFallbackValueOk returns a tuple with the FallbackValue field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFallbackValue

`func (o *WhatsAppTemplateMessageCurrency) SetFallbackValue(v string)`

SetFallbackValue sets FallbackValue field to given value.

### HasFallbackValue

`func (o *WhatsAppTemplateMessageCurrency) HasFallbackValue() bool`

HasFallbackValue returns a boolean if a field has been set.

### GetCode

`func (o *WhatsAppTemplateMessageCurrency) GetCode() string`

GetCode returns the Code field if non-nil, zero value otherwise.

### GetCodeOk

`func (o *WhatsAppTemplateMessageCurrency) GetCodeOk() (*string, bool)`

GetCodeOk returns a tuple with the Code field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCode

`func (o *WhatsAppTemplateMessageCurrency) SetCode(v string)`

SetCode sets Code field to given value.

### HasCode

`func (o *WhatsAppTemplateMessageCurrency) HasCode() bool`

HasCode returns a boolean if a field has been set.

### GetAmount1000

`func (o *WhatsAppTemplateMessageCurrency) GetAmount1000() int32`

GetAmount1000 returns the Amount1000 field if non-nil, zero value otherwise.

### GetAmount1000Ok

`func (o *WhatsAppTemplateMessageCurrency) GetAmount1000Ok() (*int32, bool)`

GetAmount1000Ok returns a tuple with the Amount1000 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAmount1000

`func (o *WhatsAppTemplateMessageCurrency) SetAmount1000(v int32)`

SetAmount1000 sets Amount1000 field to given value.

### HasAmount1000

`func (o *WhatsAppTemplateMessageCurrency) HasAmount1000() bool`

HasAmount1000 returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


