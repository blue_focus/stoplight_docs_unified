# SystemMessageType

## Enum


* `SYSTEM_MESSAGE_UNKNOWN` (value: `""`)

* `SYSTEM_MESSAGE_MIGRATE_TO_CHAT` (value: `"migrate_to_chat"`)

* `SYSTEM_MESSAGE_CHAT_CONNECTED` (value: `"chat_connected"`)

* `SYSTEM_MESSAGE_CHAT_DISCONNECTED` (value: `"chat_disconnected"`)

* `SYSTEM_MESSAGE_CHAT_CONTACT` (value: `"chat_contact"`)

* `SYSTEM_MESSAGE_CALLBACK` (value: `"callback"`)

* `SYSTEM_MESSAGE_USER_CHANGED_NUMBER` (value: `"user_changed_number"`)

* `SYSTEM_MESSAGE_USER_IDENTITY_CHANGED` (value: `"user_identity_changed"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


