# OfficeHours

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StartTime** | [**OfficeTime**](OfficeTime.md) |  | 
**EndTime** | [**OfficeTime**](OfficeTime.md) |  | 
**TimeZone** | **string** |  | 
**StartDay** | [**DayOfWeek**](DayOfWeek.md) |  | 
**EndDay** | [**DayOfWeek**](DayOfWeek.md) |  | 

## Methods

### NewOfficeHours

`func NewOfficeHours(startTime OfficeTime, endTime OfficeTime, timeZone string, startDay DayOfWeek, endDay DayOfWeek, ) *OfficeHours`

NewOfficeHours instantiates a new OfficeHours object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOfficeHoursWithDefaults

`func NewOfficeHoursWithDefaults() *OfficeHours`

NewOfficeHoursWithDefaults instantiates a new OfficeHours object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStartTime

`func (o *OfficeHours) GetStartTime() OfficeTime`

GetStartTime returns the StartTime field if non-nil, zero value otherwise.

### GetStartTimeOk

`func (o *OfficeHours) GetStartTimeOk() (*OfficeTime, bool)`

GetStartTimeOk returns a tuple with the StartTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartTime

`func (o *OfficeHours) SetStartTime(v OfficeTime)`

SetStartTime sets StartTime field to given value.


### GetEndTime

`func (o *OfficeHours) GetEndTime() OfficeTime`

GetEndTime returns the EndTime field if non-nil, zero value otherwise.

### GetEndTimeOk

`func (o *OfficeHours) GetEndTimeOk() (*OfficeTime, bool)`

GetEndTimeOk returns a tuple with the EndTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEndTime

`func (o *OfficeHours) SetEndTime(v OfficeTime)`

SetEndTime sets EndTime field to given value.


### GetTimeZone

`func (o *OfficeHours) GetTimeZone() string`

GetTimeZone returns the TimeZone field if non-nil, zero value otherwise.

### GetTimeZoneOk

`func (o *OfficeHours) GetTimeZoneOk() (*string, bool)`

GetTimeZoneOk returns a tuple with the TimeZone field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimeZone

`func (o *OfficeHours) SetTimeZone(v string)`

SetTimeZone sets TimeZone field to given value.


### GetStartDay

`func (o *OfficeHours) GetStartDay() DayOfWeek`

GetStartDay returns the StartDay field if non-nil, zero value otherwise.

### GetStartDayOk

`func (o *OfficeHours) GetStartDayOk() (*DayOfWeek, bool)`

GetStartDayOk returns a tuple with the StartDay field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartDay

`func (o *OfficeHours) SetStartDay(v DayOfWeek)`

SetStartDay sets StartDay field to given value.


### GetEndDay

`func (o *OfficeHours) GetEndDay() DayOfWeek`

GetEndDay returns the EndDay field if non-nil, zero value otherwise.

### GetEndDayOk

`func (o *OfficeHours) GetEndDayOk() (*DayOfWeek, bool)`

GetEndDayOk returns a tuple with the EndDay field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEndDay

`func (o *OfficeHours) SetEndDay(v DayOfWeek)`

SetEndDay sets EndDay field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


