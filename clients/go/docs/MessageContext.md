# MessageContext

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MessageId** | Pointer to **string** |  | [optional] 
**ContactId** | Pointer to **string** |  | [optional] 
**Referral** | Pointer to [**WebhookReferral**](WebhookReferral.md) |  | [optional] 

## Methods

### NewMessageContext

`func NewMessageContext() *MessageContext`

NewMessageContext instantiates a new MessageContext object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageContextWithDefaults

`func NewMessageContextWithDefaults() *MessageContext`

NewMessageContextWithDefaults instantiates a new MessageContext object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessageId

`func (o *MessageContext) GetMessageId() string`

GetMessageId returns the MessageId field if non-nil, zero value otherwise.

### GetMessageIdOk

`func (o *MessageContext) GetMessageIdOk() (*string, bool)`

GetMessageIdOk returns a tuple with the MessageId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageId

`func (o *MessageContext) SetMessageId(v string)`

SetMessageId sets MessageId field to given value.

### HasMessageId

`func (o *MessageContext) HasMessageId() bool`

HasMessageId returns a boolean if a field has been set.

### GetContactId

`func (o *MessageContext) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *MessageContext) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *MessageContext) SetContactId(v string)`

SetContactId sets ContactId field to given value.

### HasContactId

`func (o *MessageContext) HasContactId() bool`

HasContactId returns a boolean if a field has been set.

### GetReferral

`func (o *MessageContext) GetReferral() WebhookReferral`

GetReferral returns the Referral field if non-nil, zero value otherwise.

### GetReferralOk

`func (o *MessageContext) GetReferralOk() (*WebhookReferral, bool)`

GetReferralOk returns a tuple with the Referral field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReferral

`func (o *MessageContext) SetReferral(v WebhookReferral)`

SetReferral sets Referral field to given value.

### HasReferral

`func (o *MessageContext) HasReferral() bool`

HasReferral returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


