# Header

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | Pointer to [**HeaderType**](HeaderType.md) |  | [optional] 
**Text** | Pointer to **string** |  | [optional] 
**MediaUrl** | Pointer to **string** |  | [optional] 
**MediaFilename** | Pointer to **string** |  | [optional] 

## Methods

### NewHeader

`func NewHeader() *Header`

NewHeader instantiates a new Header object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewHeaderWithDefaults

`func NewHeaderWithDefaults() *Header`

NewHeaderWithDefaults instantiates a new Header object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *Header) GetType() HeaderType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Header) GetTypeOk() (*HeaderType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Header) SetType(v HeaderType)`

SetType sets Type field to given value.

### HasType

`func (o *Header) HasType() bool`

HasType returns a boolean if a field has been set.

### GetText

`func (o *Header) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *Header) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *Header) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *Header) HasText() bool`

HasText returns a boolean if a field has been set.

### GetMediaUrl

`func (o *Header) GetMediaUrl() string`

GetMediaUrl returns the MediaUrl field if non-nil, zero value otherwise.

### GetMediaUrlOk

`func (o *Header) GetMediaUrlOk() (*string, bool)`

GetMediaUrlOk returns a tuple with the MediaUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaUrl

`func (o *Header) SetMediaUrl(v string)`

SetMediaUrl sets MediaUrl field to given value.

### HasMediaUrl

`func (o *Header) HasMediaUrl() bool`

HasMediaUrl returns a boolean if a field has been set.

### GetMediaFilename

`func (o *Header) GetMediaFilename() string`

GetMediaFilename returns the MediaFilename field if non-nil, zero value otherwise.

### GetMediaFilenameOk

`func (o *Header) GetMediaFilenameOk() (*string, bool)`

GetMediaFilenameOk returns a tuple with the MediaFilename field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaFilename

`func (o *Header) SetMediaFilename(v string)`

SetMediaFilename sets MediaFilename field to given value.

### HasMediaFilename

`func (o *Header) HasMediaFilename() bool`

HasMediaFilename returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


