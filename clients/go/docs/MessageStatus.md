# MessageStatus

## Enum


* `STATUS_ERROR` (value: `"error"`)

* `STATUS_SENT` (value: `"sent"`)

* `STATUS_DELIVERED` (value: `"delivered"`)

* `STATUS_READ` (value: `"read"`)

* `STATUS_PLAYED` (value: `"played"`)

* `STATUS_DELETED` (value: `"deleted"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


