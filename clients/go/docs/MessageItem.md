# MessageItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Text** | Pointer to **string** |  | [optional] 
**Payload** | Pointer to **string** |  | [optional] 
**Description** | Pointer to **string** |  | [optional] 

## Methods

### NewMessageItem

`func NewMessageItem() *MessageItem`

NewMessageItem instantiates a new MessageItem object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessageItemWithDefaults

`func NewMessageItemWithDefaults() *MessageItem`

NewMessageItemWithDefaults instantiates a new MessageItem object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetText

`func (o *MessageItem) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *MessageItem) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *MessageItem) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *MessageItem) HasText() bool`

HasText returns a boolean if a field has been set.

### GetPayload

`func (o *MessageItem) GetPayload() string`

GetPayload returns the Payload field if non-nil, zero value otherwise.

### GetPayloadOk

`func (o *MessageItem) GetPayloadOk() (*string, bool)`

GetPayloadOk returns a tuple with the Payload field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayload

`func (o *MessageItem) SetPayload(v string)`

SetPayload sets Payload field to given value.

### HasPayload

`func (o *MessageItem) HasPayload() bool`

HasPayload returns a boolean if a field has been set.

### GetDescription

`func (o *MessageItem) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *MessageItem) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *MessageItem) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *MessageItem) HasDescription() bool`

HasDescription returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


