# MessengerSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Description** | Pointer to **string** |  | [optional] 
**ProfilePhoto** | Pointer to [**ProfilePhoto**](ProfilePhoto.md) |  | [optional] 
**BusinessProfile** | Pointer to [**BusinessProfile**](BusinessProfile.md) |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**Locale** | Pointer to **string** |  | [optional] 
**WebsiteUrl** | Pointer to **string** |  | [optional] 
**PrivacyPolicyUrl** | Pointer to **string** |  | [optional] 
**ContactPageUrl** | Pointer to **string** |  | [optional] 
**BrandContactName** | Pointer to **string** |  | [optional] 
**BrandContactEmail** | Pointer to **string** |  | [optional] 
**WelcomeMessage** | Pointer to **string** |  | [optional] 
**OfflineMessage** | Pointer to **string** |  | [optional] 
**Domains** | Pointer to **[]string** |  | [optional] 
**RegionCodes** | Pointer to **[]string** |  | [optional] 
**PhoneNumbers** | Pointer to [**[]PhoneNumber**](PhoneNumber.md) |  | [optional] 
**OfficeHours** | Pointer to [**[]OfficeHours**](OfficeHours.md) |  | [optional] 
**WhatsappPhoneNumber** | Pointer to [**WhatsappPhoneNumber**](WhatsappPhoneNumber.md) |  | [optional] 

## Methods

### NewMessengerSettings

`func NewMessengerSettings() *MessengerSettings`

NewMessengerSettings instantiates a new MessengerSettings object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessengerSettingsWithDefaults

`func NewMessengerSettingsWithDefaults() *MessengerSettings`

NewMessengerSettingsWithDefaults instantiates a new MessengerSettings object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDescription

`func (o *MessengerSettings) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *MessengerSettings) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *MessengerSettings) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *MessengerSettings) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetProfilePhoto

`func (o *MessengerSettings) GetProfilePhoto() ProfilePhoto`

GetProfilePhoto returns the ProfilePhoto field if non-nil, zero value otherwise.

### GetProfilePhotoOk

`func (o *MessengerSettings) GetProfilePhotoOk() (*ProfilePhoto, bool)`

GetProfilePhotoOk returns a tuple with the ProfilePhoto field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProfilePhoto

`func (o *MessengerSettings) SetProfilePhoto(v ProfilePhoto)`

SetProfilePhoto sets ProfilePhoto field to given value.

### HasProfilePhoto

`func (o *MessengerSettings) HasProfilePhoto() bool`

HasProfilePhoto returns a boolean if a field has been set.

### GetBusinessProfile

`func (o *MessengerSettings) GetBusinessProfile() BusinessProfile`

GetBusinessProfile returns the BusinessProfile field if non-nil, zero value otherwise.

### GetBusinessProfileOk

`func (o *MessengerSettings) GetBusinessProfileOk() (*BusinessProfile, bool)`

GetBusinessProfileOk returns a tuple with the BusinessProfile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBusinessProfile

`func (o *MessengerSettings) SetBusinessProfile(v BusinessProfile)`

SetBusinessProfile sets BusinessProfile field to given value.

### HasBusinessProfile

`func (o *MessengerSettings) HasBusinessProfile() bool`

HasBusinessProfile returns a boolean if a field has been set.

### GetName

`func (o *MessengerSettings) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *MessengerSettings) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *MessengerSettings) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *MessengerSettings) HasName() bool`

HasName returns a boolean if a field has been set.

### GetLocale

`func (o *MessengerSettings) GetLocale() string`

GetLocale returns the Locale field if non-nil, zero value otherwise.

### GetLocaleOk

`func (o *MessengerSettings) GetLocaleOk() (*string, bool)`

GetLocaleOk returns a tuple with the Locale field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocale

`func (o *MessengerSettings) SetLocale(v string)`

SetLocale sets Locale field to given value.

### HasLocale

`func (o *MessengerSettings) HasLocale() bool`

HasLocale returns a boolean if a field has been set.

### GetWebsiteUrl

`func (o *MessengerSettings) GetWebsiteUrl() string`

GetWebsiteUrl returns the WebsiteUrl field if non-nil, zero value otherwise.

### GetWebsiteUrlOk

`func (o *MessengerSettings) GetWebsiteUrlOk() (*string, bool)`

GetWebsiteUrlOk returns a tuple with the WebsiteUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWebsiteUrl

`func (o *MessengerSettings) SetWebsiteUrl(v string)`

SetWebsiteUrl sets WebsiteUrl field to given value.

### HasWebsiteUrl

`func (o *MessengerSettings) HasWebsiteUrl() bool`

HasWebsiteUrl returns a boolean if a field has been set.

### GetPrivacyPolicyUrl

`func (o *MessengerSettings) GetPrivacyPolicyUrl() string`

GetPrivacyPolicyUrl returns the PrivacyPolicyUrl field if non-nil, zero value otherwise.

### GetPrivacyPolicyUrlOk

`func (o *MessengerSettings) GetPrivacyPolicyUrlOk() (*string, bool)`

GetPrivacyPolicyUrlOk returns a tuple with the PrivacyPolicyUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrivacyPolicyUrl

`func (o *MessengerSettings) SetPrivacyPolicyUrl(v string)`

SetPrivacyPolicyUrl sets PrivacyPolicyUrl field to given value.

### HasPrivacyPolicyUrl

`func (o *MessengerSettings) HasPrivacyPolicyUrl() bool`

HasPrivacyPolicyUrl returns a boolean if a field has been set.

### GetContactPageUrl

`func (o *MessengerSettings) GetContactPageUrl() string`

GetContactPageUrl returns the ContactPageUrl field if non-nil, zero value otherwise.

### GetContactPageUrlOk

`func (o *MessengerSettings) GetContactPageUrlOk() (*string, bool)`

GetContactPageUrlOk returns a tuple with the ContactPageUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactPageUrl

`func (o *MessengerSettings) SetContactPageUrl(v string)`

SetContactPageUrl sets ContactPageUrl field to given value.

### HasContactPageUrl

`func (o *MessengerSettings) HasContactPageUrl() bool`

HasContactPageUrl returns a boolean if a field has been set.

### GetBrandContactName

`func (o *MessengerSettings) GetBrandContactName() string`

GetBrandContactName returns the BrandContactName field if non-nil, zero value otherwise.

### GetBrandContactNameOk

`func (o *MessengerSettings) GetBrandContactNameOk() (*string, bool)`

GetBrandContactNameOk returns a tuple with the BrandContactName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBrandContactName

`func (o *MessengerSettings) SetBrandContactName(v string)`

SetBrandContactName sets BrandContactName field to given value.

### HasBrandContactName

`func (o *MessengerSettings) HasBrandContactName() bool`

HasBrandContactName returns a boolean if a field has been set.

### GetBrandContactEmail

`func (o *MessengerSettings) GetBrandContactEmail() string`

GetBrandContactEmail returns the BrandContactEmail field if non-nil, zero value otherwise.

### GetBrandContactEmailOk

`func (o *MessengerSettings) GetBrandContactEmailOk() (*string, bool)`

GetBrandContactEmailOk returns a tuple with the BrandContactEmail field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBrandContactEmail

`func (o *MessengerSettings) SetBrandContactEmail(v string)`

SetBrandContactEmail sets BrandContactEmail field to given value.

### HasBrandContactEmail

`func (o *MessengerSettings) HasBrandContactEmail() bool`

HasBrandContactEmail returns a boolean if a field has been set.

### GetWelcomeMessage

`func (o *MessengerSettings) GetWelcomeMessage() string`

GetWelcomeMessage returns the WelcomeMessage field if non-nil, zero value otherwise.

### GetWelcomeMessageOk

`func (o *MessengerSettings) GetWelcomeMessageOk() (*string, bool)`

GetWelcomeMessageOk returns a tuple with the WelcomeMessage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWelcomeMessage

`func (o *MessengerSettings) SetWelcomeMessage(v string)`

SetWelcomeMessage sets WelcomeMessage field to given value.

### HasWelcomeMessage

`func (o *MessengerSettings) HasWelcomeMessage() bool`

HasWelcomeMessage returns a boolean if a field has been set.

### GetOfflineMessage

`func (o *MessengerSettings) GetOfflineMessage() string`

GetOfflineMessage returns the OfflineMessage field if non-nil, zero value otherwise.

### GetOfflineMessageOk

`func (o *MessengerSettings) GetOfflineMessageOk() (*string, bool)`

GetOfflineMessageOk returns a tuple with the OfflineMessage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOfflineMessage

`func (o *MessengerSettings) SetOfflineMessage(v string)`

SetOfflineMessage sets OfflineMessage field to given value.

### HasOfflineMessage

`func (o *MessengerSettings) HasOfflineMessage() bool`

HasOfflineMessage returns a boolean if a field has been set.

### GetDomains

`func (o *MessengerSettings) GetDomains() []string`

GetDomains returns the Domains field if non-nil, zero value otherwise.

### GetDomainsOk

`func (o *MessengerSettings) GetDomainsOk() (*[]string, bool)`

GetDomainsOk returns a tuple with the Domains field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDomains

`func (o *MessengerSettings) SetDomains(v []string)`

SetDomains sets Domains field to given value.

### HasDomains

`func (o *MessengerSettings) HasDomains() bool`

HasDomains returns a boolean if a field has been set.

### GetRegionCodes

`func (o *MessengerSettings) GetRegionCodes() []string`

GetRegionCodes returns the RegionCodes field if non-nil, zero value otherwise.

### GetRegionCodesOk

`func (o *MessengerSettings) GetRegionCodesOk() (*[]string, bool)`

GetRegionCodesOk returns a tuple with the RegionCodes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRegionCodes

`func (o *MessengerSettings) SetRegionCodes(v []string)`

SetRegionCodes sets RegionCodes field to given value.

### HasRegionCodes

`func (o *MessengerSettings) HasRegionCodes() bool`

HasRegionCodes returns a boolean if a field has been set.

### GetPhoneNumbers

`func (o *MessengerSettings) GetPhoneNumbers() []PhoneNumber`

GetPhoneNumbers returns the PhoneNumbers field if non-nil, zero value otherwise.

### GetPhoneNumbersOk

`func (o *MessengerSettings) GetPhoneNumbersOk() (*[]PhoneNumber, bool)`

GetPhoneNumbersOk returns a tuple with the PhoneNumbers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhoneNumbers

`func (o *MessengerSettings) SetPhoneNumbers(v []PhoneNumber)`

SetPhoneNumbers sets PhoneNumbers field to given value.

### HasPhoneNumbers

`func (o *MessengerSettings) HasPhoneNumbers() bool`

HasPhoneNumbers returns a boolean if a field has been set.

### GetOfficeHours

`func (o *MessengerSettings) GetOfficeHours() []OfficeHours`

GetOfficeHours returns the OfficeHours field if non-nil, zero value otherwise.

### GetOfficeHoursOk

`func (o *MessengerSettings) GetOfficeHoursOk() (*[]OfficeHours, bool)`

GetOfficeHoursOk returns a tuple with the OfficeHours field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOfficeHours

`func (o *MessengerSettings) SetOfficeHours(v []OfficeHours)`

SetOfficeHours sets OfficeHours field to given value.

### HasOfficeHours

`func (o *MessengerSettings) HasOfficeHours() bool`

HasOfficeHours returns a boolean if a field has been set.

### GetWhatsappPhoneNumber

`func (o *MessengerSettings) GetWhatsappPhoneNumber() WhatsappPhoneNumber`

GetWhatsappPhoneNumber returns the WhatsappPhoneNumber field if non-nil, zero value otherwise.

### GetWhatsappPhoneNumberOk

`func (o *MessengerSettings) GetWhatsappPhoneNumberOk() (*WhatsappPhoneNumber, bool)`

GetWhatsappPhoneNumberOk returns a tuple with the WhatsappPhoneNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWhatsappPhoneNumber

`func (o *MessengerSettings) SetWhatsappPhoneNumber(v WhatsappPhoneNumber)`

SetWhatsappPhoneNumber sets WhatsappPhoneNumber field to given value.

### HasWhatsappPhoneNumber

`func (o *MessengerSettings) HasWhatsappPhoneNumber() bool`

HasWhatsappPhoneNumber returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


