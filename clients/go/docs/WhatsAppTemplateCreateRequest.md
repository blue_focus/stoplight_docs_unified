# WhatsAppTemplateCreateRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**Language** | **string** |  | 
**Category** | **string** |  | 
**Components** | [**[]WhatsAppTemplateCreateRequestComponents**](WhatsAppTemplateCreateRequestComponents.md) |  | 
**AllowCategoryChange** | Pointer to **bool** |  | [optional] 

## Methods

### NewWhatsAppTemplateCreateRequest

`func NewWhatsAppTemplateCreateRequest(name string, language string, category string, components []WhatsAppTemplateCreateRequestComponents, ) *WhatsAppTemplateCreateRequest`

NewWhatsAppTemplateCreateRequest instantiates a new WhatsAppTemplateCreateRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateCreateRequestWithDefaults

`func NewWhatsAppTemplateCreateRequestWithDefaults() *WhatsAppTemplateCreateRequest`

NewWhatsAppTemplateCreateRequestWithDefaults instantiates a new WhatsAppTemplateCreateRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *WhatsAppTemplateCreateRequest) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *WhatsAppTemplateCreateRequest) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *WhatsAppTemplateCreateRequest) SetName(v string)`

SetName sets Name field to given value.


### GetLanguage

`func (o *WhatsAppTemplateCreateRequest) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *WhatsAppTemplateCreateRequest) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *WhatsAppTemplateCreateRequest) SetLanguage(v string)`

SetLanguage sets Language field to given value.


### GetCategory

`func (o *WhatsAppTemplateCreateRequest) GetCategory() string`

GetCategory returns the Category field if non-nil, zero value otherwise.

### GetCategoryOk

`func (o *WhatsAppTemplateCreateRequest) GetCategoryOk() (*string, bool)`

GetCategoryOk returns a tuple with the Category field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCategory

`func (o *WhatsAppTemplateCreateRequest) SetCategory(v string)`

SetCategory sets Category field to given value.


### GetComponents

`func (o *WhatsAppTemplateCreateRequest) GetComponents() []WhatsAppTemplateCreateRequestComponents`

GetComponents returns the Components field if non-nil, zero value otherwise.

### GetComponentsOk

`func (o *WhatsAppTemplateCreateRequest) GetComponentsOk() (*[]WhatsAppTemplateCreateRequestComponents, bool)`

GetComponentsOk returns a tuple with the Components field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComponents

`func (o *WhatsAppTemplateCreateRequest) SetComponents(v []WhatsAppTemplateCreateRequestComponents)`

SetComponents sets Components field to given value.


### GetAllowCategoryChange

`func (o *WhatsAppTemplateCreateRequest) GetAllowCategoryChange() bool`

GetAllowCategoryChange returns the AllowCategoryChange field if non-nil, zero value otherwise.

### GetAllowCategoryChangeOk

`func (o *WhatsAppTemplateCreateRequest) GetAllowCategoryChangeOk() (*bool, bool)`

GetAllowCategoryChangeOk returns a tuple with the AllowCategoryChange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAllowCategoryChange

`func (o *WhatsAppTemplateCreateRequest) SetAllowCategoryChange(v bool)`

SetAllowCategoryChange sets AllowCategoryChange field to given value.

### HasAllowCategoryChange

`func (o *WhatsAppTemplateCreateRequest) HasAllowCategoryChange() bool`

HasAllowCategoryChange returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


