# FooterLimits

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Types** | Pointer to **[]string** |  | [optional] 

## Methods

### NewFooterLimits

`func NewFooterLimits() *FooterLimits`

NewFooterLimits instantiates a new FooterLimits object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFooterLimitsWithDefaults

`func NewFooterLimitsWithDefaults() *FooterLimits`

NewFooterLimitsWithDefaults instantiates a new FooterLimits object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTypes

`func (o *FooterLimits) GetTypes() []string`

GetTypes returns the Types field if non-nil, zero value otherwise.

### GetTypesOk

`func (o *FooterLimits) GetTypesOk() (*[]string, bool)`

GetTypesOk returns a tuple with the Types field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTypes

`func (o *FooterLimits) SetTypes(v []string)`

SetTypes sets Types field to given value.

### HasTypes

`func (o *FooterLimits) HasTypes() bool`

HasTypes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


