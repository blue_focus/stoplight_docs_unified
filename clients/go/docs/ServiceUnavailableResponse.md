# ServiceUnavailableResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **int32** |  | 
**Message** | **string** |  | 
**ErrorId** | **string** |  | 

## Methods

### NewServiceUnavailableResponse

`func NewServiceUnavailableResponse(code int32, message string, errorId string, ) *ServiceUnavailableResponse`

NewServiceUnavailableResponse instantiates a new ServiceUnavailableResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewServiceUnavailableResponseWithDefaults

`func NewServiceUnavailableResponseWithDefaults() *ServiceUnavailableResponse`

NewServiceUnavailableResponseWithDefaults instantiates a new ServiceUnavailableResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCode

`func (o *ServiceUnavailableResponse) GetCode() int32`

GetCode returns the Code field if non-nil, zero value otherwise.

### GetCodeOk

`func (o *ServiceUnavailableResponse) GetCodeOk() (*int32, bool)`

GetCodeOk returns a tuple with the Code field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCode

`func (o *ServiceUnavailableResponse) SetCode(v int32)`

SetCode sets Code field to given value.


### GetMessage

`func (o *ServiceUnavailableResponse) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *ServiceUnavailableResponse) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *ServiceUnavailableResponse) SetMessage(v string)`

SetMessage sets Message field to given value.


### GetErrorId

`func (o *ServiceUnavailableResponse) GetErrorId() string`

GetErrorId returns the ErrorId field if non-nil, zero value otherwise.

### GetErrorIdOk

`func (o *ServiceUnavailableResponse) GetErrorIdOk() (*string, bool)`

GetErrorIdOk returns a tuple with the ErrorId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrorId

`func (o *ServiceUnavailableResponse) SetErrorId(v string)`

SetErrorId sets ErrorId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


