# WhatsAppTemplateAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Status** | **string** |  | 
**Namespace** | **string** |  | 
**RejectedReason** | Pointer to **string** |  | [optional] 

## Methods

### NewWhatsAppTemplateAllOf

`func NewWhatsAppTemplateAllOf(status string, namespace string, ) *WhatsAppTemplateAllOf`

NewWhatsAppTemplateAllOf instantiates a new WhatsAppTemplateAllOf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateAllOfWithDefaults

`func NewWhatsAppTemplateAllOfWithDefaults() *WhatsAppTemplateAllOf`

NewWhatsAppTemplateAllOfWithDefaults instantiates a new WhatsAppTemplateAllOf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WhatsAppTemplateAllOf) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WhatsAppTemplateAllOf) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WhatsAppTemplateAllOf) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *WhatsAppTemplateAllOf) HasId() bool`

HasId returns a boolean if a field has been set.

### GetStatus

`func (o *WhatsAppTemplateAllOf) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *WhatsAppTemplateAllOf) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *WhatsAppTemplateAllOf) SetStatus(v string)`

SetStatus sets Status field to given value.


### GetNamespace

`func (o *WhatsAppTemplateAllOf) GetNamespace() string`

GetNamespace returns the Namespace field if non-nil, zero value otherwise.

### GetNamespaceOk

`func (o *WhatsAppTemplateAllOf) GetNamespaceOk() (*string, bool)`

GetNamespaceOk returns a tuple with the Namespace field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNamespace

`func (o *WhatsAppTemplateAllOf) SetNamespace(v string)`

SetNamespace sets Namespace field to given value.


### GetRejectedReason

`func (o *WhatsAppTemplateAllOf) GetRejectedReason() string`

GetRejectedReason returns the RejectedReason field if non-nil, zero value otherwise.

### GetRejectedReasonOk

`func (o *WhatsAppTemplateAllOf) GetRejectedReasonOk() (*string, bool)`

GetRejectedReasonOk returns a tuple with the RejectedReason field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRejectedReason

`func (o *WhatsAppTemplateAllOf) SetRejectedReason(v string)`

SetRejectedReason sets RejectedReason field to given value.

### HasRejectedReason

`func (o *WhatsAppTemplateAllOf) HasRejectedReason() bool`

HasRejectedReason returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


