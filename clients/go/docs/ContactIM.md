# ContactIM

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Service** | Pointer to **string** |  | [optional] 
**UserId** | Pointer to **string** |  | [optional] 
**Type** | Pointer to **string** |  | [optional] 

## Methods

### NewContactIM

`func NewContactIM() *ContactIM`

NewContactIM instantiates a new ContactIM object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactIMWithDefaults

`func NewContactIMWithDefaults() *ContactIM`

NewContactIMWithDefaults instantiates a new ContactIM object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetService

`func (o *ContactIM) GetService() string`

GetService returns the Service field if non-nil, zero value otherwise.

### GetServiceOk

`func (o *ContactIM) GetServiceOk() (*string, bool)`

GetServiceOk returns a tuple with the Service field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetService

`func (o *ContactIM) SetService(v string)`

SetService sets Service field to given value.

### HasService

`func (o *ContactIM) HasService() bool`

HasService returns a boolean if a field has been set.

### GetUserId

`func (o *ContactIM) GetUserId() string`

GetUserId returns the UserId field if non-nil, zero value otherwise.

### GetUserIdOk

`func (o *ContactIM) GetUserIdOk() (*string, bool)`

GetUserIdOk returns a tuple with the UserId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserId

`func (o *ContactIM) SetUserId(v string)`

SetUserId sets UserId field to given value.

### HasUserId

`func (o *ContactIM) HasUserId() bool`

HasUserId returns a boolean if a field has been set.

### GetType

`func (o *ContactIM) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *ContactIM) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *ContactIM) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *ContactIM) HasType() bool`

HasType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


