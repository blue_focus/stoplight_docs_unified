# AgentType

## Enum


* `AGENT_HUMAN` (value: `"human"`)

* `AGENT_BOT` (value: `"bot"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


