# Provider

## Enum


* `MESSAGE_PIPE` (value: `"MessagePipe"`)

* `TELEGRAM` (value: `"Telegram"`)

* `WHATS_APP` (value: `"WhatsApp"`)

* `WEBCHAT` (value: `"Webchat"`)

* `FACEBOOK` (value: `"Facebook"`)

* `GOOGLE` (value: `"Google"`)

* `CHAT_WERK` (value: `"ChatWerk"`)

* `DIALOG360` (value: `"Dialog360"`)

* `META` (value: `"Meta"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


