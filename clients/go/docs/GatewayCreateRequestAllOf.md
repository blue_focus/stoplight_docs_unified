# GatewayCreateRequestAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MessengerId** | Pointer to **string** |  | [optional] 
**Provider** | [**Provider**](Provider.md) |  | 
**Messenger** | [**Messenger**](Messenger.md) |  | 
**FacebookPageId** | Pointer to **string** |  | [optional] 

## Methods

### NewGatewayCreateRequestAllOf

`func NewGatewayCreateRequestAllOf(provider Provider, messenger Messenger, ) *GatewayCreateRequestAllOf`

NewGatewayCreateRequestAllOf instantiates a new GatewayCreateRequestAllOf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewGatewayCreateRequestAllOfWithDefaults

`func NewGatewayCreateRequestAllOfWithDefaults() *GatewayCreateRequestAllOf`

NewGatewayCreateRequestAllOfWithDefaults instantiates a new GatewayCreateRequestAllOf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessengerId

`func (o *GatewayCreateRequestAllOf) GetMessengerId() string`

GetMessengerId returns the MessengerId field if non-nil, zero value otherwise.

### GetMessengerIdOk

`func (o *GatewayCreateRequestAllOf) GetMessengerIdOk() (*string, bool)`

GetMessengerIdOk returns a tuple with the MessengerId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessengerId

`func (o *GatewayCreateRequestAllOf) SetMessengerId(v string)`

SetMessengerId sets MessengerId field to given value.

### HasMessengerId

`func (o *GatewayCreateRequestAllOf) HasMessengerId() bool`

HasMessengerId returns a boolean if a field has been set.

### GetProvider

`func (o *GatewayCreateRequestAllOf) GetProvider() Provider`

GetProvider returns the Provider field if non-nil, zero value otherwise.

### GetProviderOk

`func (o *GatewayCreateRequestAllOf) GetProviderOk() (*Provider, bool)`

GetProviderOk returns a tuple with the Provider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProvider

`func (o *GatewayCreateRequestAllOf) SetProvider(v Provider)`

SetProvider sets Provider field to given value.


### GetMessenger

`func (o *GatewayCreateRequestAllOf) GetMessenger() Messenger`

GetMessenger returns the Messenger field if non-nil, zero value otherwise.

### GetMessengerOk

`func (o *GatewayCreateRequestAllOf) GetMessengerOk() (*Messenger, bool)`

GetMessengerOk returns a tuple with the Messenger field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessenger

`func (o *GatewayCreateRequestAllOf) SetMessenger(v Messenger)`

SetMessenger sets Messenger field to given value.


### GetFacebookPageId

`func (o *GatewayCreateRequestAllOf) GetFacebookPageId() string`

GetFacebookPageId returns the FacebookPageId field if non-nil, zero value otherwise.

### GetFacebookPageIdOk

`func (o *GatewayCreateRequestAllOf) GetFacebookPageIdOk() (*string, bool)`

GetFacebookPageIdOk returns a tuple with the FacebookPageId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFacebookPageId

`func (o *GatewayCreateRequestAllOf) SetFacebookPageId(v string)`

SetFacebookPageId sets FacebookPageId field to given value.

### HasFacebookPageId

`func (o *GatewayCreateRequestAllOf) HasFacebookPageId() bool`

HasFacebookPageId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


