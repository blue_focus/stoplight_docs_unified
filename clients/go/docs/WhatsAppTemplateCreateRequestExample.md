# WhatsAppTemplateCreateRequestExample

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HeaderText** | Pointer to **[]string** |  | [optional] 
**BodyText** | Pointer to **[]string** |  | [optional] 
**FooterText** | Pointer to **[]string** |  | [optional] 
**HeaderUrl** | Pointer to **[]string** |  | [optional] 
**HeaderHandle** | Pointer to **[]string** |  | [optional] 

## Methods

### NewWhatsAppTemplateCreateRequestExample

`func NewWhatsAppTemplateCreateRequestExample() *WhatsAppTemplateCreateRequestExample`

NewWhatsAppTemplateCreateRequestExample instantiates a new WhatsAppTemplateCreateRequestExample object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateCreateRequestExampleWithDefaults

`func NewWhatsAppTemplateCreateRequestExampleWithDefaults() *WhatsAppTemplateCreateRequestExample`

NewWhatsAppTemplateCreateRequestExampleWithDefaults instantiates a new WhatsAppTemplateCreateRequestExample object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetHeaderText

`func (o *WhatsAppTemplateCreateRequestExample) GetHeaderText() []string`

GetHeaderText returns the HeaderText field if non-nil, zero value otherwise.

### GetHeaderTextOk

`func (o *WhatsAppTemplateCreateRequestExample) GetHeaderTextOk() (*[]string, bool)`

GetHeaderTextOk returns a tuple with the HeaderText field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeaderText

`func (o *WhatsAppTemplateCreateRequestExample) SetHeaderText(v []string)`

SetHeaderText sets HeaderText field to given value.

### HasHeaderText

`func (o *WhatsAppTemplateCreateRequestExample) HasHeaderText() bool`

HasHeaderText returns a boolean if a field has been set.

### GetBodyText

`func (o *WhatsAppTemplateCreateRequestExample) GetBodyText() []string`

GetBodyText returns the BodyText field if non-nil, zero value otherwise.

### GetBodyTextOk

`func (o *WhatsAppTemplateCreateRequestExample) GetBodyTextOk() (*[]string, bool)`

GetBodyTextOk returns a tuple with the BodyText field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBodyText

`func (o *WhatsAppTemplateCreateRequestExample) SetBodyText(v []string)`

SetBodyText sets BodyText field to given value.

### HasBodyText

`func (o *WhatsAppTemplateCreateRequestExample) HasBodyText() bool`

HasBodyText returns a boolean if a field has been set.

### GetFooterText

`func (o *WhatsAppTemplateCreateRequestExample) GetFooterText() []string`

GetFooterText returns the FooterText field if non-nil, zero value otherwise.

### GetFooterTextOk

`func (o *WhatsAppTemplateCreateRequestExample) GetFooterTextOk() (*[]string, bool)`

GetFooterTextOk returns a tuple with the FooterText field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFooterText

`func (o *WhatsAppTemplateCreateRequestExample) SetFooterText(v []string)`

SetFooterText sets FooterText field to given value.

### HasFooterText

`func (o *WhatsAppTemplateCreateRequestExample) HasFooterText() bool`

HasFooterText returns a boolean if a field has been set.

### GetHeaderUrl

`func (o *WhatsAppTemplateCreateRequestExample) GetHeaderUrl() []string`

GetHeaderUrl returns the HeaderUrl field if non-nil, zero value otherwise.

### GetHeaderUrlOk

`func (o *WhatsAppTemplateCreateRequestExample) GetHeaderUrlOk() (*[]string, bool)`

GetHeaderUrlOk returns a tuple with the HeaderUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeaderUrl

`func (o *WhatsAppTemplateCreateRequestExample) SetHeaderUrl(v []string)`

SetHeaderUrl sets HeaderUrl field to given value.

### HasHeaderUrl

`func (o *WhatsAppTemplateCreateRequestExample) HasHeaderUrl() bool`

HasHeaderUrl returns a boolean if a field has been set.

### GetHeaderHandle

`func (o *WhatsAppTemplateCreateRequestExample) GetHeaderHandle() []string`

GetHeaderHandle returns the HeaderHandle field if non-nil, zero value otherwise.

### GetHeaderHandleOk

`func (o *WhatsAppTemplateCreateRequestExample) GetHeaderHandleOk() (*[]string, bool)`

GetHeaderHandleOk returns a tuple with the HeaderHandle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeaderHandle

`func (o *WhatsAppTemplateCreateRequestExample) SetHeaderHandle(v []string)`

SetHeaderHandle sets HeaderHandle field to given value.

### HasHeaderHandle

`func (o *WhatsAppTemplateCreateRequestExample) HasHeaderHandle() bool`

HasHeaderHandle returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


