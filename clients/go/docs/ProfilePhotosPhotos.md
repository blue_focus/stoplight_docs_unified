# ProfilePhotosPhotos

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | Pointer to **string** |  | [optional] 
**Mime** | Pointer to **string** |  | [optional] 
**Width** | Pointer to **string** |  | [optional] 
**Height** | Pointer to **string** |  | [optional] 
**Binary** | Pointer to **string** | base64 encoded binary data | [optional] 

## Methods

### NewProfilePhotosPhotos

`func NewProfilePhotosPhotos() *ProfilePhotosPhotos`

NewProfilePhotosPhotos instantiates a new ProfilePhotosPhotos object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProfilePhotosPhotosWithDefaults

`func NewProfilePhotosPhotosWithDefaults() *ProfilePhotosPhotos`

NewProfilePhotosPhotosWithDefaults instantiates a new ProfilePhotosPhotos object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUrl

`func (o *ProfilePhotosPhotos) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *ProfilePhotosPhotos) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *ProfilePhotosPhotos) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *ProfilePhotosPhotos) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetMime

`func (o *ProfilePhotosPhotos) GetMime() string`

GetMime returns the Mime field if non-nil, zero value otherwise.

### GetMimeOk

`func (o *ProfilePhotosPhotos) GetMimeOk() (*string, bool)`

GetMimeOk returns a tuple with the Mime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMime

`func (o *ProfilePhotosPhotos) SetMime(v string)`

SetMime sets Mime field to given value.

### HasMime

`func (o *ProfilePhotosPhotos) HasMime() bool`

HasMime returns a boolean if a field has been set.

### GetWidth

`func (o *ProfilePhotosPhotos) GetWidth() string`

GetWidth returns the Width field if non-nil, zero value otherwise.

### GetWidthOk

`func (o *ProfilePhotosPhotos) GetWidthOk() (*string, bool)`

GetWidthOk returns a tuple with the Width field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWidth

`func (o *ProfilePhotosPhotos) SetWidth(v string)`

SetWidth sets Width field to given value.

### HasWidth

`func (o *ProfilePhotosPhotos) HasWidth() bool`

HasWidth returns a boolean if a field has been set.

### GetHeight

`func (o *ProfilePhotosPhotos) GetHeight() string`

GetHeight returns the Height field if non-nil, zero value otherwise.

### GetHeightOk

`func (o *ProfilePhotosPhotos) GetHeightOk() (*string, bool)`

GetHeightOk returns a tuple with the Height field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeight

`func (o *ProfilePhotosPhotos) SetHeight(v string)`

SetHeight sets Height field to given value.

### HasHeight

`func (o *ProfilePhotosPhotos) HasHeight() bool`

HasHeight returns a boolean if a field has been set.

### GetBinary

`func (o *ProfilePhotosPhotos) GetBinary() string`

GetBinary returns the Binary field if non-nil, zero value otherwise.

### GetBinaryOk

`func (o *ProfilePhotosPhotos) GetBinaryOk() (*string, bool)`

GetBinaryOk returns a tuple with the Binary field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBinary

`func (o *ProfilePhotosPhotos) SetBinary(v string)`

SetBinary sets Binary field to given value.

### HasBinary

`func (o *ProfilePhotosPhotos) HasBinary() bool`

HasBinary returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


