# WhatsAppTemplateMedia

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Link** | Pointer to **string** |  | [optional] 
**Filename** | Pointer to **string** |  | [optional] 

## Methods

### NewWhatsAppTemplateMedia

`func NewWhatsAppTemplateMedia() *WhatsAppTemplateMedia`

NewWhatsAppTemplateMedia instantiates a new WhatsAppTemplateMedia object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMediaWithDefaults

`func NewWhatsAppTemplateMediaWithDefaults() *WhatsAppTemplateMedia`

NewWhatsAppTemplateMediaWithDefaults instantiates a new WhatsAppTemplateMedia object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WhatsAppTemplateMedia) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WhatsAppTemplateMedia) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WhatsAppTemplateMedia) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *WhatsAppTemplateMedia) HasId() bool`

HasId returns a boolean if a field has been set.

### GetLink

`func (o *WhatsAppTemplateMedia) GetLink() string`

GetLink returns the Link field if non-nil, zero value otherwise.

### GetLinkOk

`func (o *WhatsAppTemplateMedia) GetLinkOk() (*string, bool)`

GetLinkOk returns a tuple with the Link field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLink

`func (o *WhatsAppTemplateMedia) SetLink(v string)`

SetLink sets Link field to given value.

### HasLink

`func (o *WhatsAppTemplateMedia) HasLink() bool`

HasLink returns a boolean if a field has been set.

### GetFilename

`func (o *WhatsAppTemplateMedia) GetFilename() string`

GetFilename returns the Filename field if non-nil, zero value otherwise.

### GetFilenameOk

`func (o *WhatsAppTemplateMedia) GetFilenameOk() (*string, bool)`

GetFilenameOk returns a tuple with the Filename field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFilename

`func (o *WhatsAppTemplateMedia) SetFilename(v string)`

SetFilename sets Filename field to given value.

### HasFilename

`func (o *WhatsAppTemplateMedia) HasFilename() bool`

HasFilename returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


