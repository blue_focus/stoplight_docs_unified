# WebhookData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Gateway** | [**WebhookDataGateway**](WebhookDataGateway.md) |  | 
**Messages** | Pointer to [**[]WebhookMessage**](WebhookMessage.md) |  | [optional] 
**Statuses** | Pointer to [**[]WebhookStatus**](WebhookStatus.md) |  | [optional] 
**System** | Pointer to [**[]WebhookSystemMessage**](WebhookSystemMessage.md) |  | [optional] 

## Methods

### NewWebhookData

`func NewWebhookData(gateway WebhookDataGateway, ) *WebhookData`

NewWebhookData instantiates a new WebhookData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookDataWithDefaults

`func NewWebhookDataWithDefaults() *WebhookData`

NewWebhookDataWithDefaults instantiates a new WebhookData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetGateway

`func (o *WebhookData) GetGateway() WebhookDataGateway`

GetGateway returns the Gateway field if non-nil, zero value otherwise.

### GetGatewayOk

`func (o *WebhookData) GetGatewayOk() (*WebhookDataGateway, bool)`

GetGatewayOk returns a tuple with the Gateway field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGateway

`func (o *WebhookData) SetGateway(v WebhookDataGateway)`

SetGateway sets Gateway field to given value.


### GetMessages

`func (o *WebhookData) GetMessages() []WebhookMessage`

GetMessages returns the Messages field if non-nil, zero value otherwise.

### GetMessagesOk

`func (o *WebhookData) GetMessagesOk() (*[]WebhookMessage, bool)`

GetMessagesOk returns a tuple with the Messages field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessages

`func (o *WebhookData) SetMessages(v []WebhookMessage)`

SetMessages sets Messages field to given value.

### HasMessages

`func (o *WebhookData) HasMessages() bool`

HasMessages returns a boolean if a field has been set.

### GetStatuses

`func (o *WebhookData) GetStatuses() []WebhookStatus`

GetStatuses returns the Statuses field if non-nil, zero value otherwise.

### GetStatusesOk

`func (o *WebhookData) GetStatusesOk() (*[]WebhookStatus, bool)`

GetStatusesOk returns a tuple with the Statuses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatuses

`func (o *WebhookData) SetStatuses(v []WebhookStatus)`

SetStatuses sets Statuses field to given value.

### HasStatuses

`func (o *WebhookData) HasStatuses() bool`

HasStatuses returns a boolean if a field has been set.

### GetSystem

`func (o *WebhookData) GetSystem() []WebhookSystemMessage`

GetSystem returns the System field if non-nil, zero value otherwise.

### GetSystemOk

`func (o *WebhookData) GetSystemOk() (*[]WebhookSystemMessage, bool)`

GetSystemOk returns a tuple with the System field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSystem

`func (o *WebhookData) SetSystem(v []WebhookSystemMessage)`

SetSystem sets System field to given value.

### HasSystem

`func (o *WebhookData) HasSystem() bool`

HasSystem returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


