# WebhookMessageChat

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContactId** | **string** |  | 
**Name** | **string** |  | 
**Type** | [**ChatType**](ChatType.md) |  | 
**Attributes** | Pointer to **map[string]string** |  | [optional] 

## Methods

### NewWebhookMessageChat

`func NewWebhookMessageChat(contactId string, name string, type_ ChatType, ) *WebhookMessageChat`

NewWebhookMessageChat instantiates a new WebhookMessageChat object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookMessageChatWithDefaults

`func NewWebhookMessageChatWithDefaults() *WebhookMessageChat`

NewWebhookMessageChatWithDefaults instantiates a new WebhookMessageChat object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContactId

`func (o *WebhookMessageChat) GetContactId() string`

GetContactId returns the ContactId field if non-nil, zero value otherwise.

### GetContactIdOk

`func (o *WebhookMessageChat) GetContactIdOk() (*string, bool)`

GetContactIdOk returns a tuple with the ContactId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContactId

`func (o *WebhookMessageChat) SetContactId(v string)`

SetContactId sets ContactId field to given value.


### GetName

`func (o *WebhookMessageChat) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *WebhookMessageChat) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *WebhookMessageChat) SetName(v string)`

SetName sets Name field to given value.


### GetType

`func (o *WebhookMessageChat) GetType() ChatType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WebhookMessageChat) GetTypeOk() (*ChatType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WebhookMessageChat) SetType(v ChatType)`

SetType sets Type field to given value.


### GetAttributes

`func (o *WebhookMessageChat) GetAttributes() map[string]string`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *WebhookMessageChat) GetAttributesOk() (*map[string]string, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *WebhookMessageChat) SetAttributes(v map[string]string)`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *WebhookMessageChat) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


