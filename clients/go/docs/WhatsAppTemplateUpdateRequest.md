# WhatsAppTemplateUpdateRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Category** | **string** |  | 
**Components** | [**[]WhatsAppTemplateCreateRequestComponents**](WhatsAppTemplateCreateRequestComponents.md) |  | 

## Methods

### NewWhatsAppTemplateUpdateRequest

`func NewWhatsAppTemplateUpdateRequest(category string, components []WhatsAppTemplateCreateRequestComponents, ) *WhatsAppTemplateUpdateRequest`

NewWhatsAppTemplateUpdateRequest instantiates a new WhatsAppTemplateUpdateRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateUpdateRequestWithDefaults

`func NewWhatsAppTemplateUpdateRequestWithDefaults() *WhatsAppTemplateUpdateRequest`

NewWhatsAppTemplateUpdateRequestWithDefaults instantiates a new WhatsAppTemplateUpdateRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCategory

`func (o *WhatsAppTemplateUpdateRequest) GetCategory() string`

GetCategory returns the Category field if non-nil, zero value otherwise.

### GetCategoryOk

`func (o *WhatsAppTemplateUpdateRequest) GetCategoryOk() (*string, bool)`

GetCategoryOk returns a tuple with the Category field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCategory

`func (o *WhatsAppTemplateUpdateRequest) SetCategory(v string)`

SetCategory sets Category field to given value.


### GetComponents

`func (o *WhatsAppTemplateUpdateRequest) GetComponents() []WhatsAppTemplateCreateRequestComponents`

GetComponents returns the Components field if non-nil, zero value otherwise.

### GetComponentsOk

`func (o *WhatsAppTemplateUpdateRequest) GetComponentsOk() (*[]WhatsAppTemplateCreateRequestComponents, bool)`

GetComponentsOk returns a tuple with the Components field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComponents

`func (o *WhatsAppTemplateUpdateRequest) SetComponents(v []WhatsAppTemplateCreateRequestComponents)`

SetComponents sets Components field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


