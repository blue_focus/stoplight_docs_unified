# WhatsAppTemplateMessageParameters

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Text** | Pointer to **string** |  | [optional] 
**Payload** | Pointer to **string** | For buttons only | [optional] 
**Currency** | Pointer to [**WhatsAppTemplateMessageCurrency**](WhatsAppTemplateMessageCurrency.md) |  | [optional] 
**DateTime** | Pointer to [**WhatsAppTemplateMessageDateTime**](WhatsAppTemplateMessageDateTime.md) |  | [optional] 
**Image** | Pointer to [**WhatsAppTemplateMedia**](WhatsAppTemplateMedia.md) |  | [optional] 
**Video** | Pointer to [**WhatsAppTemplateMedia**](WhatsAppTemplateMedia.md) |  | [optional] 
**Document** | Pointer to [**WhatsAppTemplateMedia**](WhatsAppTemplateMedia.md) |  | [optional] 

## Methods

### NewWhatsAppTemplateMessageParameters

`func NewWhatsAppTemplateMessageParameters(type_ string, ) *WhatsAppTemplateMessageParameters`

NewWhatsAppTemplateMessageParameters instantiates a new WhatsAppTemplateMessageParameters object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMessageParametersWithDefaults

`func NewWhatsAppTemplateMessageParametersWithDefaults() *WhatsAppTemplateMessageParameters`

NewWhatsAppTemplateMessageParametersWithDefaults instantiates a new WhatsAppTemplateMessageParameters object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *WhatsAppTemplateMessageParameters) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WhatsAppTemplateMessageParameters) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WhatsAppTemplateMessageParameters) SetType(v string)`

SetType sets Type field to given value.


### GetText

`func (o *WhatsAppTemplateMessageParameters) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *WhatsAppTemplateMessageParameters) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *WhatsAppTemplateMessageParameters) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *WhatsAppTemplateMessageParameters) HasText() bool`

HasText returns a boolean if a field has been set.

### GetPayload

`func (o *WhatsAppTemplateMessageParameters) GetPayload() string`

GetPayload returns the Payload field if non-nil, zero value otherwise.

### GetPayloadOk

`func (o *WhatsAppTemplateMessageParameters) GetPayloadOk() (*string, bool)`

GetPayloadOk returns a tuple with the Payload field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayload

`func (o *WhatsAppTemplateMessageParameters) SetPayload(v string)`

SetPayload sets Payload field to given value.

### HasPayload

`func (o *WhatsAppTemplateMessageParameters) HasPayload() bool`

HasPayload returns a boolean if a field has been set.

### GetCurrency

`func (o *WhatsAppTemplateMessageParameters) GetCurrency() WhatsAppTemplateMessageCurrency`

GetCurrency returns the Currency field if non-nil, zero value otherwise.

### GetCurrencyOk

`func (o *WhatsAppTemplateMessageParameters) GetCurrencyOk() (*WhatsAppTemplateMessageCurrency, bool)`

GetCurrencyOk returns a tuple with the Currency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrency

`func (o *WhatsAppTemplateMessageParameters) SetCurrency(v WhatsAppTemplateMessageCurrency)`

SetCurrency sets Currency field to given value.

### HasCurrency

`func (o *WhatsAppTemplateMessageParameters) HasCurrency() bool`

HasCurrency returns a boolean if a field has been set.

### GetDateTime

`func (o *WhatsAppTemplateMessageParameters) GetDateTime() WhatsAppTemplateMessageDateTime`

GetDateTime returns the DateTime field if non-nil, zero value otherwise.

### GetDateTimeOk

`func (o *WhatsAppTemplateMessageParameters) GetDateTimeOk() (*WhatsAppTemplateMessageDateTime, bool)`

GetDateTimeOk returns a tuple with the DateTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDateTime

`func (o *WhatsAppTemplateMessageParameters) SetDateTime(v WhatsAppTemplateMessageDateTime)`

SetDateTime sets DateTime field to given value.

### HasDateTime

`func (o *WhatsAppTemplateMessageParameters) HasDateTime() bool`

HasDateTime returns a boolean if a field has been set.

### GetImage

`func (o *WhatsAppTemplateMessageParameters) GetImage() WhatsAppTemplateMedia`

GetImage returns the Image field if non-nil, zero value otherwise.

### GetImageOk

`func (o *WhatsAppTemplateMessageParameters) GetImageOk() (*WhatsAppTemplateMedia, bool)`

GetImageOk returns a tuple with the Image field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetImage

`func (o *WhatsAppTemplateMessageParameters) SetImage(v WhatsAppTemplateMedia)`

SetImage sets Image field to given value.

### HasImage

`func (o *WhatsAppTemplateMessageParameters) HasImage() bool`

HasImage returns a boolean if a field has been set.

### GetVideo

`func (o *WhatsAppTemplateMessageParameters) GetVideo() WhatsAppTemplateMedia`

GetVideo returns the Video field if non-nil, zero value otherwise.

### GetVideoOk

`func (o *WhatsAppTemplateMessageParameters) GetVideoOk() (*WhatsAppTemplateMedia, bool)`

GetVideoOk returns a tuple with the Video field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideo

`func (o *WhatsAppTemplateMessageParameters) SetVideo(v WhatsAppTemplateMedia)`

SetVideo sets Video field to given value.

### HasVideo

`func (o *WhatsAppTemplateMessageParameters) HasVideo() bool`

HasVideo returns a boolean if a field has been set.

### GetDocument

`func (o *WhatsAppTemplateMessageParameters) GetDocument() WhatsAppTemplateMedia`

GetDocument returns the Document field if non-nil, zero value otherwise.

### GetDocumentOk

`func (o *WhatsAppTemplateMessageParameters) GetDocumentOk() (*WhatsAppTemplateMedia, bool)`

GetDocumentOk returns a tuple with the Document field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDocument

`func (o *WhatsAppTemplateMessageParameters) SetDocument(v WhatsAppTemplateMedia)`

SetDocument sets Document field to given value.

### HasDocument

`func (o *WhatsAppTemplateMessageParameters) HasDocument() bool`

HasDocument returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


