# Callback

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Text** | Pointer to **string** |  | [optional] 
**Payload** | Pointer to **string** |  | [optional] 

## Methods

### NewCallback

`func NewCallback() *Callback`

NewCallback instantiates a new Callback object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCallbackWithDefaults

`func NewCallbackWithDefaults() *Callback`

NewCallbackWithDefaults instantiates a new Callback object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetText

`func (o *Callback) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *Callback) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *Callback) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *Callback) HasText() bool`

HasText returns a boolean if a field has been set.

### GetPayload

`func (o *Callback) GetPayload() string`

GetPayload returns the Payload field if non-nil, zero value otherwise.

### GetPayloadOk

`func (o *Callback) GetPayloadOk() (*string, bool)`

GetPayloadOk returns a tuple with the Payload field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPayload

`func (o *Callback) SetPayload(v string)`

SetPayload sets Payload field to given value.

### HasPayload

`func (o *Callback) HasPayload() bool`

HasPayload returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


