# CheckContactsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Contacts** | Pointer to [**[]ContactStatus**](ContactStatus.md) |  | [optional] 
**Error** | Pointer to [**ErrorDetails**](ErrorDetails.md) |  | [optional] 

## Methods

### NewCheckContactsResponse

`func NewCheckContactsResponse() *CheckContactsResponse`

NewCheckContactsResponse instantiates a new CheckContactsResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCheckContactsResponseWithDefaults

`func NewCheckContactsResponseWithDefaults() *CheckContactsResponse`

NewCheckContactsResponseWithDefaults instantiates a new CheckContactsResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContacts

`func (o *CheckContactsResponse) GetContacts() []ContactStatus`

GetContacts returns the Contacts field if non-nil, zero value otherwise.

### GetContactsOk

`func (o *CheckContactsResponse) GetContactsOk() (*[]ContactStatus, bool)`

GetContactsOk returns a tuple with the Contacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContacts

`func (o *CheckContactsResponse) SetContacts(v []ContactStatus)`

SetContacts sets Contacts field to given value.

### HasContacts

`func (o *CheckContactsResponse) HasContacts() bool`

HasContacts returns a boolean if a field has been set.

### GetError

`func (o *CheckContactsResponse) GetError() ErrorDetails`

GetError returns the Error field if non-nil, zero value otherwise.

### GetErrorOk

`func (o *CheckContactsResponse) GetErrorOk() (*ErrorDetails, bool)`

GetErrorOk returns a tuple with the Error field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetError

`func (o *CheckContactsResponse) SetError(v ErrorDetails)`

SetError sets Error field to given value.

### HasError

`func (o *CheckContactsResponse) HasError() bool`

HasError returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


