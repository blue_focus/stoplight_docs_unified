# BusinessProfile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Description** | Pointer to **string** |  | [optional] 
**Address** | Pointer to **string** |  | [optional] 
**Email** | Pointer to **string** |  | [optional] 
**Vertical** | Pointer to **string** |  | [optional] 
**Websites** | Pointer to **[]string** |  | [optional] 

## Methods

### NewBusinessProfile

`func NewBusinessProfile() *BusinessProfile`

NewBusinessProfile instantiates a new BusinessProfile object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBusinessProfileWithDefaults

`func NewBusinessProfileWithDefaults() *BusinessProfile`

NewBusinessProfileWithDefaults instantiates a new BusinessProfile object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDescription

`func (o *BusinessProfile) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *BusinessProfile) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *BusinessProfile) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *BusinessProfile) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetAddress

`func (o *BusinessProfile) GetAddress() string`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *BusinessProfile) GetAddressOk() (*string, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *BusinessProfile) SetAddress(v string)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *BusinessProfile) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetEmail

`func (o *BusinessProfile) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *BusinessProfile) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *BusinessProfile) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *BusinessProfile) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetVertical

`func (o *BusinessProfile) GetVertical() string`

GetVertical returns the Vertical field if non-nil, zero value otherwise.

### GetVerticalOk

`func (o *BusinessProfile) GetVerticalOk() (*string, bool)`

GetVerticalOk returns a tuple with the Vertical field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVertical

`func (o *BusinessProfile) SetVertical(v string)`

SetVertical sets Vertical field to given value.

### HasVertical

`func (o *BusinessProfile) HasVertical() bool`

HasVertical returns a boolean if a field has been set.

### GetWebsites

`func (o *BusinessProfile) GetWebsites() []string`

GetWebsites returns the Websites field if non-nil, zero value otherwise.

### GetWebsitesOk

`func (o *BusinessProfile) GetWebsitesOk() (*[]string, bool)`

GetWebsitesOk returns a tuple with the Websites field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWebsites

`func (o *BusinessProfile) SetWebsites(v []string)`

SetWebsites sets Websites field to given value.

### HasWebsites

`func (o *BusinessProfile) HasWebsites() bool`

HasWebsites returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


