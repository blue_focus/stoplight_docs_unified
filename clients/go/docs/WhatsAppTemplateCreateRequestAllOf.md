# WhatsAppTemplateCreateRequestAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | 
**Language** | **string** |  | 
**AllowCategoryChange** | Pointer to **bool** |  | [optional] 

## Methods

### NewWhatsAppTemplateCreateRequestAllOf

`func NewWhatsAppTemplateCreateRequestAllOf(name string, language string, ) *WhatsAppTemplateCreateRequestAllOf`

NewWhatsAppTemplateCreateRequestAllOf instantiates a new WhatsAppTemplateCreateRequestAllOf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateCreateRequestAllOfWithDefaults

`func NewWhatsAppTemplateCreateRequestAllOfWithDefaults() *WhatsAppTemplateCreateRequestAllOf`

NewWhatsAppTemplateCreateRequestAllOfWithDefaults instantiates a new WhatsAppTemplateCreateRequestAllOf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetName

`func (o *WhatsAppTemplateCreateRequestAllOf) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *WhatsAppTemplateCreateRequestAllOf) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *WhatsAppTemplateCreateRequestAllOf) SetName(v string)`

SetName sets Name field to given value.


### GetLanguage

`func (o *WhatsAppTemplateCreateRequestAllOf) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *WhatsAppTemplateCreateRequestAllOf) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *WhatsAppTemplateCreateRequestAllOf) SetLanguage(v string)`

SetLanguage sets Language field to given value.


### GetAllowCategoryChange

`func (o *WhatsAppTemplateCreateRequestAllOf) GetAllowCategoryChange() bool`

GetAllowCategoryChange returns the AllowCategoryChange field if non-nil, zero value otherwise.

### GetAllowCategoryChangeOk

`func (o *WhatsAppTemplateCreateRequestAllOf) GetAllowCategoryChangeOk() (*bool, bool)`

GetAllowCategoryChangeOk returns a tuple with the AllowCategoryChange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAllowCategoryChange

`func (o *WhatsAppTemplateCreateRequestAllOf) SetAllowCategoryChange(v bool)`

SetAllowCategoryChange sets AllowCategoryChange field to given value.

### HasAllowCategoryChange

`func (o *WhatsAppTemplateCreateRequestAllOf) HasAllowCategoryChange() bool`

HasAllowCategoryChange returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


