# SendMessageRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**To** | **string** |  | 
**Type** | [**MessageType**](MessageType.md) |  | 
**Text** | Pointer to **string** |  | [optional] 
**MediaId** | Pointer to **string** |  | [optional] 
**MediaUrl** | Pointer to **string** |  | [optional] 
**MediaFilename** | Pointer to **string** |  | [optional] 
**Location** | Pointer to [**Location**](Location.md) |  | [optional] 
**Buttons** | Pointer to [**[]Button**](Button.md) |  | [optional] 
**Contacts** | Pointer to [**[]Contact**](Contact.md) |  | [optional] 
**ButtonName** | Pointer to **string** | may be required if message type is &#39;list&#39; | [optional] 
**Header** | Pointer to [**Header**](Header.md) |  | [optional] 
**Footer** | Pointer to [**Footer**](Footer.md) |  | [optional] 
**Items** | Pointer to [**[]MessageItem**](MessageItem.md) |  | [optional] 
**Agent** | Pointer to [**Agent**](Agent.md) |  | [optional] 

## Methods

### NewSendMessageRequest

`func NewSendMessageRequest(to string, type_ MessageType, ) *SendMessageRequest`

NewSendMessageRequest instantiates a new SendMessageRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSendMessageRequestWithDefaults

`func NewSendMessageRequestWithDefaults() *SendMessageRequest`

NewSendMessageRequestWithDefaults instantiates a new SendMessageRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTo

`func (o *SendMessageRequest) GetTo() string`

GetTo returns the To field if non-nil, zero value otherwise.

### GetToOk

`func (o *SendMessageRequest) GetToOk() (*string, bool)`

GetToOk returns a tuple with the To field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTo

`func (o *SendMessageRequest) SetTo(v string)`

SetTo sets To field to given value.


### GetType

`func (o *SendMessageRequest) GetType() MessageType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *SendMessageRequest) GetTypeOk() (*MessageType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *SendMessageRequest) SetType(v MessageType)`

SetType sets Type field to given value.


### GetText

`func (o *SendMessageRequest) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *SendMessageRequest) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *SendMessageRequest) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *SendMessageRequest) HasText() bool`

HasText returns a boolean if a field has been set.

### GetMediaId

`func (o *SendMessageRequest) GetMediaId() string`

GetMediaId returns the MediaId field if non-nil, zero value otherwise.

### GetMediaIdOk

`func (o *SendMessageRequest) GetMediaIdOk() (*string, bool)`

GetMediaIdOk returns a tuple with the MediaId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaId

`func (o *SendMessageRequest) SetMediaId(v string)`

SetMediaId sets MediaId field to given value.

### HasMediaId

`func (o *SendMessageRequest) HasMediaId() bool`

HasMediaId returns a boolean if a field has been set.

### GetMediaUrl

`func (o *SendMessageRequest) GetMediaUrl() string`

GetMediaUrl returns the MediaUrl field if non-nil, zero value otherwise.

### GetMediaUrlOk

`func (o *SendMessageRequest) GetMediaUrlOk() (*string, bool)`

GetMediaUrlOk returns a tuple with the MediaUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaUrl

`func (o *SendMessageRequest) SetMediaUrl(v string)`

SetMediaUrl sets MediaUrl field to given value.

### HasMediaUrl

`func (o *SendMessageRequest) HasMediaUrl() bool`

HasMediaUrl returns a boolean if a field has been set.

### GetMediaFilename

`func (o *SendMessageRequest) GetMediaFilename() string`

GetMediaFilename returns the MediaFilename field if non-nil, zero value otherwise.

### GetMediaFilenameOk

`func (o *SendMessageRequest) GetMediaFilenameOk() (*string, bool)`

GetMediaFilenameOk returns a tuple with the MediaFilename field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMediaFilename

`func (o *SendMessageRequest) SetMediaFilename(v string)`

SetMediaFilename sets MediaFilename field to given value.

### HasMediaFilename

`func (o *SendMessageRequest) HasMediaFilename() bool`

HasMediaFilename returns a boolean if a field has been set.

### GetLocation

`func (o *SendMessageRequest) GetLocation() Location`

GetLocation returns the Location field if non-nil, zero value otherwise.

### GetLocationOk

`func (o *SendMessageRequest) GetLocationOk() (*Location, bool)`

GetLocationOk returns a tuple with the Location field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocation

`func (o *SendMessageRequest) SetLocation(v Location)`

SetLocation sets Location field to given value.

### HasLocation

`func (o *SendMessageRequest) HasLocation() bool`

HasLocation returns a boolean if a field has been set.

### GetButtons

`func (o *SendMessageRequest) GetButtons() []Button`

GetButtons returns the Buttons field if non-nil, zero value otherwise.

### GetButtonsOk

`func (o *SendMessageRequest) GetButtonsOk() (*[]Button, bool)`

GetButtonsOk returns a tuple with the Buttons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtons

`func (o *SendMessageRequest) SetButtons(v []Button)`

SetButtons sets Buttons field to given value.

### HasButtons

`func (o *SendMessageRequest) HasButtons() bool`

HasButtons returns a boolean if a field has been set.

### GetContacts

`func (o *SendMessageRequest) GetContacts() []Contact`

GetContacts returns the Contacts field if non-nil, zero value otherwise.

### GetContactsOk

`func (o *SendMessageRequest) GetContactsOk() (*[]Contact, bool)`

GetContactsOk returns a tuple with the Contacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContacts

`func (o *SendMessageRequest) SetContacts(v []Contact)`

SetContacts sets Contacts field to given value.

### HasContacts

`func (o *SendMessageRequest) HasContacts() bool`

HasContacts returns a boolean if a field has been set.

### GetButtonName

`func (o *SendMessageRequest) GetButtonName() string`

GetButtonName returns the ButtonName field if non-nil, zero value otherwise.

### GetButtonNameOk

`func (o *SendMessageRequest) GetButtonNameOk() (*string, bool)`

GetButtonNameOk returns a tuple with the ButtonName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtonName

`func (o *SendMessageRequest) SetButtonName(v string)`

SetButtonName sets ButtonName field to given value.

### HasButtonName

`func (o *SendMessageRequest) HasButtonName() bool`

HasButtonName returns a boolean if a field has been set.

### GetHeader

`func (o *SendMessageRequest) GetHeader() Header`

GetHeader returns the Header field if non-nil, zero value otherwise.

### GetHeaderOk

`func (o *SendMessageRequest) GetHeaderOk() (*Header, bool)`

GetHeaderOk returns a tuple with the Header field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeader

`func (o *SendMessageRequest) SetHeader(v Header)`

SetHeader sets Header field to given value.

### HasHeader

`func (o *SendMessageRequest) HasHeader() bool`

HasHeader returns a boolean if a field has been set.

### GetFooter

`func (o *SendMessageRequest) GetFooter() Footer`

GetFooter returns the Footer field if non-nil, zero value otherwise.

### GetFooterOk

`func (o *SendMessageRequest) GetFooterOk() (*Footer, bool)`

GetFooterOk returns a tuple with the Footer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFooter

`func (o *SendMessageRequest) SetFooter(v Footer)`

SetFooter sets Footer field to given value.

### HasFooter

`func (o *SendMessageRequest) HasFooter() bool`

HasFooter returns a boolean if a field has been set.

### GetItems

`func (o *SendMessageRequest) GetItems() []MessageItem`

GetItems returns the Items field if non-nil, zero value otherwise.

### GetItemsOk

`func (o *SendMessageRequest) GetItemsOk() (*[]MessageItem, bool)`

GetItemsOk returns a tuple with the Items field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItems

`func (o *SendMessageRequest) SetItems(v []MessageItem)`

SetItems sets Items field to given value.

### HasItems

`func (o *SendMessageRequest) HasItems() bool`

HasItems returns a boolean if a field has been set.

### GetAgent

`func (o *SendMessageRequest) GetAgent() Agent`

GetAgent returns the Agent field if non-nil, zero value otherwise.

### GetAgentOk

`func (o *SendMessageRequest) GetAgentOk() (*Agent, bool)`

GetAgentOk returns a tuple with the Agent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAgent

`func (o *SendMessageRequest) SetAgent(v Agent)`

SetAgent sets Agent field to given value.

### HasAgent

`func (o *SendMessageRequest) HasAgent() bool`

HasAgent returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


