# MessengerLimitsMessageTypes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | [**MessageType**](MessageType.md) |  | 
**Size** | Pointer to **int32** |  | [optional] 
**MimeTypes** | Pointer to **[]string** |  | [optional] 
**ItemSize** | Pointer to **int32** |  | [optional] 
**Header** | Pointer to [**HeaderLimits**](HeaderLimits.md) |  | [optional] 
**Footer** | Pointer to [**FooterLimits**](FooterLimits.md) |  | [optional] 

## Methods

### NewMessengerLimitsMessageTypes

`func NewMessengerLimitsMessageTypes(type_ MessageType, ) *MessengerLimitsMessageTypes`

NewMessengerLimitsMessageTypes instantiates a new MessengerLimitsMessageTypes object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMessengerLimitsMessageTypesWithDefaults

`func NewMessengerLimitsMessageTypesWithDefaults() *MessengerLimitsMessageTypes`

NewMessengerLimitsMessageTypesWithDefaults instantiates a new MessengerLimitsMessageTypes object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *MessengerLimitsMessageTypes) GetType() MessageType`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MessengerLimitsMessageTypes) GetTypeOk() (*MessageType, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MessengerLimitsMessageTypes) SetType(v MessageType)`

SetType sets Type field to given value.


### GetSize

`func (o *MessengerLimitsMessageTypes) GetSize() int32`

GetSize returns the Size field if non-nil, zero value otherwise.

### GetSizeOk

`func (o *MessengerLimitsMessageTypes) GetSizeOk() (*int32, bool)`

GetSizeOk returns a tuple with the Size field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSize

`func (o *MessengerLimitsMessageTypes) SetSize(v int32)`

SetSize sets Size field to given value.

### HasSize

`func (o *MessengerLimitsMessageTypes) HasSize() bool`

HasSize returns a boolean if a field has been set.

### GetMimeTypes

`func (o *MessengerLimitsMessageTypes) GetMimeTypes() []string`

GetMimeTypes returns the MimeTypes field if non-nil, zero value otherwise.

### GetMimeTypesOk

`func (o *MessengerLimitsMessageTypes) GetMimeTypesOk() (*[]string, bool)`

GetMimeTypesOk returns a tuple with the MimeTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMimeTypes

`func (o *MessengerLimitsMessageTypes) SetMimeTypes(v []string)`

SetMimeTypes sets MimeTypes field to given value.

### HasMimeTypes

`func (o *MessengerLimitsMessageTypes) HasMimeTypes() bool`

HasMimeTypes returns a boolean if a field has been set.

### GetItemSize

`func (o *MessengerLimitsMessageTypes) GetItemSize() int32`

GetItemSize returns the ItemSize field if non-nil, zero value otherwise.

### GetItemSizeOk

`func (o *MessengerLimitsMessageTypes) GetItemSizeOk() (*int32, bool)`

GetItemSizeOk returns a tuple with the ItemSize field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItemSize

`func (o *MessengerLimitsMessageTypes) SetItemSize(v int32)`

SetItemSize sets ItemSize field to given value.

### HasItemSize

`func (o *MessengerLimitsMessageTypes) HasItemSize() bool`

HasItemSize returns a boolean if a field has been set.

### GetHeader

`func (o *MessengerLimitsMessageTypes) GetHeader() HeaderLimits`

GetHeader returns the Header field if non-nil, zero value otherwise.

### GetHeaderOk

`func (o *MessengerLimitsMessageTypes) GetHeaderOk() (*HeaderLimits, bool)`

GetHeaderOk returns a tuple with the Header field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeader

`func (o *MessengerLimitsMessageTypes) SetHeader(v HeaderLimits)`

SetHeader sets Header field to given value.

### HasHeader

`func (o *MessengerLimitsMessageTypes) HasHeader() bool`

HasHeader returns a boolean if a field has been set.

### GetFooter

`func (o *MessengerLimitsMessageTypes) GetFooter() FooterLimits`

GetFooter returns the Footer field if non-nil, zero value otherwise.

### GetFooterOk

`func (o *MessengerLimitsMessageTypes) GetFooterOk() (*FooterLimits, bool)`

GetFooterOk returns a tuple with the Footer field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFooter

`func (o *MessengerLimitsMessageTypes) SetFooter(v FooterLimits)`

SetFooter sets Footer field to given value.

### HasFooter

`func (o *MessengerLimitsMessageTypes) HasFooter() bool`

HasFooter returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


