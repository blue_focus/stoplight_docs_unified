# WhatsAppTemplateCreateRequestComponents

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Format** | Pointer to **string** | Applies to HEADER type only | [optional] 
**Text** | Pointer to **string** |  | [optional] 
**Example** | Pointer to [**WhatsAppTemplateCreateRequestExample**](WhatsAppTemplateCreateRequestExample.md) |  | [optional] 
**Buttons** | Pointer to [**[]WhatsAppTemplateCreateRequestButtons**](WhatsAppTemplateCreateRequestButtons.md) |  | [optional] 

## Methods

### NewWhatsAppTemplateCreateRequestComponents

`func NewWhatsAppTemplateCreateRequestComponents(type_ string, ) *WhatsAppTemplateCreateRequestComponents`

NewWhatsAppTemplateCreateRequestComponents instantiates a new WhatsAppTemplateCreateRequestComponents object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateCreateRequestComponentsWithDefaults

`func NewWhatsAppTemplateCreateRequestComponentsWithDefaults() *WhatsAppTemplateCreateRequestComponents`

NewWhatsAppTemplateCreateRequestComponentsWithDefaults instantiates a new WhatsAppTemplateCreateRequestComponents object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *WhatsAppTemplateCreateRequestComponents) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WhatsAppTemplateCreateRequestComponents) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WhatsAppTemplateCreateRequestComponents) SetType(v string)`

SetType sets Type field to given value.


### GetFormat

`func (o *WhatsAppTemplateCreateRequestComponents) GetFormat() string`

GetFormat returns the Format field if non-nil, zero value otherwise.

### GetFormatOk

`func (o *WhatsAppTemplateCreateRequestComponents) GetFormatOk() (*string, bool)`

GetFormatOk returns a tuple with the Format field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormat

`func (o *WhatsAppTemplateCreateRequestComponents) SetFormat(v string)`

SetFormat sets Format field to given value.

### HasFormat

`func (o *WhatsAppTemplateCreateRequestComponents) HasFormat() bool`

HasFormat returns a boolean if a field has been set.

### GetText

`func (o *WhatsAppTemplateCreateRequestComponents) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *WhatsAppTemplateCreateRequestComponents) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *WhatsAppTemplateCreateRequestComponents) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *WhatsAppTemplateCreateRequestComponents) HasText() bool`

HasText returns a boolean if a field has been set.

### GetExample

`func (o *WhatsAppTemplateCreateRequestComponents) GetExample() WhatsAppTemplateCreateRequestExample`

GetExample returns the Example field if non-nil, zero value otherwise.

### GetExampleOk

`func (o *WhatsAppTemplateCreateRequestComponents) GetExampleOk() (*WhatsAppTemplateCreateRequestExample, bool)`

GetExampleOk returns a tuple with the Example field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExample

`func (o *WhatsAppTemplateCreateRequestComponents) SetExample(v WhatsAppTemplateCreateRequestExample)`

SetExample sets Example field to given value.

### HasExample

`func (o *WhatsAppTemplateCreateRequestComponents) HasExample() bool`

HasExample returns a boolean if a field has been set.

### GetButtons

`func (o *WhatsAppTemplateCreateRequestComponents) GetButtons() []WhatsAppTemplateCreateRequestButtons`

GetButtons returns the Buttons field if non-nil, zero value otherwise.

### GetButtonsOk

`func (o *WhatsAppTemplateCreateRequestComponents) GetButtonsOk() (*[]WhatsAppTemplateCreateRequestButtons, bool)`

GetButtonsOk returns a tuple with the Buttons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtons

`func (o *WhatsAppTemplateCreateRequestComponents) SetButtons(v []WhatsAppTemplateCreateRequestButtons)`

SetButtons sets Buttons field to given value.

### HasButtons

`func (o *WhatsAppTemplateCreateRequestComponents) HasButtons() bool`

HasButtons returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


