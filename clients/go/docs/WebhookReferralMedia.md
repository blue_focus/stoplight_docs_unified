# WebhookReferralMedia

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 

## Methods

### NewWebhookReferralMedia

`func NewWebhookReferralMedia() *WebhookReferralMedia`

NewWebhookReferralMedia instantiates a new WebhookReferralMedia object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookReferralMediaWithDefaults

`func NewWebhookReferralMediaWithDefaults() *WebhookReferralMedia`

NewWebhookReferralMediaWithDefaults instantiates a new WebhookReferralMedia object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WebhookReferralMedia) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WebhookReferralMedia) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WebhookReferralMedia) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *WebhookReferralMedia) HasId() bool`

HasId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


