# WhatsappPhoneNumber

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayPhoneNumber** | Pointer to **string** |  | [optional] 
**QualityRating** | Pointer to **string** |  | [optional] 

## Methods

### NewWhatsappPhoneNumber

`func NewWhatsappPhoneNumber() *WhatsappPhoneNumber`

NewWhatsappPhoneNumber instantiates a new WhatsappPhoneNumber object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsappPhoneNumberWithDefaults

`func NewWhatsappPhoneNumberWithDefaults() *WhatsappPhoneNumber`

NewWhatsappPhoneNumberWithDefaults instantiates a new WhatsappPhoneNumber object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDisplayPhoneNumber

`func (o *WhatsappPhoneNumber) GetDisplayPhoneNumber() string`

GetDisplayPhoneNumber returns the DisplayPhoneNumber field if non-nil, zero value otherwise.

### GetDisplayPhoneNumberOk

`func (o *WhatsappPhoneNumber) GetDisplayPhoneNumberOk() (*string, bool)`

GetDisplayPhoneNumberOk returns a tuple with the DisplayPhoneNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisplayPhoneNumber

`func (o *WhatsappPhoneNumber) SetDisplayPhoneNumber(v string)`

SetDisplayPhoneNumber sets DisplayPhoneNumber field to given value.

### HasDisplayPhoneNumber

`func (o *WhatsappPhoneNumber) HasDisplayPhoneNumber() bool`

HasDisplayPhoneNumber returns a boolean if a field has been set.

### GetQualityRating

`func (o *WhatsappPhoneNumber) GetQualityRating() string`

GetQualityRating returns the QualityRating field if non-nil, zero value otherwise.

### GetQualityRatingOk

`func (o *WhatsappPhoneNumber) GetQualityRatingOk() (*string, bool)`

GetQualityRatingOk returns a tuple with the QualityRating field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetQualityRating

`func (o *WhatsappPhoneNumber) SetQualityRating(v string)`

SetQualityRating sets QualityRating field to given value.

### HasQualityRating

`func (o *WhatsappPhoneNumber) HasQualityRating() bool`

HasQualityRating returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


