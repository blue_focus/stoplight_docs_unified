# WebhookMessageVideo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Url** | **string** |  | 
**Mime** | **string** |  | 
**Width** | **int32** |  | 
**Height** | **int32** |  | 
**Filename** | Pointer to **string** |  | [optional] 

## Methods

### NewWebhookMessageVideo

`func NewWebhookMessageVideo(url string, mime string, width int32, height int32, ) *WebhookMessageVideo`

NewWebhookMessageVideo instantiates a new WebhookMessageVideo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookMessageVideoWithDefaults

`func NewWebhookMessageVideoWithDefaults() *WebhookMessageVideo`

NewWebhookMessageVideoWithDefaults instantiates a new WebhookMessageVideo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WebhookMessageVideo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WebhookMessageVideo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WebhookMessageVideo) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *WebhookMessageVideo) HasId() bool`

HasId returns a boolean if a field has been set.

### GetUrl

`func (o *WebhookMessageVideo) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *WebhookMessageVideo) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *WebhookMessageVideo) SetUrl(v string)`

SetUrl sets Url field to given value.


### GetMime

`func (o *WebhookMessageVideo) GetMime() string`

GetMime returns the Mime field if non-nil, zero value otherwise.

### GetMimeOk

`func (o *WebhookMessageVideo) GetMimeOk() (*string, bool)`

GetMimeOk returns a tuple with the Mime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMime

`func (o *WebhookMessageVideo) SetMime(v string)`

SetMime sets Mime field to given value.


### GetWidth

`func (o *WebhookMessageVideo) GetWidth() int32`

GetWidth returns the Width field if non-nil, zero value otherwise.

### GetWidthOk

`func (o *WebhookMessageVideo) GetWidthOk() (*int32, bool)`

GetWidthOk returns a tuple with the Width field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWidth

`func (o *WebhookMessageVideo) SetWidth(v int32)`

SetWidth sets Width field to given value.


### GetHeight

`func (o *WebhookMessageVideo) GetHeight() int32`

GetHeight returns the Height field if non-nil, zero value otherwise.

### GetHeightOk

`func (o *WebhookMessageVideo) GetHeightOk() (*int32, bool)`

GetHeightOk returns a tuple with the Height field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeight

`func (o *WebhookMessageVideo) SetHeight(v int32)`

SetHeight sets Height field to given value.


### GetFilename

`func (o *WebhookMessageVideo) GetFilename() string`

GetFilename returns the Filename field if non-nil, zero value otherwise.

### GetFilenameOk

`func (o *WebhookMessageVideo) GetFilenameOk() (*string, bool)`

GetFilenameOk returns a tuple with the Filename field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFilename

`func (o *WebhookMessageVideo) SetFilename(v string)`

SetFilename sets Filename field to given value.

### HasFilename

`func (o *WebhookMessageVideo) HasFilename() bool`

HasFilename returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


