# WhatsAppTemplateMutableButtons

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Text** | **string** |  | 
**Url** | Pointer to **string** |  | [optional] 
**PhoneNumber** | Pointer to **string** |  | [optional] 
**Example** | Pointer to **[]string** |  | [optional] 

## Methods

### NewWhatsAppTemplateMutableButtons

`func NewWhatsAppTemplateMutableButtons(type_ string, text string, ) *WhatsAppTemplateMutableButtons`

NewWhatsAppTemplateMutableButtons instantiates a new WhatsAppTemplateMutableButtons object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMutableButtonsWithDefaults

`func NewWhatsAppTemplateMutableButtonsWithDefaults() *WhatsAppTemplateMutableButtons`

NewWhatsAppTemplateMutableButtonsWithDefaults instantiates a new WhatsAppTemplateMutableButtons object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *WhatsAppTemplateMutableButtons) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WhatsAppTemplateMutableButtons) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WhatsAppTemplateMutableButtons) SetType(v string)`

SetType sets Type field to given value.


### GetText

`func (o *WhatsAppTemplateMutableButtons) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *WhatsAppTemplateMutableButtons) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *WhatsAppTemplateMutableButtons) SetText(v string)`

SetText sets Text field to given value.


### GetUrl

`func (o *WhatsAppTemplateMutableButtons) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *WhatsAppTemplateMutableButtons) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *WhatsAppTemplateMutableButtons) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *WhatsAppTemplateMutableButtons) HasUrl() bool`

HasUrl returns a boolean if a field has been set.

### GetPhoneNumber

`func (o *WhatsAppTemplateMutableButtons) GetPhoneNumber() string`

GetPhoneNumber returns the PhoneNumber field if non-nil, zero value otherwise.

### GetPhoneNumberOk

`func (o *WhatsAppTemplateMutableButtons) GetPhoneNumberOk() (*string, bool)`

GetPhoneNumberOk returns a tuple with the PhoneNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhoneNumber

`func (o *WhatsAppTemplateMutableButtons) SetPhoneNumber(v string)`

SetPhoneNumber sets PhoneNumber field to given value.

### HasPhoneNumber

`func (o *WhatsAppTemplateMutableButtons) HasPhoneNumber() bool`

HasPhoneNumber returns a boolean if a field has been set.

### GetExample

`func (o *WhatsAppTemplateMutableButtons) GetExample() []string`

GetExample returns the Example field if non-nil, zero value otherwise.

### GetExampleOk

`func (o *WhatsAppTemplateMutableButtons) GetExampleOk() (*[]string, bool)`

GetExampleOk returns a tuple with the Example field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExample

`func (o *WhatsAppTemplateMutableButtons) SetExample(v []string)`

SetExample sets Example field to given value.

### HasExample

`func (o *WhatsAppTemplateMutableButtons) HasExample() bool`

HasExample returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


