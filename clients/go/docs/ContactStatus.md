# ContactStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Input** | **string** |  | 
**WaId** | Pointer to **string** |  | [optional] 
**Status** | **string** |  | 

## Methods

### NewContactStatus

`func NewContactStatus(input string, status string, ) *ContactStatus`

NewContactStatus instantiates a new ContactStatus object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactStatusWithDefaults

`func NewContactStatusWithDefaults() *ContactStatus`

NewContactStatusWithDefaults instantiates a new ContactStatus object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInput

`func (o *ContactStatus) GetInput() string`

GetInput returns the Input field if non-nil, zero value otherwise.

### GetInputOk

`func (o *ContactStatus) GetInputOk() (*string, bool)`

GetInputOk returns a tuple with the Input field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInput

`func (o *ContactStatus) SetInput(v string)`

SetInput sets Input field to given value.


### GetWaId

`func (o *ContactStatus) GetWaId() string`

GetWaId returns the WaId field if non-nil, zero value otherwise.

### GetWaIdOk

`func (o *ContactStatus) GetWaIdOk() (*string, bool)`

GetWaIdOk returns a tuple with the WaId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWaId

`func (o *ContactStatus) SetWaId(v string)`

SetWaId sets WaId field to given value.

### HasWaId

`func (o *ContactStatus) HasWaId() bool`

HasWaId returns a boolean if a field has been set.

### GetStatus

`func (o *ContactStatus) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *ContactStatus) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *ContactStatus) SetStatus(v string)`

SetStatus sets Status field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


