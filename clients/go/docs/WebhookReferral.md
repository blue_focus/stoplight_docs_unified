# WebhookReferral

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Headline** | Pointer to **string** |  | [optional] 
**Body** | Pointer to **string** |  | [optional] 
**SourceType** | Pointer to **string** |  | [optional] 
**SourceId** | Pointer to **string** |  | [optional] 
**SourceUrl** | Pointer to **string** |  | [optional] 
**Image** | Pointer to [**WebhookReferralMedia**](WebhookReferralMedia.md) |  | [optional] 
**Video** | Pointer to [**WebhookReferralMedia**](WebhookReferralMedia.md) |  | [optional] 

## Methods

### NewWebhookReferral

`func NewWebhookReferral() *WebhookReferral`

NewWebhookReferral instantiates a new WebhookReferral object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookReferralWithDefaults

`func NewWebhookReferralWithDefaults() *WebhookReferral`

NewWebhookReferralWithDefaults instantiates a new WebhookReferral object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetHeadline

`func (o *WebhookReferral) GetHeadline() string`

GetHeadline returns the Headline field if non-nil, zero value otherwise.

### GetHeadlineOk

`func (o *WebhookReferral) GetHeadlineOk() (*string, bool)`

GetHeadlineOk returns a tuple with the Headline field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeadline

`func (o *WebhookReferral) SetHeadline(v string)`

SetHeadline sets Headline field to given value.

### HasHeadline

`func (o *WebhookReferral) HasHeadline() bool`

HasHeadline returns a boolean if a field has been set.

### GetBody

`func (o *WebhookReferral) GetBody() string`

GetBody returns the Body field if non-nil, zero value otherwise.

### GetBodyOk

`func (o *WebhookReferral) GetBodyOk() (*string, bool)`

GetBodyOk returns a tuple with the Body field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBody

`func (o *WebhookReferral) SetBody(v string)`

SetBody sets Body field to given value.

### HasBody

`func (o *WebhookReferral) HasBody() bool`

HasBody returns a boolean if a field has been set.

### GetSourceType

`func (o *WebhookReferral) GetSourceType() string`

GetSourceType returns the SourceType field if non-nil, zero value otherwise.

### GetSourceTypeOk

`func (o *WebhookReferral) GetSourceTypeOk() (*string, bool)`

GetSourceTypeOk returns a tuple with the SourceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceType

`func (o *WebhookReferral) SetSourceType(v string)`

SetSourceType sets SourceType field to given value.

### HasSourceType

`func (o *WebhookReferral) HasSourceType() bool`

HasSourceType returns a boolean if a field has been set.

### GetSourceId

`func (o *WebhookReferral) GetSourceId() string`

GetSourceId returns the SourceId field if non-nil, zero value otherwise.

### GetSourceIdOk

`func (o *WebhookReferral) GetSourceIdOk() (*string, bool)`

GetSourceIdOk returns a tuple with the SourceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceId

`func (o *WebhookReferral) SetSourceId(v string)`

SetSourceId sets SourceId field to given value.

### HasSourceId

`func (o *WebhookReferral) HasSourceId() bool`

HasSourceId returns a boolean if a field has been set.

### GetSourceUrl

`func (o *WebhookReferral) GetSourceUrl() string`

GetSourceUrl returns the SourceUrl field if non-nil, zero value otherwise.

### GetSourceUrlOk

`func (o *WebhookReferral) GetSourceUrlOk() (*string, bool)`

GetSourceUrlOk returns a tuple with the SourceUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceUrl

`func (o *WebhookReferral) SetSourceUrl(v string)`

SetSourceUrl sets SourceUrl field to given value.

### HasSourceUrl

`func (o *WebhookReferral) HasSourceUrl() bool`

HasSourceUrl returns a boolean if a field has been set.

### GetImage

`func (o *WebhookReferral) GetImage() WebhookReferralMedia`

GetImage returns the Image field if non-nil, zero value otherwise.

### GetImageOk

`func (o *WebhookReferral) GetImageOk() (*WebhookReferralMedia, bool)`

GetImageOk returns a tuple with the Image field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetImage

`func (o *WebhookReferral) SetImage(v WebhookReferralMedia)`

SetImage sets Image field to given value.

### HasImage

`func (o *WebhookReferral) HasImage() bool`

HasImage returns a boolean if a field has been set.

### GetVideo

`func (o *WebhookReferral) GetVideo() WebhookReferralMedia`

GetVideo returns the Video field if non-nil, zero value otherwise.

### GetVideoOk

`func (o *WebhookReferral) GetVideoOk() (*WebhookReferralMedia, bool)`

GetVideoOk returns a tuple with the Video field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVideo

`func (o *WebhookReferral) SetVideo(v WebhookReferralMedia)`

SetVideo sets Video field to given value.

### HasVideo

`func (o *WebhookReferral) HasVideo() bool`

HasVideo returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


