# Messenger

## Enum


* `MESSENGER_WHATS_APP` (value: `"WhatsApp"`)

* `MESSENGER_TELEGRAM_BOT` (value: `"TelegramBot"`)

* `MESSENGER_WEBCHAT` (value: `"Webchat"`)

* `MESSENGER_FACEBOOK_MESSENGER` (value: `"FacebookMessenger"`)

* `MESSENGER_INSTAGRAM` (value: `"Instagram"`)

* `MESSENGER_GOOGLE_BM` (value: `"GoogleBM"`)

* `MESSENGER_CHATWERK_CONNECT` (value: `"ChatWerkConnect"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


