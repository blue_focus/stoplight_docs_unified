# WhatsAppTemplateMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**To** | **string** |  | 
**Ttl** | Pointer to **string** |  | [optional] 
**Namespace** | **string** |  | 
**Name** | **string** |  | 
**Language** | **string** |  | 
**Components** | Pointer to [**[]WhatsAppTemplateMessageComponents**](WhatsAppTemplateMessageComponents.md) |  | [optional] 

## Methods

### NewWhatsAppTemplateMessage

`func NewWhatsAppTemplateMessage(to string, namespace string, name string, language string, ) *WhatsAppTemplateMessage`

NewWhatsAppTemplateMessage instantiates a new WhatsAppTemplateMessage object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMessageWithDefaults

`func NewWhatsAppTemplateMessageWithDefaults() *WhatsAppTemplateMessage`

NewWhatsAppTemplateMessageWithDefaults instantiates a new WhatsAppTemplateMessage object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTo

`func (o *WhatsAppTemplateMessage) GetTo() string`

GetTo returns the To field if non-nil, zero value otherwise.

### GetToOk

`func (o *WhatsAppTemplateMessage) GetToOk() (*string, bool)`

GetToOk returns a tuple with the To field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTo

`func (o *WhatsAppTemplateMessage) SetTo(v string)`

SetTo sets To field to given value.


### GetTtl

`func (o *WhatsAppTemplateMessage) GetTtl() string`

GetTtl returns the Ttl field if non-nil, zero value otherwise.

### GetTtlOk

`func (o *WhatsAppTemplateMessage) GetTtlOk() (*string, bool)`

GetTtlOk returns a tuple with the Ttl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTtl

`func (o *WhatsAppTemplateMessage) SetTtl(v string)`

SetTtl sets Ttl field to given value.

### HasTtl

`func (o *WhatsAppTemplateMessage) HasTtl() bool`

HasTtl returns a boolean if a field has been set.

### GetNamespace

`func (o *WhatsAppTemplateMessage) GetNamespace() string`

GetNamespace returns the Namespace field if non-nil, zero value otherwise.

### GetNamespaceOk

`func (o *WhatsAppTemplateMessage) GetNamespaceOk() (*string, bool)`

GetNamespaceOk returns a tuple with the Namespace field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNamespace

`func (o *WhatsAppTemplateMessage) SetNamespace(v string)`

SetNamespace sets Namespace field to given value.


### GetName

`func (o *WhatsAppTemplateMessage) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *WhatsAppTemplateMessage) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *WhatsAppTemplateMessage) SetName(v string)`

SetName sets Name field to given value.


### GetLanguage

`func (o *WhatsAppTemplateMessage) GetLanguage() string`

GetLanguage returns the Language field if non-nil, zero value otherwise.

### GetLanguageOk

`func (o *WhatsAppTemplateMessage) GetLanguageOk() (*string, bool)`

GetLanguageOk returns a tuple with the Language field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLanguage

`func (o *WhatsAppTemplateMessage) SetLanguage(v string)`

SetLanguage sets Language field to given value.


### GetComponents

`func (o *WhatsAppTemplateMessage) GetComponents() []WhatsAppTemplateMessageComponents`

GetComponents returns the Components field if non-nil, zero value otherwise.

### GetComponentsOk

`func (o *WhatsAppTemplateMessage) GetComponentsOk() (*[]WhatsAppTemplateMessageComponents, bool)`

GetComponentsOk returns a tuple with the Components field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComponents

`func (o *WhatsAppTemplateMessage) SetComponents(v []WhatsAppTemplateMessageComponents)`

SetComponents sets Components field to given value.

### HasComponents

`func (o *WhatsAppTemplateMessage) HasComponents() bool`

HasComponents returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


