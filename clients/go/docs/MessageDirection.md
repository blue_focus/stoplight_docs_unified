# MessageDirection

## Enum


* `DIRECTION_IN` (value: `"in"`)

* `DIRECTION_OUT` (value: `"out"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


