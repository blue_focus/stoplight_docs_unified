# WebhookPricing

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PricingModel** | Pointer to **string** |  | [optional] 
**Billable** | Pointer to **bool** |  | [optional] 

## Methods

### NewWebhookPricing

`func NewWebhookPricing() *WebhookPricing`

NewWebhookPricing instantiates a new WebhookPricing object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookPricingWithDefaults

`func NewWebhookPricingWithDefaults() *WebhookPricing`

NewWebhookPricingWithDefaults instantiates a new WebhookPricing object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPricingModel

`func (o *WebhookPricing) GetPricingModel() string`

GetPricingModel returns the PricingModel field if non-nil, zero value otherwise.

### GetPricingModelOk

`func (o *WebhookPricing) GetPricingModelOk() (*string, bool)`

GetPricingModelOk returns a tuple with the PricingModel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPricingModel

`func (o *WebhookPricing) SetPricingModel(v string)`

SetPricingModel sets PricingModel field to given value.

### HasPricingModel

`func (o *WebhookPricing) HasPricingModel() bool`

HasPricingModel returns a boolean if a field has been set.

### GetBillable

`func (o *WebhookPricing) GetBillable() bool`

GetBillable returns the Billable field if non-nil, zero value otherwise.

### GetBillableOk

`func (o *WebhookPricing) GetBillableOk() (*bool, bool)`

GetBillableOk returns a tuple with the Billable field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBillable

`func (o *WebhookPricing) SetBillable(v bool)`

SetBillable sets Billable field to given value.

### HasBillable

`func (o *WebhookPricing) HasBillable() bool`

HasBillable returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


