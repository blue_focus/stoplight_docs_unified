# OfficeTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hours** | **int32** |  | 
**Minutes** | **int32** |  | 
**Seconds** | Pointer to **int32** |  | [optional] 

## Methods

### NewOfficeTime

`func NewOfficeTime(hours int32, minutes int32, ) *OfficeTime`

NewOfficeTime instantiates a new OfficeTime object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOfficeTimeWithDefaults

`func NewOfficeTimeWithDefaults() *OfficeTime`

NewOfficeTimeWithDefaults instantiates a new OfficeTime object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetHours

`func (o *OfficeTime) GetHours() int32`

GetHours returns the Hours field if non-nil, zero value otherwise.

### GetHoursOk

`func (o *OfficeTime) GetHoursOk() (*int32, bool)`

GetHoursOk returns a tuple with the Hours field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHours

`func (o *OfficeTime) SetHours(v int32)`

SetHours sets Hours field to given value.


### GetMinutes

`func (o *OfficeTime) GetMinutes() int32`

GetMinutes returns the Minutes field if non-nil, zero value otherwise.

### GetMinutesOk

`func (o *OfficeTime) GetMinutesOk() (*int32, bool)`

GetMinutesOk returns a tuple with the Minutes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMinutes

`func (o *OfficeTime) SetMinutes(v int32)`

SetMinutes sets Minutes field to given value.


### GetSeconds

`func (o *OfficeTime) GetSeconds() int32`

GetSeconds returns the Seconds field if non-nil, zero value otherwise.

### GetSecondsOk

`func (o *OfficeTime) GetSecondsOk() (*int32, bool)`

GetSecondsOk returns a tuple with the Seconds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSeconds

`func (o *OfficeTime) SetSeconds(v int32)`

SetSeconds sets Seconds field to given value.

### HasSeconds

`func (o *OfficeTime) HasSeconds() bool`

HasSeconds returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


