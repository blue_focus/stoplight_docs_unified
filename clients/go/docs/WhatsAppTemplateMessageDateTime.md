# WhatsAppTemplateMessageDateTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FallbackValue** | Pointer to **string** |  | [optional] 
**Timestamp** | Pointer to **string** |  | [optional] 
**DayOfWeek** | Pointer to **string** |  | [optional] 
**DayOfMonth** | Pointer to **string** |  | [optional] 
**Year** | Pointer to **string** |  | [optional] 
**Month** | Pointer to **string** |  | [optional] 
**Hour** | Pointer to **string** |  | [optional] 
**Minute** | Pointer to **string** |  | [optional] 

## Methods

### NewWhatsAppTemplateMessageDateTime

`func NewWhatsAppTemplateMessageDateTime() *WhatsAppTemplateMessageDateTime`

NewWhatsAppTemplateMessageDateTime instantiates a new WhatsAppTemplateMessageDateTime object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMessageDateTimeWithDefaults

`func NewWhatsAppTemplateMessageDateTimeWithDefaults() *WhatsAppTemplateMessageDateTime`

NewWhatsAppTemplateMessageDateTimeWithDefaults instantiates a new WhatsAppTemplateMessageDateTime object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFallbackValue

`func (o *WhatsAppTemplateMessageDateTime) GetFallbackValue() string`

GetFallbackValue returns the FallbackValue field if non-nil, zero value otherwise.

### GetFallbackValueOk

`func (o *WhatsAppTemplateMessageDateTime) GetFallbackValueOk() (*string, bool)`

GetFallbackValueOk returns a tuple with the FallbackValue field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFallbackValue

`func (o *WhatsAppTemplateMessageDateTime) SetFallbackValue(v string)`

SetFallbackValue sets FallbackValue field to given value.

### HasFallbackValue

`func (o *WhatsAppTemplateMessageDateTime) HasFallbackValue() bool`

HasFallbackValue returns a boolean if a field has been set.

### GetTimestamp

`func (o *WhatsAppTemplateMessageDateTime) GetTimestamp() string`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *WhatsAppTemplateMessageDateTime) GetTimestampOk() (*string, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *WhatsAppTemplateMessageDateTime) SetTimestamp(v string)`

SetTimestamp sets Timestamp field to given value.

### HasTimestamp

`func (o *WhatsAppTemplateMessageDateTime) HasTimestamp() bool`

HasTimestamp returns a boolean if a field has been set.

### GetDayOfWeek

`func (o *WhatsAppTemplateMessageDateTime) GetDayOfWeek() string`

GetDayOfWeek returns the DayOfWeek field if non-nil, zero value otherwise.

### GetDayOfWeekOk

`func (o *WhatsAppTemplateMessageDateTime) GetDayOfWeekOk() (*string, bool)`

GetDayOfWeekOk returns a tuple with the DayOfWeek field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDayOfWeek

`func (o *WhatsAppTemplateMessageDateTime) SetDayOfWeek(v string)`

SetDayOfWeek sets DayOfWeek field to given value.

### HasDayOfWeek

`func (o *WhatsAppTemplateMessageDateTime) HasDayOfWeek() bool`

HasDayOfWeek returns a boolean if a field has been set.

### GetDayOfMonth

`func (o *WhatsAppTemplateMessageDateTime) GetDayOfMonth() string`

GetDayOfMonth returns the DayOfMonth field if non-nil, zero value otherwise.

### GetDayOfMonthOk

`func (o *WhatsAppTemplateMessageDateTime) GetDayOfMonthOk() (*string, bool)`

GetDayOfMonthOk returns a tuple with the DayOfMonth field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDayOfMonth

`func (o *WhatsAppTemplateMessageDateTime) SetDayOfMonth(v string)`

SetDayOfMonth sets DayOfMonth field to given value.

### HasDayOfMonth

`func (o *WhatsAppTemplateMessageDateTime) HasDayOfMonth() bool`

HasDayOfMonth returns a boolean if a field has been set.

### GetYear

`func (o *WhatsAppTemplateMessageDateTime) GetYear() string`

GetYear returns the Year field if non-nil, zero value otherwise.

### GetYearOk

`func (o *WhatsAppTemplateMessageDateTime) GetYearOk() (*string, bool)`

GetYearOk returns a tuple with the Year field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetYear

`func (o *WhatsAppTemplateMessageDateTime) SetYear(v string)`

SetYear sets Year field to given value.

### HasYear

`func (o *WhatsAppTemplateMessageDateTime) HasYear() bool`

HasYear returns a boolean if a field has been set.

### GetMonth

`func (o *WhatsAppTemplateMessageDateTime) GetMonth() string`

GetMonth returns the Month field if non-nil, zero value otherwise.

### GetMonthOk

`func (o *WhatsAppTemplateMessageDateTime) GetMonthOk() (*string, bool)`

GetMonthOk returns a tuple with the Month field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMonth

`func (o *WhatsAppTemplateMessageDateTime) SetMonth(v string)`

SetMonth sets Month field to given value.

### HasMonth

`func (o *WhatsAppTemplateMessageDateTime) HasMonth() bool`

HasMonth returns a boolean if a field has been set.

### GetHour

`func (o *WhatsAppTemplateMessageDateTime) GetHour() string`

GetHour returns the Hour field if non-nil, zero value otherwise.

### GetHourOk

`func (o *WhatsAppTemplateMessageDateTime) GetHourOk() (*string, bool)`

GetHourOk returns a tuple with the Hour field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHour

`func (o *WhatsAppTemplateMessageDateTime) SetHour(v string)`

SetHour sets Hour field to given value.

### HasHour

`func (o *WhatsAppTemplateMessageDateTime) HasHour() bool`

HasHour returns a boolean if a field has been set.

### GetMinute

`func (o *WhatsAppTemplateMessageDateTime) GetMinute() string`

GetMinute returns the Minute field if non-nil, zero value otherwise.

### GetMinuteOk

`func (o *WhatsAppTemplateMessageDateTime) GetMinuteOk() (*string, bool)`

GetMinuteOk returns a tuple with the Minute field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMinute

`func (o *WhatsAppTemplateMessageDateTime) SetMinute(v string)`

SetMinute sets Minute field to given value.

### HasMinute

`func (o *WhatsAppTemplateMessageDateTime) HasMinute() bool`

HasMinute returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


