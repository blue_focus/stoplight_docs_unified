# WhatsAppTemplateMutableComponents

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Format** | Pointer to **string** | Applies to HEADER type only | [optional] 
**Text** | Pointer to **string** |  | [optional] 
**Example** | Pointer to [**WhatsAppTemplateMutableExample**](WhatsAppTemplateMutableExample.md) |  | [optional] 
**Buttons** | Pointer to [**[]WhatsAppTemplateMutableButtons**](WhatsAppTemplateMutableButtons.md) |  | [optional] 

## Methods

### NewWhatsAppTemplateMutableComponents

`func NewWhatsAppTemplateMutableComponents(type_ string, ) *WhatsAppTemplateMutableComponents`

NewWhatsAppTemplateMutableComponents instantiates a new WhatsAppTemplateMutableComponents object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMutableComponentsWithDefaults

`func NewWhatsAppTemplateMutableComponentsWithDefaults() *WhatsAppTemplateMutableComponents`

NewWhatsAppTemplateMutableComponentsWithDefaults instantiates a new WhatsAppTemplateMutableComponents object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *WhatsAppTemplateMutableComponents) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *WhatsAppTemplateMutableComponents) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *WhatsAppTemplateMutableComponents) SetType(v string)`

SetType sets Type field to given value.


### GetFormat

`func (o *WhatsAppTemplateMutableComponents) GetFormat() string`

GetFormat returns the Format field if non-nil, zero value otherwise.

### GetFormatOk

`func (o *WhatsAppTemplateMutableComponents) GetFormatOk() (*string, bool)`

GetFormatOk returns a tuple with the Format field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormat

`func (o *WhatsAppTemplateMutableComponents) SetFormat(v string)`

SetFormat sets Format field to given value.

### HasFormat

`func (o *WhatsAppTemplateMutableComponents) HasFormat() bool`

HasFormat returns a boolean if a field has been set.

### GetText

`func (o *WhatsAppTemplateMutableComponents) GetText() string`

GetText returns the Text field if non-nil, zero value otherwise.

### GetTextOk

`func (o *WhatsAppTemplateMutableComponents) GetTextOk() (*string, bool)`

GetTextOk returns a tuple with the Text field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetText

`func (o *WhatsAppTemplateMutableComponents) SetText(v string)`

SetText sets Text field to given value.

### HasText

`func (o *WhatsAppTemplateMutableComponents) HasText() bool`

HasText returns a boolean if a field has been set.

### GetExample

`func (o *WhatsAppTemplateMutableComponents) GetExample() WhatsAppTemplateMutableExample`

GetExample returns the Example field if non-nil, zero value otherwise.

### GetExampleOk

`func (o *WhatsAppTemplateMutableComponents) GetExampleOk() (*WhatsAppTemplateMutableExample, bool)`

GetExampleOk returns a tuple with the Example field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExample

`func (o *WhatsAppTemplateMutableComponents) SetExample(v WhatsAppTemplateMutableExample)`

SetExample sets Example field to given value.

### HasExample

`func (o *WhatsAppTemplateMutableComponents) HasExample() bool`

HasExample returns a boolean if a field has been set.

### GetButtons

`func (o *WhatsAppTemplateMutableComponents) GetButtons() []WhatsAppTemplateMutableButtons`

GetButtons returns the Buttons field if non-nil, zero value otherwise.

### GetButtonsOk

`func (o *WhatsAppTemplateMutableComponents) GetButtonsOk() (*[]WhatsAppTemplateMutableButtons, bool)`

GetButtonsOk returns a tuple with the Buttons field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetButtons

`func (o *WhatsAppTemplateMutableComponents) SetButtons(v []WhatsAppTemplateMutableButtons)`

SetButtons sets Buttons field to given value.

### HasButtons

`func (o *WhatsAppTemplateMutableComponents) HasButtons() bool`

HasButtons returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


