# ProfilePhoto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MimeType** | Pointer to **string** |  | [optional] 
**Binary** | Pointer to **string** | base64 encoded image data | [optional] 
**Url** | Pointer to **string** |  | [optional] 

## Methods

### NewProfilePhoto

`func NewProfilePhoto() *ProfilePhoto`

NewProfilePhoto instantiates a new ProfilePhoto object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProfilePhotoWithDefaults

`func NewProfilePhotoWithDefaults() *ProfilePhoto`

NewProfilePhotoWithDefaults instantiates a new ProfilePhoto object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMimeType

`func (o *ProfilePhoto) GetMimeType() string`

GetMimeType returns the MimeType field if non-nil, zero value otherwise.

### GetMimeTypeOk

`func (o *ProfilePhoto) GetMimeTypeOk() (*string, bool)`

GetMimeTypeOk returns a tuple with the MimeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMimeType

`func (o *ProfilePhoto) SetMimeType(v string)`

SetMimeType sets MimeType field to given value.

### HasMimeType

`func (o *ProfilePhoto) HasMimeType() bool`

HasMimeType returns a boolean if a field has been set.

### GetBinary

`func (o *ProfilePhoto) GetBinary() string`

GetBinary returns the Binary field if non-nil, zero value otherwise.

### GetBinaryOk

`func (o *ProfilePhoto) GetBinaryOk() (*string, bool)`

GetBinaryOk returns a tuple with the Binary field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBinary

`func (o *ProfilePhoto) SetBinary(v string)`

SetBinary sets Binary field to given value.

### HasBinary

`func (o *ProfilePhoto) HasBinary() bool`

HasBinary returns a boolean if a field has been set.

### GetUrl

`func (o *ProfilePhoto) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *ProfilePhoto) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *ProfilePhoto) SetUrl(v string)`

SetUrl sets Url field to given value.

### HasUrl

`func (o *ProfilePhoto) HasUrl() bool`

HasUrl returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


