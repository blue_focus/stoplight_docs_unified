# WhatsAppTemplatesList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Templates** | Pointer to [**[]WhatsAppTemplate**](WhatsAppTemplate.md) |  | [optional] 

## Methods

### NewWhatsAppTemplatesList

`func NewWhatsAppTemplatesList() *WhatsAppTemplatesList`

NewWhatsAppTemplatesList instantiates a new WhatsAppTemplatesList object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplatesListWithDefaults

`func NewWhatsAppTemplatesListWithDefaults() *WhatsAppTemplatesList`

NewWhatsAppTemplatesListWithDefaults instantiates a new WhatsAppTemplatesList object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTemplates

`func (o *WhatsAppTemplatesList) GetTemplates() []WhatsAppTemplate`

GetTemplates returns the Templates field if non-nil, zero value otherwise.

### GetTemplatesOk

`func (o *WhatsAppTemplatesList) GetTemplatesOk() (*[]WhatsAppTemplate, bool)`

GetTemplatesOk returns a tuple with the Templates field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTemplates

`func (o *WhatsAppTemplatesList) SetTemplates(v []WhatsAppTemplate)`

SetTemplates sets Templates field to given value.

### HasTemplates

`func (o *WhatsAppTemplatesList) HasTemplates() bool`

HasTemplates returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


