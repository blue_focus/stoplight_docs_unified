# MessageType

## Enum


* `MESSAGE_TEXT` (value: `"text"`)

* `MESSAGE_IMAGE` (value: `"image"`)

* `MESSAGE_VIDEO` (value: `"video"`)

* `MESSAGE_AUDIO` (value: `"audio"`)

* `MESSAGE_DOCUMENT` (value: `"document"`)

* `MESSAGE_FILE` (value: `"file"`)

* `MESSAGE_STICKER` (value: `"sticker"`)

* `MESSAGE_LOCATION` (value: `"location"`)

* `MESSAGE_CONTACT` (value: `"contact"`)

* `MESSAGE_BUTTONS` (value: `"buttons"`)

* `MESSAGE_MULTISELECT` (value: `"multiselect"`)

* `MESSAGE_LIST` (value: `"list"`)

* `MESSAGE_REACTION` (value: `"reaction"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


