# ProfilePhotos

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Photos** | [**[]ProfilePhotosPhotos**](ProfilePhotosPhotos.md) |  | 
**Error** | Pointer to [**ErrorDetails**](ErrorDetails.md) |  | [optional] 

## Methods

### NewProfilePhotos

`func NewProfilePhotos(photos []ProfilePhotosPhotos, ) *ProfilePhotos`

NewProfilePhotos instantiates a new ProfilePhotos object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProfilePhotosWithDefaults

`func NewProfilePhotosWithDefaults() *ProfilePhotos`

NewProfilePhotosWithDefaults instantiates a new ProfilePhotos object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPhotos

`func (o *ProfilePhotos) GetPhotos() []ProfilePhotosPhotos`

GetPhotos returns the Photos field if non-nil, zero value otherwise.

### GetPhotosOk

`func (o *ProfilePhotos) GetPhotosOk() (*[]ProfilePhotosPhotos, bool)`

GetPhotosOk returns a tuple with the Photos field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPhotos

`func (o *ProfilePhotos) SetPhotos(v []ProfilePhotosPhotos)`

SetPhotos sets Photos field to given value.


### GetError

`func (o *ProfilePhotos) GetError() ErrorDetails`

GetError returns the Error field if non-nil, zero value otherwise.

### GetErrorOk

`func (o *ProfilePhotos) GetErrorOk() (*ErrorDetails, bool)`

GetErrorOk returns a tuple with the Error field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetError

`func (o *ProfilePhotos) SetError(v ErrorDetails)`

SetError sets Error field to given value.

### HasError

`func (o *ProfilePhotos) HasError() bool`

HasError returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


