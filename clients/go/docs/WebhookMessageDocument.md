# WebhookMessageDocument

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** |  | [optional] 
**Url** | **string** |  | 
**Mime** | **string** |  | 
**Filename** | Pointer to **string** |  | [optional] 

## Methods

### NewWebhookMessageDocument

`func NewWebhookMessageDocument(url string, mime string, ) *WebhookMessageDocument`

NewWebhookMessageDocument instantiates a new WebhookMessageDocument object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookMessageDocumentWithDefaults

`func NewWebhookMessageDocumentWithDefaults() *WebhookMessageDocument`

NewWebhookMessageDocumentWithDefaults instantiates a new WebhookMessageDocument object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *WebhookMessageDocument) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *WebhookMessageDocument) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *WebhookMessageDocument) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *WebhookMessageDocument) HasId() bool`

HasId returns a boolean if a field has been set.

### GetUrl

`func (o *WebhookMessageDocument) GetUrl() string`

GetUrl returns the Url field if non-nil, zero value otherwise.

### GetUrlOk

`func (o *WebhookMessageDocument) GetUrlOk() (*string, bool)`

GetUrlOk returns a tuple with the Url field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUrl

`func (o *WebhookMessageDocument) SetUrl(v string)`

SetUrl sets Url field to given value.


### GetMime

`func (o *WebhookMessageDocument) GetMime() string`

GetMime returns the Mime field if non-nil, zero value otherwise.

### GetMimeOk

`func (o *WebhookMessageDocument) GetMimeOk() (*string, bool)`

GetMimeOk returns a tuple with the Mime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMime

`func (o *WebhookMessageDocument) SetMime(v string)`

SetMime sets Mime field to given value.


### GetFilename

`func (o *WebhookMessageDocument) GetFilename() string`

GetFilename returns the Filename field if non-nil, zero value otherwise.

### GetFilenameOk

`func (o *WebhookMessageDocument) GetFilenameOk() (*string, bool)`

GetFilenameOk returns a tuple with the Filename field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFilename

`func (o *WebhookMessageDocument) SetFilename(v string)`

SetFilename sets Filename field to given value.

### HasFilename

`func (o *WebhookMessageDocument) HasFilename() bool`

HasFilename returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


