# WhatsAppTemplateMutable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Category** | Pointer to **string** |  | [optional] 
**Components** | Pointer to [**[]WhatsAppTemplateMutableComponents**](WhatsAppTemplateMutableComponents.md) |  | [optional] 

## Methods

### NewWhatsAppTemplateMutable

`func NewWhatsAppTemplateMutable() *WhatsAppTemplateMutable`

NewWhatsAppTemplateMutable instantiates a new WhatsAppTemplateMutable object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateMutableWithDefaults

`func NewWhatsAppTemplateMutableWithDefaults() *WhatsAppTemplateMutable`

NewWhatsAppTemplateMutableWithDefaults instantiates a new WhatsAppTemplateMutable object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCategory

`func (o *WhatsAppTemplateMutable) GetCategory() string`

GetCategory returns the Category field if non-nil, zero value otherwise.

### GetCategoryOk

`func (o *WhatsAppTemplateMutable) GetCategoryOk() (*string, bool)`

GetCategoryOk returns a tuple with the Category field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCategory

`func (o *WhatsAppTemplateMutable) SetCategory(v string)`

SetCategory sets Category field to given value.

### HasCategory

`func (o *WhatsAppTemplateMutable) HasCategory() bool`

HasCategory returns a boolean if a field has been set.

### GetComponents

`func (o *WhatsAppTemplateMutable) GetComponents() []WhatsAppTemplateMutableComponents`

GetComponents returns the Components field if non-nil, zero value otherwise.

### GetComponentsOk

`func (o *WhatsAppTemplateMutable) GetComponentsOk() (*[]WhatsAppTemplateMutableComponents, bool)`

GetComponentsOk returns a tuple with the Components field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComponents

`func (o *WhatsAppTemplateMutable) SetComponents(v []WhatsAppTemplateMutableComponents)`

SetComponents sets Components field to given value.

### HasComponents

`func (o *WhatsAppTemplateMutable) HasComponents() bool`

HasComponents returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


