# \WhatsAppTemplatesApi

All URIs are relative to *https://api.unified.chatwerk.de/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**WhatsappTemplateDelete**](WhatsAppTemplatesApi.md#WhatsappTemplateDelete) | **Delete** /gateways/{gatewayId}/templates/whatsapp/{name} | Delete template
[**WhatsappTemplateSend**](WhatsAppTemplatesApi.md#WhatsappTemplateSend) | **Post** /gateways/{gatewayId}/templates/whatsapp/send | Send whatsapp template message
[**WhatsappTemplateSubmit**](WhatsAppTemplatesApi.md#WhatsappTemplateSubmit) | **Post** /gateways/{gatewayId}/templates/whatsapp | Submit new template
[**WhatsappTemplateUpdate**](WhatsAppTemplatesApi.md#WhatsappTemplateUpdate) | **Put** /gateways/{gatewayId}/templates/whatsapp/{template-id} | Update template
[**WhatsappTemplatesGet**](WhatsAppTemplatesApi.md#WhatsappTemplatesGet) | **Get** /gateways/{gatewayId}/templates/whatsapp | Get templates



## WhatsappTemplateDelete

> GenericResponse WhatsappTemplateDelete(ctx, gatewayId, name).Execute()

Delete template



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    name := "name_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WhatsAppTemplatesApi.WhatsappTemplateDelete(context.Background(), gatewayId, name).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WhatsAppTemplatesApi.WhatsappTemplateDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `WhatsappTemplateDelete`: GenericResponse
    fmt.Fprintf(os.Stdout, "Response from `WhatsAppTemplatesApi.WhatsappTemplateDelete`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 
**name** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiWhatsappTemplateDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## WhatsappTemplateSend

> SendMessageResponse WhatsappTemplateSend(ctx, gatewayId).WhatsAppTemplateMessage(whatsAppTemplateMessage).Execute()

Send whatsapp template message



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    whatsAppTemplateMessage := *openapiclient.NewWhatsAppTemplateMessage("To_example", "Namespace_example", "Name_example", "Language_example") // WhatsAppTemplateMessage |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WhatsAppTemplatesApi.WhatsappTemplateSend(context.Background(), gatewayId).WhatsAppTemplateMessage(whatsAppTemplateMessage).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WhatsAppTemplatesApi.WhatsappTemplateSend``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `WhatsappTemplateSend`: SendMessageResponse
    fmt.Fprintf(os.Stdout, "Response from `WhatsAppTemplatesApi.WhatsappTemplateSend`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiWhatsappTemplateSendRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **whatsAppTemplateMessage** | [**WhatsAppTemplateMessage**](WhatsAppTemplateMessage.md) |  | 

### Return type

[**SendMessageResponse**](SendMessageResponse.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## WhatsappTemplateSubmit

> WhatsAppTemplate WhatsappTemplateSubmit(ctx, gatewayId).WhatsAppTemplateCreateRequest(whatsAppTemplateCreateRequest).Execute()

Submit new template



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    whatsAppTemplateCreateRequest := *openapiclient.NewWhatsAppTemplateCreateRequest("Name_example", "Language_example", "Category_example", []openapiclient.WhatsAppTemplateCreateRequestComponents{*openapiclient.NewWhatsAppTemplateCreateRequestComponents("Type_example")}) // WhatsAppTemplateCreateRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WhatsAppTemplatesApi.WhatsappTemplateSubmit(context.Background(), gatewayId).WhatsAppTemplateCreateRequest(whatsAppTemplateCreateRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WhatsAppTemplatesApi.WhatsappTemplateSubmit``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `WhatsappTemplateSubmit`: WhatsAppTemplate
    fmt.Fprintf(os.Stdout, "Response from `WhatsAppTemplatesApi.WhatsappTemplateSubmit`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiWhatsappTemplateSubmitRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **whatsAppTemplateCreateRequest** | [**WhatsAppTemplateCreateRequest**](WhatsAppTemplateCreateRequest.md) |  | 

### Return type

[**WhatsAppTemplate**](WhatsAppTemplate.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## WhatsappTemplateUpdate

> WhatsAppTemplateUpdateResponse WhatsappTemplateUpdate(ctx, gatewayId, templateId).WhatsAppTemplateUpdateRequest(whatsAppTemplateUpdateRequest).Execute()

Update template



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 
    templateId := "templateId_example" // string | 
    whatsAppTemplateUpdateRequest := *openapiclient.NewWhatsAppTemplateUpdateRequest("Category_example", []openapiclient.WhatsAppTemplateCreateRequestComponents{*openapiclient.NewWhatsAppTemplateCreateRequestComponents("Type_example")}) // WhatsAppTemplateUpdateRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WhatsAppTemplatesApi.WhatsappTemplateUpdate(context.Background(), gatewayId, templateId).WhatsAppTemplateUpdateRequest(whatsAppTemplateUpdateRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WhatsAppTemplatesApi.WhatsappTemplateUpdate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `WhatsappTemplateUpdate`: WhatsAppTemplateUpdateResponse
    fmt.Fprintf(os.Stdout, "Response from `WhatsAppTemplatesApi.WhatsappTemplateUpdate`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 
**templateId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiWhatsappTemplateUpdateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **whatsAppTemplateUpdateRequest** | [**WhatsAppTemplateUpdateRequest**](WhatsAppTemplateUpdateRequest.md) |  | 

### Return type

[**WhatsAppTemplateUpdateResponse**](WhatsAppTemplateUpdateResponse.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## WhatsappTemplatesGet

> WhatsAppTemplatesList WhatsappTemplatesGet(ctx, gatewayId).Execute()

Get templates



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    gatewayId := "gatewayId_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.WhatsAppTemplatesApi.WhatsappTemplatesGet(context.Background(), gatewayId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `WhatsAppTemplatesApi.WhatsappTemplatesGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `WhatsappTemplatesGet`: WhatsAppTemplatesList
    fmt.Fprintf(os.Stdout, "Response from `WhatsAppTemplatesApi.WhatsappTemplatesGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**gatewayId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiWhatsappTemplatesGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**WhatsAppTemplatesList**](WhatsAppTemplatesList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader), [ApiKeyQuery](../README.md#ApiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

