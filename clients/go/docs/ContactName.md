# ContactName

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FormattedName** | Pointer to **string** |  | [optional] 
**FirstName** | Pointer to **string** |  | [optional] 
**LastName** | Pointer to **string** |  | [optional] 
**AdditionalName** | Pointer to **string** |  | [optional] 
**Prefix** | Pointer to **string** |  | [optional] 
**Suffix** | Pointer to **string** |  | [optional] 

## Methods

### NewContactName

`func NewContactName() *ContactName`

NewContactName instantiates a new ContactName object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewContactNameWithDefaults

`func NewContactNameWithDefaults() *ContactName`

NewContactNameWithDefaults instantiates a new ContactName object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFormattedName

`func (o *ContactName) GetFormattedName() string`

GetFormattedName returns the FormattedName field if non-nil, zero value otherwise.

### GetFormattedNameOk

`func (o *ContactName) GetFormattedNameOk() (*string, bool)`

GetFormattedNameOk returns a tuple with the FormattedName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFormattedName

`func (o *ContactName) SetFormattedName(v string)`

SetFormattedName sets FormattedName field to given value.

### HasFormattedName

`func (o *ContactName) HasFormattedName() bool`

HasFormattedName returns a boolean if a field has been set.

### GetFirstName

`func (o *ContactName) GetFirstName() string`

GetFirstName returns the FirstName field if non-nil, zero value otherwise.

### GetFirstNameOk

`func (o *ContactName) GetFirstNameOk() (*string, bool)`

GetFirstNameOk returns a tuple with the FirstName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFirstName

`func (o *ContactName) SetFirstName(v string)`

SetFirstName sets FirstName field to given value.

### HasFirstName

`func (o *ContactName) HasFirstName() bool`

HasFirstName returns a boolean if a field has been set.

### GetLastName

`func (o *ContactName) GetLastName() string`

GetLastName returns the LastName field if non-nil, zero value otherwise.

### GetLastNameOk

`func (o *ContactName) GetLastNameOk() (*string, bool)`

GetLastNameOk returns a tuple with the LastName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastName

`func (o *ContactName) SetLastName(v string)`

SetLastName sets LastName field to given value.

### HasLastName

`func (o *ContactName) HasLastName() bool`

HasLastName returns a boolean if a field has been set.

### GetAdditionalName

`func (o *ContactName) GetAdditionalName() string`

GetAdditionalName returns the AdditionalName field if non-nil, zero value otherwise.

### GetAdditionalNameOk

`func (o *ContactName) GetAdditionalNameOk() (*string, bool)`

GetAdditionalNameOk returns a tuple with the AdditionalName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalName

`func (o *ContactName) SetAdditionalName(v string)`

SetAdditionalName sets AdditionalName field to given value.

### HasAdditionalName

`func (o *ContactName) HasAdditionalName() bool`

HasAdditionalName returns a boolean if a field has been set.

### GetPrefix

`func (o *ContactName) GetPrefix() string`

GetPrefix returns the Prefix field if non-nil, zero value otherwise.

### GetPrefixOk

`func (o *ContactName) GetPrefixOk() (*string, bool)`

GetPrefixOk returns a tuple with the Prefix field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrefix

`func (o *ContactName) SetPrefix(v string)`

SetPrefix sets Prefix field to given value.

### HasPrefix

`func (o *ContactName) HasPrefix() bool`

HasPrefix returns a boolean if a field has been set.

### GetSuffix

`func (o *ContactName) GetSuffix() string`

GetSuffix returns the Suffix field if non-nil, zero value otherwise.

### GetSuffixOk

`func (o *ContactName) GetSuffixOk() (*string, bool)`

GetSuffixOk returns a tuple with the Suffix field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSuffix

`func (o *ContactName) SetSuffix(v string)`

SetSuffix sets Suffix field to given value.

### HasSuffix

`func (o *ContactName) HasSuffix() bool`

HasSuffix returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


