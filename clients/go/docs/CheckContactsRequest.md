# CheckContactsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Contacts** | **[]string** |  | 
**ForceCheck** | Pointer to **bool** |  | [optional] 
**Blocking** | Pointer to **bool** |  | [optional] 

## Methods

### NewCheckContactsRequest

`func NewCheckContactsRequest(contacts []string, ) *CheckContactsRequest`

NewCheckContactsRequest instantiates a new CheckContactsRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCheckContactsRequestWithDefaults

`func NewCheckContactsRequestWithDefaults() *CheckContactsRequest`

NewCheckContactsRequestWithDefaults instantiates a new CheckContactsRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContacts

`func (o *CheckContactsRequest) GetContacts() []string`

GetContacts returns the Contacts field if non-nil, zero value otherwise.

### GetContactsOk

`func (o *CheckContactsRequest) GetContactsOk() (*[]string, bool)`

GetContactsOk returns a tuple with the Contacts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContacts

`func (o *CheckContactsRequest) SetContacts(v []string)`

SetContacts sets Contacts field to given value.


### GetForceCheck

`func (o *CheckContactsRequest) GetForceCheck() bool`

GetForceCheck returns the ForceCheck field if non-nil, zero value otherwise.

### GetForceCheckOk

`func (o *CheckContactsRequest) GetForceCheckOk() (*bool, bool)`

GetForceCheckOk returns a tuple with the ForceCheck field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetForceCheck

`func (o *CheckContactsRequest) SetForceCheck(v bool)`

SetForceCheck sets ForceCheck field to given value.

### HasForceCheck

`func (o *CheckContactsRequest) HasForceCheck() bool`

HasForceCheck returns a boolean if a field has been set.

### GetBlocking

`func (o *CheckContactsRequest) GetBlocking() bool`

GetBlocking returns the Blocking field if non-nil, zero value otherwise.

### GetBlockingOk

`func (o *CheckContactsRequest) GetBlockingOk() (*bool, bool)`

GetBlockingOk returns a tuple with the Blocking field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlocking

`func (o *CheckContactsRequest) SetBlocking(v bool)`

SetBlocking sets Blocking field to given value.

### HasBlocking

`func (o *CheckContactsRequest) HasBlocking() bool`

HasBlocking returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


