# HeaderType

## Enum


* `HEADER_TEXT` (value: `"text"`)

* `HEADER_IMAGE` (value: `"image"`)

* `HEADER_VIDEO` (value: `"video"`)

* `HEADER_DOCUMENT` (value: `"document"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


