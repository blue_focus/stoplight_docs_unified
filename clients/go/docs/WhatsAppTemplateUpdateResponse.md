# WhatsAppTemplateUpdateResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Success** | Pointer to **bool** |  | [optional] 
**Error** | Pointer to [**ErrorDetails**](ErrorDetails.md) |  | [optional] 

## Methods

### NewWhatsAppTemplateUpdateResponse

`func NewWhatsAppTemplateUpdateResponse() *WhatsAppTemplateUpdateResponse`

NewWhatsAppTemplateUpdateResponse instantiates a new WhatsAppTemplateUpdateResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWhatsAppTemplateUpdateResponseWithDefaults

`func NewWhatsAppTemplateUpdateResponseWithDefaults() *WhatsAppTemplateUpdateResponse`

NewWhatsAppTemplateUpdateResponseWithDefaults instantiates a new WhatsAppTemplateUpdateResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSuccess

`func (o *WhatsAppTemplateUpdateResponse) GetSuccess() bool`

GetSuccess returns the Success field if non-nil, zero value otherwise.

### GetSuccessOk

`func (o *WhatsAppTemplateUpdateResponse) GetSuccessOk() (*bool, bool)`

GetSuccessOk returns a tuple with the Success field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSuccess

`func (o *WhatsAppTemplateUpdateResponse) SetSuccess(v bool)`

SetSuccess sets Success field to given value.

### HasSuccess

`func (o *WhatsAppTemplateUpdateResponse) HasSuccess() bool`

HasSuccess returns a boolean if a field has been set.

### GetError

`func (o *WhatsAppTemplateUpdateResponse) GetError() ErrorDetails`

GetError returns the Error field if non-nil, zero value otherwise.

### GetErrorOk

`func (o *WhatsAppTemplateUpdateResponse) GetErrorOk() (*ErrorDetails, bool)`

GetErrorOk returns a tuple with the Error field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetError

`func (o *WhatsAppTemplateUpdateResponse) SetError(v ErrorDetails)`

SetError sets Error field to given value.

### HasError

`func (o *WhatsAppTemplateUpdateResponse) HasError() bool`

HasError returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


