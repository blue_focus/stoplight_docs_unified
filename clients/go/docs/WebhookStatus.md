# WebhookStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MessageId** | **string** |  | 
**Status** | [**MessageStatus**](MessageStatus.md) |  | 
**Errors** | Pointer to [**[]WebhookError**](WebhookError.md) |  | [optional] 
**Timestamp** | **int32** |  | 
**Conversation** | Pointer to [**WebhookConversation**](WebhookConversation.md) |  | [optional] 
**Pricing** | Pointer to [**WebhookPricing**](WebhookPricing.md) |  | [optional] 

## Methods

### NewWebhookStatus

`func NewWebhookStatus(messageId string, status MessageStatus, timestamp int32, ) *WebhookStatus`

NewWebhookStatus instantiates a new WebhookStatus object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWebhookStatusWithDefaults

`func NewWebhookStatusWithDefaults() *WebhookStatus`

NewWebhookStatusWithDefaults instantiates a new WebhookStatus object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessageId

`func (o *WebhookStatus) GetMessageId() string`

GetMessageId returns the MessageId field if non-nil, zero value otherwise.

### GetMessageIdOk

`func (o *WebhookStatus) GetMessageIdOk() (*string, bool)`

GetMessageIdOk returns a tuple with the MessageId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessageId

`func (o *WebhookStatus) SetMessageId(v string)`

SetMessageId sets MessageId field to given value.


### GetStatus

`func (o *WebhookStatus) GetStatus() MessageStatus`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *WebhookStatus) GetStatusOk() (*MessageStatus, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *WebhookStatus) SetStatus(v MessageStatus)`

SetStatus sets Status field to given value.


### GetErrors

`func (o *WebhookStatus) GetErrors() []WebhookError`

GetErrors returns the Errors field if non-nil, zero value otherwise.

### GetErrorsOk

`func (o *WebhookStatus) GetErrorsOk() (*[]WebhookError, bool)`

GetErrorsOk returns a tuple with the Errors field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetErrors

`func (o *WebhookStatus) SetErrors(v []WebhookError)`

SetErrors sets Errors field to given value.

### HasErrors

`func (o *WebhookStatus) HasErrors() bool`

HasErrors returns a boolean if a field has been set.

### GetTimestamp

`func (o *WebhookStatus) GetTimestamp() int32`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *WebhookStatus) GetTimestampOk() (*int32, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *WebhookStatus) SetTimestamp(v int32)`

SetTimestamp sets Timestamp field to given value.


### GetConversation

`func (o *WebhookStatus) GetConversation() WebhookConversation`

GetConversation returns the Conversation field if non-nil, zero value otherwise.

### GetConversationOk

`func (o *WebhookStatus) GetConversationOk() (*WebhookConversation, bool)`

GetConversationOk returns a tuple with the Conversation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConversation

`func (o *WebhookStatus) SetConversation(v WebhookConversation)`

SetConversation sets Conversation field to given value.

### HasConversation

`func (o *WebhookStatus) HasConversation() bool`

HasConversation returns a boolean if a field has been set.

### GetPricing

`func (o *WebhookStatus) GetPricing() WebhookPricing`

GetPricing returns the Pricing field if non-nil, zero value otherwise.

### GetPricingOk

`func (o *WebhookStatus) GetPricingOk() (*WebhookPricing, bool)`

GetPricingOk returns a tuple with the Pricing field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPricing

`func (o *WebhookStatus) SetPricing(v WebhookPricing)`

SetPricing sets Pricing field to given value.

### HasPricing

`func (o *WebhookStatus) HasPricing() bool`

HasPricing returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


