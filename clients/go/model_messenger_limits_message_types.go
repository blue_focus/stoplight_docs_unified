/*
 * Unified API
 *
 * Unified API allows sending and receiving messages through different messaging platforms using single, unified API.
 *
 * API version: 1.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package unifiedapi

import (
	"encoding/json"
)

// MessengerLimitsMessageTypes struct for MessengerLimitsMessageTypes
type MessengerLimitsMessageTypes struct {
	Type MessageType `json:"type"`
	Size *int32 `json:"size,omitempty"`
	MimeTypes *[]string `json:"mime_types,omitempty"`
	ItemSize *int32 `json:"item_size,omitempty"`
	Header *HeaderLimits `json:"header,omitempty"`
	Footer *FooterLimits `json:"footer,omitempty"`
}

// NewMessengerLimitsMessageTypes instantiates a new MessengerLimitsMessageTypes object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMessengerLimitsMessageTypes(type_ MessageType) *MessengerLimitsMessageTypes {
	this := MessengerLimitsMessageTypes{}
	this.Type = type_
	return &this
}

// NewMessengerLimitsMessageTypesWithDefaults instantiates a new MessengerLimitsMessageTypes object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMessengerLimitsMessageTypesWithDefaults() *MessengerLimitsMessageTypes {
	this := MessengerLimitsMessageTypes{}
	return &this
}

// GetType returns the Type field value
func (o *MessengerLimitsMessageTypes) GetType() MessageType {
	if o == nil {
		var ret MessageType
		return ret
	}

	return o.Type
}

// GetTypeOk returns a tuple with the Type field value
// and a boolean to check if the value has been set.
func (o *MessengerLimitsMessageTypes) GetTypeOk() (*MessageType, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Type, true
}

// SetType sets field value
func (o *MessengerLimitsMessageTypes) SetType(v MessageType) {
	o.Type = v
}

// GetSize returns the Size field value if set, zero value otherwise.
func (o *MessengerLimitsMessageTypes) GetSize() int32 {
	if o == nil || o.Size == nil {
		var ret int32
		return ret
	}
	return *o.Size
}

// GetSizeOk returns a tuple with the Size field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessengerLimitsMessageTypes) GetSizeOk() (*int32, bool) {
	if o == nil || o.Size == nil {
		return nil, false
	}
	return o.Size, true
}

// HasSize returns a boolean if a field has been set.
func (o *MessengerLimitsMessageTypes) HasSize() bool {
	if o != nil && o.Size != nil {
		return true
	}

	return false
}

// SetSize gets a reference to the given int32 and assigns it to the Size field.
func (o *MessengerLimitsMessageTypes) SetSize(v int32) {
	o.Size = &v
}

// GetMimeTypes returns the MimeTypes field value if set, zero value otherwise.
func (o *MessengerLimitsMessageTypes) GetMimeTypes() []string {
	if o == nil || o.MimeTypes == nil {
		var ret []string
		return ret
	}
	return *o.MimeTypes
}

// GetMimeTypesOk returns a tuple with the MimeTypes field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessengerLimitsMessageTypes) GetMimeTypesOk() (*[]string, bool) {
	if o == nil || o.MimeTypes == nil {
		return nil, false
	}
	return o.MimeTypes, true
}

// HasMimeTypes returns a boolean if a field has been set.
func (o *MessengerLimitsMessageTypes) HasMimeTypes() bool {
	if o != nil && o.MimeTypes != nil {
		return true
	}

	return false
}

// SetMimeTypes gets a reference to the given []string and assigns it to the MimeTypes field.
func (o *MessengerLimitsMessageTypes) SetMimeTypes(v []string) {
	o.MimeTypes = &v
}

// GetItemSize returns the ItemSize field value if set, zero value otherwise.
func (o *MessengerLimitsMessageTypes) GetItemSize() int32 {
	if o == nil || o.ItemSize == nil {
		var ret int32
		return ret
	}
	return *o.ItemSize
}

// GetItemSizeOk returns a tuple with the ItemSize field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessengerLimitsMessageTypes) GetItemSizeOk() (*int32, bool) {
	if o == nil || o.ItemSize == nil {
		return nil, false
	}
	return o.ItemSize, true
}

// HasItemSize returns a boolean if a field has been set.
func (o *MessengerLimitsMessageTypes) HasItemSize() bool {
	if o != nil && o.ItemSize != nil {
		return true
	}

	return false
}

// SetItemSize gets a reference to the given int32 and assigns it to the ItemSize field.
func (o *MessengerLimitsMessageTypes) SetItemSize(v int32) {
	o.ItemSize = &v
}

// GetHeader returns the Header field value if set, zero value otherwise.
func (o *MessengerLimitsMessageTypes) GetHeader() HeaderLimits {
	if o == nil || o.Header == nil {
		var ret HeaderLimits
		return ret
	}
	return *o.Header
}

// GetHeaderOk returns a tuple with the Header field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessengerLimitsMessageTypes) GetHeaderOk() (*HeaderLimits, bool) {
	if o == nil || o.Header == nil {
		return nil, false
	}
	return o.Header, true
}

// HasHeader returns a boolean if a field has been set.
func (o *MessengerLimitsMessageTypes) HasHeader() bool {
	if o != nil && o.Header != nil {
		return true
	}

	return false
}

// SetHeader gets a reference to the given HeaderLimits and assigns it to the Header field.
func (o *MessengerLimitsMessageTypes) SetHeader(v HeaderLimits) {
	o.Header = &v
}

// GetFooter returns the Footer field value if set, zero value otherwise.
func (o *MessengerLimitsMessageTypes) GetFooter() FooterLimits {
	if o == nil || o.Footer == nil {
		var ret FooterLimits
		return ret
	}
	return *o.Footer
}

// GetFooterOk returns a tuple with the Footer field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessengerLimitsMessageTypes) GetFooterOk() (*FooterLimits, bool) {
	if o == nil || o.Footer == nil {
		return nil, false
	}
	return o.Footer, true
}

// HasFooter returns a boolean if a field has been set.
func (o *MessengerLimitsMessageTypes) HasFooter() bool {
	if o != nil && o.Footer != nil {
		return true
	}

	return false
}

// SetFooter gets a reference to the given FooterLimits and assigns it to the Footer field.
func (o *MessengerLimitsMessageTypes) SetFooter(v FooterLimits) {
	o.Footer = &v
}

func (o MessengerLimitsMessageTypes) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["type"] = o.Type
	}
	if o.Size != nil {
		toSerialize["size"] = o.Size
	}
	if o.MimeTypes != nil {
		toSerialize["mime_types"] = o.MimeTypes
	}
	if o.ItemSize != nil {
		toSerialize["item_size"] = o.ItemSize
	}
	if o.Header != nil {
		toSerialize["header"] = o.Header
	}
	if o.Footer != nil {
		toSerialize["footer"] = o.Footer
	}
	return json.Marshal(toSerialize)
}

type NullableMessengerLimitsMessageTypes struct {
	value *MessengerLimitsMessageTypes
	isSet bool
}

func (v NullableMessengerLimitsMessageTypes) Get() *MessengerLimitsMessageTypes {
	return v.value
}

func (v *NullableMessengerLimitsMessageTypes) Set(val *MessengerLimitsMessageTypes) {
	v.value = val
	v.isSet = true
}

func (v NullableMessengerLimitsMessageTypes) IsSet() bool {
	return v.isSet
}

func (v *NullableMessengerLimitsMessageTypes) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMessengerLimitsMessageTypes(val *MessengerLimitsMessageTypes) *NullableMessengerLimitsMessageTypes {
	return &NullableMessengerLimitsMessageTypes{value: val, isSet: true}
}

func (v NullableMessengerLimitsMessageTypes) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMessengerLimitsMessageTypes) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


