/*
 * Unified API
 *
 * Unified API allows sending and receiving messages through different messaging platforms using single, unified API.
 *
 * API version: 1.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package unifiedapi

import (
	"encoding/json"
)

// MessageItem struct for MessageItem
type MessageItem struct {
	Text *string `json:"text,omitempty"`
	Payload *string `json:"payload,omitempty"`
	Description *string `json:"description,omitempty"`
}

// NewMessageItem instantiates a new MessageItem object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewMessageItem() *MessageItem {
	this := MessageItem{}
	return &this
}

// NewMessageItemWithDefaults instantiates a new MessageItem object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewMessageItemWithDefaults() *MessageItem {
	this := MessageItem{}
	return &this
}

// GetText returns the Text field value if set, zero value otherwise.
func (o *MessageItem) GetText() string {
	if o == nil || o.Text == nil {
		var ret string
		return ret
	}
	return *o.Text
}

// GetTextOk returns a tuple with the Text field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageItem) GetTextOk() (*string, bool) {
	if o == nil || o.Text == nil {
		return nil, false
	}
	return o.Text, true
}

// HasText returns a boolean if a field has been set.
func (o *MessageItem) HasText() bool {
	if o != nil && o.Text != nil {
		return true
	}

	return false
}

// SetText gets a reference to the given string and assigns it to the Text field.
func (o *MessageItem) SetText(v string) {
	o.Text = &v
}

// GetPayload returns the Payload field value if set, zero value otherwise.
func (o *MessageItem) GetPayload() string {
	if o == nil || o.Payload == nil {
		var ret string
		return ret
	}
	return *o.Payload
}

// GetPayloadOk returns a tuple with the Payload field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageItem) GetPayloadOk() (*string, bool) {
	if o == nil || o.Payload == nil {
		return nil, false
	}
	return o.Payload, true
}

// HasPayload returns a boolean if a field has been set.
func (o *MessageItem) HasPayload() bool {
	if o != nil && o.Payload != nil {
		return true
	}

	return false
}

// SetPayload gets a reference to the given string and assigns it to the Payload field.
func (o *MessageItem) SetPayload(v string) {
	o.Payload = &v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *MessageItem) GetDescription() string {
	if o == nil || o.Description == nil {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *MessageItem) GetDescriptionOk() (*string, bool) {
	if o == nil || o.Description == nil {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *MessageItem) HasDescription() bool {
	if o != nil && o.Description != nil {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *MessageItem) SetDescription(v string) {
	o.Description = &v
}

func (o MessageItem) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Text != nil {
		toSerialize["text"] = o.Text
	}
	if o.Payload != nil {
		toSerialize["payload"] = o.Payload
	}
	if o.Description != nil {
		toSerialize["description"] = o.Description
	}
	return json.Marshal(toSerialize)
}

type NullableMessageItem struct {
	value *MessageItem
	isSet bool
}

func (v NullableMessageItem) Get() *MessageItem {
	return v.value
}

func (v *NullableMessageItem) Set(val *MessageItem) {
	v.value = val
	v.isSet = true
}

func (v NullableMessageItem) IsSet() bool {
	return v.isSet
}

func (v *NullableMessageItem) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableMessageItem(val *MessageItem) *NullableMessageItem {
	return &NullableMessageItem{value: val, isSet: true}
}

func (v NullableMessageItem) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableMessageItem) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


