Code generation example
=======================

Execute from  `stoplight_docs_unified` directory.

```
docker run --rm -v "${PWD}:/local" openapitools/openapi-generator-cli:v5.1.0 generate \
    -i /local/reference/api.v1.yaml \
    -g go \
    --package-name=unifiedapi \
    --git-host=bitbucket.org \
    --git-user-id=blue_focus \
    --git-repo-id=stoplight_docs_unified/clients/go \
    -o /local/clients/go
```