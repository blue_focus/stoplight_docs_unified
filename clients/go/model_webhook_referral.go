/*
 * Unified API
 *
 * Unified API allows sending and receiving messages through different messaging platforms using single, unified API.
 *
 * API version: 1.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package unifiedapi

import (
	"encoding/json"
)

// WebhookReferral struct for WebhookReferral
type WebhookReferral struct {
	Headline *string `json:"headline,omitempty"`
	Body *string `json:"body,omitempty"`
	SourceType *string `json:"source_type,omitempty"`
	SourceId *string `json:"source_id,omitempty"`
	SourceUrl *string `json:"source_url,omitempty"`
	Image *WebhookReferralMedia `json:"image,omitempty"`
	Video *WebhookReferralMedia `json:"video,omitempty"`
}

// NewWebhookReferral instantiates a new WebhookReferral object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewWebhookReferral() *WebhookReferral {
	this := WebhookReferral{}
	return &this
}

// NewWebhookReferralWithDefaults instantiates a new WebhookReferral object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewWebhookReferralWithDefaults() *WebhookReferral {
	this := WebhookReferral{}
	return &this
}

// GetHeadline returns the Headline field value if set, zero value otherwise.
func (o *WebhookReferral) GetHeadline() string {
	if o == nil || o.Headline == nil {
		var ret string
		return ret
	}
	return *o.Headline
}

// GetHeadlineOk returns a tuple with the Headline field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WebhookReferral) GetHeadlineOk() (*string, bool) {
	if o == nil || o.Headline == nil {
		return nil, false
	}
	return o.Headline, true
}

// HasHeadline returns a boolean if a field has been set.
func (o *WebhookReferral) HasHeadline() bool {
	if o != nil && o.Headline != nil {
		return true
	}

	return false
}

// SetHeadline gets a reference to the given string and assigns it to the Headline field.
func (o *WebhookReferral) SetHeadline(v string) {
	o.Headline = &v
}

// GetBody returns the Body field value if set, zero value otherwise.
func (o *WebhookReferral) GetBody() string {
	if o == nil || o.Body == nil {
		var ret string
		return ret
	}
	return *o.Body
}

// GetBodyOk returns a tuple with the Body field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WebhookReferral) GetBodyOk() (*string, bool) {
	if o == nil || o.Body == nil {
		return nil, false
	}
	return o.Body, true
}

// HasBody returns a boolean if a field has been set.
func (o *WebhookReferral) HasBody() bool {
	if o != nil && o.Body != nil {
		return true
	}

	return false
}

// SetBody gets a reference to the given string and assigns it to the Body field.
func (o *WebhookReferral) SetBody(v string) {
	o.Body = &v
}

// GetSourceType returns the SourceType field value if set, zero value otherwise.
func (o *WebhookReferral) GetSourceType() string {
	if o == nil || o.SourceType == nil {
		var ret string
		return ret
	}
	return *o.SourceType
}

// GetSourceTypeOk returns a tuple with the SourceType field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WebhookReferral) GetSourceTypeOk() (*string, bool) {
	if o == nil || o.SourceType == nil {
		return nil, false
	}
	return o.SourceType, true
}

// HasSourceType returns a boolean if a field has been set.
func (o *WebhookReferral) HasSourceType() bool {
	if o != nil && o.SourceType != nil {
		return true
	}

	return false
}

// SetSourceType gets a reference to the given string and assigns it to the SourceType field.
func (o *WebhookReferral) SetSourceType(v string) {
	o.SourceType = &v
}

// GetSourceId returns the SourceId field value if set, zero value otherwise.
func (o *WebhookReferral) GetSourceId() string {
	if o == nil || o.SourceId == nil {
		var ret string
		return ret
	}
	return *o.SourceId
}

// GetSourceIdOk returns a tuple with the SourceId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WebhookReferral) GetSourceIdOk() (*string, bool) {
	if o == nil || o.SourceId == nil {
		return nil, false
	}
	return o.SourceId, true
}

// HasSourceId returns a boolean if a field has been set.
func (o *WebhookReferral) HasSourceId() bool {
	if o != nil && o.SourceId != nil {
		return true
	}

	return false
}

// SetSourceId gets a reference to the given string and assigns it to the SourceId field.
func (o *WebhookReferral) SetSourceId(v string) {
	o.SourceId = &v
}

// GetSourceUrl returns the SourceUrl field value if set, zero value otherwise.
func (o *WebhookReferral) GetSourceUrl() string {
	if o == nil || o.SourceUrl == nil {
		var ret string
		return ret
	}
	return *o.SourceUrl
}

// GetSourceUrlOk returns a tuple with the SourceUrl field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WebhookReferral) GetSourceUrlOk() (*string, bool) {
	if o == nil || o.SourceUrl == nil {
		return nil, false
	}
	return o.SourceUrl, true
}

// HasSourceUrl returns a boolean if a field has been set.
func (o *WebhookReferral) HasSourceUrl() bool {
	if o != nil && o.SourceUrl != nil {
		return true
	}

	return false
}

// SetSourceUrl gets a reference to the given string and assigns it to the SourceUrl field.
func (o *WebhookReferral) SetSourceUrl(v string) {
	o.SourceUrl = &v
}

// GetImage returns the Image field value if set, zero value otherwise.
func (o *WebhookReferral) GetImage() WebhookReferralMedia {
	if o == nil || o.Image == nil {
		var ret WebhookReferralMedia
		return ret
	}
	return *o.Image
}

// GetImageOk returns a tuple with the Image field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WebhookReferral) GetImageOk() (*WebhookReferralMedia, bool) {
	if o == nil || o.Image == nil {
		return nil, false
	}
	return o.Image, true
}

// HasImage returns a boolean if a field has been set.
func (o *WebhookReferral) HasImage() bool {
	if o != nil && o.Image != nil {
		return true
	}

	return false
}

// SetImage gets a reference to the given WebhookReferralMedia and assigns it to the Image field.
func (o *WebhookReferral) SetImage(v WebhookReferralMedia) {
	o.Image = &v
}

// GetVideo returns the Video field value if set, zero value otherwise.
func (o *WebhookReferral) GetVideo() WebhookReferralMedia {
	if o == nil || o.Video == nil {
		var ret WebhookReferralMedia
		return ret
	}
	return *o.Video
}

// GetVideoOk returns a tuple with the Video field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WebhookReferral) GetVideoOk() (*WebhookReferralMedia, bool) {
	if o == nil || o.Video == nil {
		return nil, false
	}
	return o.Video, true
}

// HasVideo returns a boolean if a field has been set.
func (o *WebhookReferral) HasVideo() bool {
	if o != nil && o.Video != nil {
		return true
	}

	return false
}

// SetVideo gets a reference to the given WebhookReferralMedia and assigns it to the Video field.
func (o *WebhookReferral) SetVideo(v WebhookReferralMedia) {
	o.Video = &v
}

func (o WebhookReferral) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Headline != nil {
		toSerialize["headline"] = o.Headline
	}
	if o.Body != nil {
		toSerialize["body"] = o.Body
	}
	if o.SourceType != nil {
		toSerialize["source_type"] = o.SourceType
	}
	if o.SourceId != nil {
		toSerialize["source_id"] = o.SourceId
	}
	if o.SourceUrl != nil {
		toSerialize["source_url"] = o.SourceUrl
	}
	if o.Image != nil {
		toSerialize["image"] = o.Image
	}
	if o.Video != nil {
		toSerialize["video"] = o.Video
	}
	return json.Marshal(toSerialize)
}

type NullableWebhookReferral struct {
	value *WebhookReferral
	isSet bool
}

func (v NullableWebhookReferral) Get() *WebhookReferral {
	return v.value
}

func (v *NullableWebhookReferral) Set(val *WebhookReferral) {
	v.value = val
	v.isSet = true
}

func (v NullableWebhookReferral) IsSet() bool {
	return v.isSet
}

func (v *NullableWebhookReferral) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableWebhookReferral(val *WebhookReferral) *NullableWebhookReferral {
	return &NullableWebhookReferral{value: val, isSet: true}
}

func (v NullableWebhookReferral) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableWebhookReferral) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


