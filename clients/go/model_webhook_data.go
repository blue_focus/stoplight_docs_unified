/*
 * Unified API
 *
 * Unified API allows sending and receiving messages through different messaging platforms using single, unified API.
 *
 * API version: 1.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package unifiedapi

import (
	"encoding/json"
)

// WebhookData struct for WebhookData
type WebhookData struct {
	Gateway WebhookDataGateway `json:"gateway"`
	Messages *[]WebhookMessage `json:"messages,omitempty"`
	Statuses *[]WebhookStatus `json:"statuses,omitempty"`
	System *[]WebhookSystemMessage `json:"system,omitempty"`
}

// NewWebhookData instantiates a new WebhookData object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewWebhookData(gateway WebhookDataGateway) *WebhookData {
	this := WebhookData{}
	this.Gateway = gateway
	return &this
}

// NewWebhookDataWithDefaults instantiates a new WebhookData object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewWebhookDataWithDefaults() *WebhookData {
	this := WebhookData{}
	return &this
}

// GetGateway returns the Gateway field value
func (o *WebhookData) GetGateway() WebhookDataGateway {
	if o == nil {
		var ret WebhookDataGateway
		return ret
	}

	return o.Gateway
}

// GetGatewayOk returns a tuple with the Gateway field value
// and a boolean to check if the value has been set.
func (o *WebhookData) GetGatewayOk() (*WebhookDataGateway, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Gateway, true
}

// SetGateway sets field value
func (o *WebhookData) SetGateway(v WebhookDataGateway) {
	o.Gateway = v
}

// GetMessages returns the Messages field value if set, zero value otherwise.
func (o *WebhookData) GetMessages() []WebhookMessage {
	if o == nil || o.Messages == nil {
		var ret []WebhookMessage
		return ret
	}
	return *o.Messages
}

// GetMessagesOk returns a tuple with the Messages field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WebhookData) GetMessagesOk() (*[]WebhookMessage, bool) {
	if o == nil || o.Messages == nil {
		return nil, false
	}
	return o.Messages, true
}

// HasMessages returns a boolean if a field has been set.
func (o *WebhookData) HasMessages() bool {
	if o != nil && o.Messages != nil {
		return true
	}

	return false
}

// SetMessages gets a reference to the given []WebhookMessage and assigns it to the Messages field.
func (o *WebhookData) SetMessages(v []WebhookMessage) {
	o.Messages = &v
}

// GetStatuses returns the Statuses field value if set, zero value otherwise.
func (o *WebhookData) GetStatuses() []WebhookStatus {
	if o == nil || o.Statuses == nil {
		var ret []WebhookStatus
		return ret
	}
	return *o.Statuses
}

// GetStatusesOk returns a tuple with the Statuses field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WebhookData) GetStatusesOk() (*[]WebhookStatus, bool) {
	if o == nil || o.Statuses == nil {
		return nil, false
	}
	return o.Statuses, true
}

// HasStatuses returns a boolean if a field has been set.
func (o *WebhookData) HasStatuses() bool {
	if o != nil && o.Statuses != nil {
		return true
	}

	return false
}

// SetStatuses gets a reference to the given []WebhookStatus and assigns it to the Statuses field.
func (o *WebhookData) SetStatuses(v []WebhookStatus) {
	o.Statuses = &v
}

// GetSystem returns the System field value if set, zero value otherwise.
func (o *WebhookData) GetSystem() []WebhookSystemMessage {
	if o == nil || o.System == nil {
		var ret []WebhookSystemMessage
		return ret
	}
	return *o.System
}

// GetSystemOk returns a tuple with the System field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WebhookData) GetSystemOk() (*[]WebhookSystemMessage, bool) {
	if o == nil || o.System == nil {
		return nil, false
	}
	return o.System, true
}

// HasSystem returns a boolean if a field has been set.
func (o *WebhookData) HasSystem() bool {
	if o != nil && o.System != nil {
		return true
	}

	return false
}

// SetSystem gets a reference to the given []WebhookSystemMessage and assigns it to the System field.
func (o *WebhookData) SetSystem(v []WebhookSystemMessage) {
	o.System = &v
}

func (o WebhookData) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["gateway"] = o.Gateway
	}
	if o.Messages != nil {
		toSerialize["messages"] = o.Messages
	}
	if o.Statuses != nil {
		toSerialize["statuses"] = o.Statuses
	}
	if o.System != nil {
		toSerialize["system"] = o.System
	}
	return json.Marshal(toSerialize)
}

type NullableWebhookData struct {
	value *WebhookData
	isSet bool
}

func (v NullableWebhookData) Get() *WebhookData {
	return v.value
}

func (v *NullableWebhookData) Set(val *WebhookData) {
	v.value = val
	v.isSet = true
}

func (v NullableWebhookData) IsSet() bool {
	return v.isSet
}

func (v *NullableWebhookData) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableWebhookData(val *WebhookData) *NullableWebhookData {
	return &NullableWebhookData{value: val, isSet: true}
}

func (v NullableWebhookData) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableWebhookData) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


