/*
 * Unified API
 *
 * Unified API allows sending and receiving messages through different messaging platforms using single, unified API.
 *
 * API version: 1.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package unifiedapi

import (
	"encoding/json"
)

// WhatsAppTemplateMessageComponents struct for WhatsAppTemplateMessageComponents
type WhatsAppTemplateMessageComponents struct {
	Type string `json:"type"`
	// For buttons only
	SubType *string `json:"sub_type,omitempty"`
	// For buttons only
	Index *string `json:"index,omitempty"`
	Parameters *[]WhatsAppTemplateMessageParameters `json:"parameters,omitempty"`
}

// NewWhatsAppTemplateMessageComponents instantiates a new WhatsAppTemplateMessageComponents object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewWhatsAppTemplateMessageComponents(type_ string) *WhatsAppTemplateMessageComponents {
	this := WhatsAppTemplateMessageComponents{}
	this.Type = type_
	return &this
}

// NewWhatsAppTemplateMessageComponentsWithDefaults instantiates a new WhatsAppTemplateMessageComponents object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewWhatsAppTemplateMessageComponentsWithDefaults() *WhatsAppTemplateMessageComponents {
	this := WhatsAppTemplateMessageComponents{}
	return &this
}

// GetType returns the Type field value
func (o *WhatsAppTemplateMessageComponents) GetType() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Type
}

// GetTypeOk returns a tuple with the Type field value
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateMessageComponents) GetTypeOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Type, true
}

// SetType sets field value
func (o *WhatsAppTemplateMessageComponents) SetType(v string) {
	o.Type = v
}

// GetSubType returns the SubType field value if set, zero value otherwise.
func (o *WhatsAppTemplateMessageComponents) GetSubType() string {
	if o == nil || o.SubType == nil {
		var ret string
		return ret
	}
	return *o.SubType
}

// GetSubTypeOk returns a tuple with the SubType field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateMessageComponents) GetSubTypeOk() (*string, bool) {
	if o == nil || o.SubType == nil {
		return nil, false
	}
	return o.SubType, true
}

// HasSubType returns a boolean if a field has been set.
func (o *WhatsAppTemplateMessageComponents) HasSubType() bool {
	if o != nil && o.SubType != nil {
		return true
	}

	return false
}

// SetSubType gets a reference to the given string and assigns it to the SubType field.
func (o *WhatsAppTemplateMessageComponents) SetSubType(v string) {
	o.SubType = &v
}

// GetIndex returns the Index field value if set, zero value otherwise.
func (o *WhatsAppTemplateMessageComponents) GetIndex() string {
	if o == nil || o.Index == nil {
		var ret string
		return ret
	}
	return *o.Index
}

// GetIndexOk returns a tuple with the Index field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateMessageComponents) GetIndexOk() (*string, bool) {
	if o == nil || o.Index == nil {
		return nil, false
	}
	return o.Index, true
}

// HasIndex returns a boolean if a field has been set.
func (o *WhatsAppTemplateMessageComponents) HasIndex() bool {
	if o != nil && o.Index != nil {
		return true
	}

	return false
}

// SetIndex gets a reference to the given string and assigns it to the Index field.
func (o *WhatsAppTemplateMessageComponents) SetIndex(v string) {
	o.Index = &v
}

// GetParameters returns the Parameters field value if set, zero value otherwise.
func (o *WhatsAppTemplateMessageComponents) GetParameters() []WhatsAppTemplateMessageParameters {
	if o == nil || o.Parameters == nil {
		var ret []WhatsAppTemplateMessageParameters
		return ret
	}
	return *o.Parameters
}

// GetParametersOk returns a tuple with the Parameters field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *WhatsAppTemplateMessageComponents) GetParametersOk() (*[]WhatsAppTemplateMessageParameters, bool) {
	if o == nil || o.Parameters == nil {
		return nil, false
	}
	return o.Parameters, true
}

// HasParameters returns a boolean if a field has been set.
func (o *WhatsAppTemplateMessageComponents) HasParameters() bool {
	if o != nil && o.Parameters != nil {
		return true
	}

	return false
}

// SetParameters gets a reference to the given []WhatsAppTemplateMessageParameters and assigns it to the Parameters field.
func (o *WhatsAppTemplateMessageComponents) SetParameters(v []WhatsAppTemplateMessageParameters) {
	o.Parameters = &v
}

func (o WhatsAppTemplateMessageComponents) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["type"] = o.Type
	}
	if o.SubType != nil {
		toSerialize["sub_type"] = o.SubType
	}
	if o.Index != nil {
		toSerialize["index"] = o.Index
	}
	if o.Parameters != nil {
		toSerialize["parameters"] = o.Parameters
	}
	return json.Marshal(toSerialize)
}

type NullableWhatsAppTemplateMessageComponents struct {
	value *WhatsAppTemplateMessageComponents
	isSet bool
}

func (v NullableWhatsAppTemplateMessageComponents) Get() *WhatsAppTemplateMessageComponents {
	return v.value
}

func (v *NullableWhatsAppTemplateMessageComponents) Set(val *WhatsAppTemplateMessageComponents) {
	v.value = val
	v.isSet = true
}

func (v NullableWhatsAppTemplateMessageComponents) IsSet() bool {
	return v.isSet
}

func (v *NullableWhatsAppTemplateMessageComponents) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableWhatsAppTemplateMessageComponents(val *WhatsAppTemplateMessageComponents) *NullableWhatsAppTemplateMessageComponents {
	return &NullableWhatsAppTemplateMessageComponents{value: val, isSet: true}
}

func (v NullableWhatsAppTemplateMessageComponents) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableWhatsAppTemplateMessageComponents) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


