/*
 * Unified API
 *
 * Unified API allows sending and receiving messages through different messaging platforms using single, unified API.
 *
 * API version: 1.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package unifiedapi

import (
	"encoding/json"
)

// Footer struct for Footer
type Footer struct {
	Text *string `json:"text,omitempty"`
}

// NewFooter instantiates a new Footer object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewFooter() *Footer {
	this := Footer{}
	return &this
}

// NewFooterWithDefaults instantiates a new Footer object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewFooterWithDefaults() *Footer {
	this := Footer{}
	return &this
}

// GetText returns the Text field value if set, zero value otherwise.
func (o *Footer) GetText() string {
	if o == nil || o.Text == nil {
		var ret string
		return ret
	}
	return *o.Text
}

// GetTextOk returns a tuple with the Text field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Footer) GetTextOk() (*string, bool) {
	if o == nil || o.Text == nil {
		return nil, false
	}
	return o.Text, true
}

// HasText returns a boolean if a field has been set.
func (o *Footer) HasText() bool {
	if o != nil && o.Text != nil {
		return true
	}

	return false
}

// SetText gets a reference to the given string and assigns it to the Text field.
func (o *Footer) SetText(v string) {
	o.Text = &v
}

func (o Footer) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Text != nil {
		toSerialize["text"] = o.Text
	}
	return json.Marshal(toSerialize)
}

type NullableFooter struct {
	value *Footer
	isSet bool
}

func (v NullableFooter) Get() *Footer {
	return v.value
}

func (v *NullableFooter) Set(val *Footer) {
	v.value = val
	v.isSet = true
}

func (v NullableFooter) IsSet() bool {
	return v.isSet
}

func (v *NullableFooter) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFooter(val *Footer) *NullableFooter {
	return &NullableFooter{value: val, isSet: true}
}

func (v NullableFooter) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFooter) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


