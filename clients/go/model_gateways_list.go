/*
 * Unified API
 *
 * Unified API allows sending and receiving messages through different messaging platforms using single, unified API.
 *
 * API version: 1.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package unifiedapi

import (
	"encoding/json"
)

// GatewaysList struct for GatewaysList
type GatewaysList struct {
	Results *[]Gateway `json:"results,omitempty"`
	Total *int32 `json:"total,omitempty"`
	Limit *int32 `json:"limit,omitempty"`
	Offset *int32 `json:"offset,omitempty"`
}

// NewGatewaysList instantiates a new GatewaysList object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGatewaysList() *GatewaysList {
	this := GatewaysList{}
	return &this
}

// NewGatewaysListWithDefaults instantiates a new GatewaysList object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGatewaysListWithDefaults() *GatewaysList {
	this := GatewaysList{}
	return &this
}

// GetResults returns the Results field value if set, zero value otherwise.
func (o *GatewaysList) GetResults() []Gateway {
	if o == nil || o.Results == nil {
		var ret []Gateway
		return ret
	}
	return *o.Results
}

// GetResultsOk returns a tuple with the Results field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GatewaysList) GetResultsOk() (*[]Gateway, bool) {
	if o == nil || o.Results == nil {
		return nil, false
	}
	return o.Results, true
}

// HasResults returns a boolean if a field has been set.
func (o *GatewaysList) HasResults() bool {
	if o != nil && o.Results != nil {
		return true
	}

	return false
}

// SetResults gets a reference to the given []Gateway and assigns it to the Results field.
func (o *GatewaysList) SetResults(v []Gateway) {
	o.Results = &v
}

// GetTotal returns the Total field value if set, zero value otherwise.
func (o *GatewaysList) GetTotal() int32 {
	if o == nil || o.Total == nil {
		var ret int32
		return ret
	}
	return *o.Total
}

// GetTotalOk returns a tuple with the Total field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GatewaysList) GetTotalOk() (*int32, bool) {
	if o == nil || o.Total == nil {
		return nil, false
	}
	return o.Total, true
}

// HasTotal returns a boolean if a field has been set.
func (o *GatewaysList) HasTotal() bool {
	if o != nil && o.Total != nil {
		return true
	}

	return false
}

// SetTotal gets a reference to the given int32 and assigns it to the Total field.
func (o *GatewaysList) SetTotal(v int32) {
	o.Total = &v
}

// GetLimit returns the Limit field value if set, zero value otherwise.
func (o *GatewaysList) GetLimit() int32 {
	if o == nil || o.Limit == nil {
		var ret int32
		return ret
	}
	return *o.Limit
}

// GetLimitOk returns a tuple with the Limit field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GatewaysList) GetLimitOk() (*int32, bool) {
	if o == nil || o.Limit == nil {
		return nil, false
	}
	return o.Limit, true
}

// HasLimit returns a boolean if a field has been set.
func (o *GatewaysList) HasLimit() bool {
	if o != nil && o.Limit != nil {
		return true
	}

	return false
}

// SetLimit gets a reference to the given int32 and assigns it to the Limit field.
func (o *GatewaysList) SetLimit(v int32) {
	o.Limit = &v
}

// GetOffset returns the Offset field value if set, zero value otherwise.
func (o *GatewaysList) GetOffset() int32 {
	if o == nil || o.Offset == nil {
		var ret int32
		return ret
	}
	return *o.Offset
}

// GetOffsetOk returns a tuple with the Offset field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GatewaysList) GetOffsetOk() (*int32, bool) {
	if o == nil || o.Offset == nil {
		return nil, false
	}
	return o.Offset, true
}

// HasOffset returns a boolean if a field has been set.
func (o *GatewaysList) HasOffset() bool {
	if o != nil && o.Offset != nil {
		return true
	}

	return false
}

// SetOffset gets a reference to the given int32 and assigns it to the Offset field.
func (o *GatewaysList) SetOffset(v int32) {
	o.Offset = &v
}

func (o GatewaysList) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Results != nil {
		toSerialize["results"] = o.Results
	}
	if o.Total != nil {
		toSerialize["total"] = o.Total
	}
	if o.Limit != nil {
		toSerialize["limit"] = o.Limit
	}
	if o.Offset != nil {
		toSerialize["offset"] = o.Offset
	}
	return json.Marshal(toSerialize)
}

type NullableGatewaysList struct {
	value *GatewaysList
	isSet bool
}

func (v NullableGatewaysList) Get() *GatewaysList {
	return v.value
}

func (v *NullableGatewaysList) Set(val *GatewaysList) {
	v.value = val
	v.isSet = true
}

func (v NullableGatewaysList) IsSet() bool {
	return v.isSet
}

func (v *NullableGatewaysList) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGatewaysList(val *GatewaysList) *NullableGatewaysList {
	return &NullableGatewaysList{value: val, isSet: true}
}

func (v NullableGatewaysList) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGatewaysList) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


