/*
 * Unified API
 *
 * Unified API allows sending and receiving messages through different messaging platforms using single, unified API.
 *
 * API version: 1.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package unifiedapi

import (
	"encoding/json"
)

// GenericResponse struct for GenericResponse
type GenericResponse struct {
	Code int32 `json:"code"`
	Message *string `json:"message,omitempty"`
	ErrorId *string `json:"error_id,omitempty"`
	Errors *[]GenericResponseErrors `json:"errors,omitempty"`
}

// NewGenericResponse instantiates a new GenericResponse object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewGenericResponse(code int32) *GenericResponse {
	this := GenericResponse{}
	this.Code = code
	return &this
}

// NewGenericResponseWithDefaults instantiates a new GenericResponse object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewGenericResponseWithDefaults() *GenericResponse {
	this := GenericResponse{}
	return &this
}

// GetCode returns the Code field value
func (o *GenericResponse) GetCode() int32 {
	if o == nil {
		var ret int32
		return ret
	}

	return o.Code
}

// GetCodeOk returns a tuple with the Code field value
// and a boolean to check if the value has been set.
func (o *GenericResponse) GetCodeOk() (*int32, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Code, true
}

// SetCode sets field value
func (o *GenericResponse) SetCode(v int32) {
	o.Code = v
}

// GetMessage returns the Message field value if set, zero value otherwise.
func (o *GenericResponse) GetMessage() string {
	if o == nil || o.Message == nil {
		var ret string
		return ret
	}
	return *o.Message
}

// GetMessageOk returns a tuple with the Message field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GenericResponse) GetMessageOk() (*string, bool) {
	if o == nil || o.Message == nil {
		return nil, false
	}
	return o.Message, true
}

// HasMessage returns a boolean if a field has been set.
func (o *GenericResponse) HasMessage() bool {
	if o != nil && o.Message != nil {
		return true
	}

	return false
}

// SetMessage gets a reference to the given string and assigns it to the Message field.
func (o *GenericResponse) SetMessage(v string) {
	o.Message = &v
}

// GetErrorId returns the ErrorId field value if set, zero value otherwise.
func (o *GenericResponse) GetErrorId() string {
	if o == nil || o.ErrorId == nil {
		var ret string
		return ret
	}
	return *o.ErrorId
}

// GetErrorIdOk returns a tuple with the ErrorId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GenericResponse) GetErrorIdOk() (*string, bool) {
	if o == nil || o.ErrorId == nil {
		return nil, false
	}
	return o.ErrorId, true
}

// HasErrorId returns a boolean if a field has been set.
func (o *GenericResponse) HasErrorId() bool {
	if o != nil && o.ErrorId != nil {
		return true
	}

	return false
}

// SetErrorId gets a reference to the given string and assigns it to the ErrorId field.
func (o *GenericResponse) SetErrorId(v string) {
	o.ErrorId = &v
}

// GetErrors returns the Errors field value if set, zero value otherwise.
func (o *GenericResponse) GetErrors() []GenericResponseErrors {
	if o == nil || o.Errors == nil {
		var ret []GenericResponseErrors
		return ret
	}
	return *o.Errors
}

// GetErrorsOk returns a tuple with the Errors field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *GenericResponse) GetErrorsOk() (*[]GenericResponseErrors, bool) {
	if o == nil || o.Errors == nil {
		return nil, false
	}
	return o.Errors, true
}

// HasErrors returns a boolean if a field has been set.
func (o *GenericResponse) HasErrors() bool {
	if o != nil && o.Errors != nil {
		return true
	}

	return false
}

// SetErrors gets a reference to the given []GenericResponseErrors and assigns it to the Errors field.
func (o *GenericResponse) SetErrors(v []GenericResponseErrors) {
	o.Errors = &v
}

func (o GenericResponse) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["code"] = o.Code
	}
	if o.Message != nil {
		toSerialize["message"] = o.Message
	}
	if o.ErrorId != nil {
		toSerialize["error_id"] = o.ErrorId
	}
	if o.Errors != nil {
		toSerialize["errors"] = o.Errors
	}
	return json.Marshal(toSerialize)
}

type NullableGenericResponse struct {
	value *GenericResponse
	isSet bool
}

func (v NullableGenericResponse) Get() *GenericResponse {
	return v.value
}

func (v *NullableGenericResponse) Set(val *GenericResponse) {
	v.value = val
	v.isSet = true
}

func (v NullableGenericResponse) IsSet() bool {
	return v.isSet
}

func (v *NullableGenericResponse) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableGenericResponse(val *GenericResponse) *NullableGenericResponse {
	return &NullableGenericResponse{value: val, isSet: true}
}

func (v NullableGenericResponse) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableGenericResponse) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


