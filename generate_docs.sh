#!/usr/bin/env sh

mkdir generated

echo "generate docs"
java -jar /opt/swagger-codegen-cli/swagger-codegen-cli.jar generate -l openapi -i reference/api.v1.yaml -o out
cp swagger_template.html generated/unifiedapi.html
sed -i "s|{{ file_name }}|unifiedapi.json.gz|g" generated/unifiedapi.html
gzip -c out/openapi.json > generated/unifiedapi.json.gz
for FILE in pages/*
do
  FILENAME="$(basename "$FILE" | cut -d'.' -f1)"
  echo "generate docs for ${FILENAME}"
  cp markdown_template.html "generated/${FILENAME}.html"
  sed -i "s|{{ file_name }}|${FILENAME}.md.gz|g" "generated/${FILENAME}.html"
  gzip -c "pages/${FILENAME}.md" > "generated/${FILENAME}.md.gz"
done

